﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainDeterminationMapping
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainDeterminationMapping))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabLocationMapping = New System.Windows.Forms.TabPage()
        Me.OSDLocationMappingDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OSDLocationMappingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSource_MaintainDeterminationMapping = New WindowsApp1.DataSource_MaintainDeterminationMapping()
        Me.TabTaxCategoryMapping = New System.Windows.Forms.TabPage()
        Me.OSDTaxTypeMappingDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OSDTaxTypeMappingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OSDLocationMappingTableAdapter = New WindowsApp1.DataSource_MaintainDeterminationMappingTableAdapters.OSDLocationMappingTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainDeterminationMappingTableAdapters.TableAdapterManager()
        Me.OSDTaxTypeMappingTableAdapter = New WindowsApp1.DataSource_MaintainDeterminationMappingTableAdapters.OSDTaxTypeMappingTableAdapter()
        Me.OSDLocationMappingBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.OSDLocationMappingBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabLocationMapping.SuspendLayout()
        CType(Me.OSDLocationMappingDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OSDLocationMappingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSource_MaintainDeterminationMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabTaxCategoryMapping.SuspendLayout()
        CType(Me.OSDTaxTypeMappingDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OSDTaxTypeMappingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OSDLocationMappingBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OSDLocationMappingBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabLocationMapping)
        Me.TabControl1.Controls.Add(Me.TabTaxCategoryMapping)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(752, 421)
        Me.TabControl1.TabIndex = 0
        '
        'TabLocationMapping
        '
        Me.TabLocationMapping.Controls.Add(Me.OSDLocationMappingDataGridView)
        Me.TabLocationMapping.Location = New System.Drawing.Point(4, 22)
        Me.TabLocationMapping.Name = "TabLocationMapping"
        Me.TabLocationMapping.Padding = New System.Windows.Forms.Padding(3)
        Me.TabLocationMapping.Size = New System.Drawing.Size(744, 395)
        Me.TabLocationMapping.TabIndex = 0
        Me.TabLocationMapping.Text = "Location Mapping"
        Me.TabLocationMapping.UseVisualStyleBackColor = True
        '
        'OSDLocationMappingDataGridView
        '
        Me.OSDLocationMappingDataGridView.AutoGenerateColumns = False
        Me.OSDLocationMappingDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.OSDLocationMappingDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.OSDLocationMappingDataGridView.DataSource = Me.OSDLocationMappingBindingSource
        Me.OSDLocationMappingDataGridView.Location = New System.Drawing.Point(3, 6)
        Me.OSDLocationMappingDataGridView.Name = "OSDLocationMappingDataGridView"
        Me.OSDLocationMappingDataGridView.Size = New System.Drawing.Size(738, 388)
        Me.OSDLocationMappingDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Group2"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Group2"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "StateCode"
        Me.DataGridViewTextBoxColumn2.HeaderText = "StateCode"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "DivisionName"
        Me.DataGridViewTextBoxColumn3.HeaderText = "DivisionName"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "LocationCode"
        Me.DataGridViewTextBoxColumn4.HeaderText = "LocationCode"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'OSDLocationMappingBindingSource
        '
        Me.OSDLocationMappingBindingSource.DataMember = "OSDLocationMapping"
        Me.OSDLocationMappingBindingSource.DataSource = Me.DataSource_MaintainDeterminationMapping
        '
        'DataSource_MaintainDeterminationMapping
        '
        Me.DataSource_MaintainDeterminationMapping.DataSetName = "DataSource_MaintainDeterminationMapping"
        Me.DataSource_MaintainDeterminationMapping.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabTaxCategoryMapping
        '
        Me.TabTaxCategoryMapping.Controls.Add(Me.OSDTaxTypeMappingDataGridView)
        Me.TabTaxCategoryMapping.Location = New System.Drawing.Point(4, 22)
        Me.TabTaxCategoryMapping.Name = "TabTaxCategoryMapping"
        Me.TabTaxCategoryMapping.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTaxCategoryMapping.Size = New System.Drawing.Size(744, 433)
        Me.TabTaxCategoryMapping.TabIndex = 1
        Me.TabTaxCategoryMapping.Text = "Tax Category Mapping"
        Me.TabTaxCategoryMapping.UseVisualStyleBackColor = True
        '
        'OSDTaxTypeMappingDataGridView
        '
        Me.OSDTaxTypeMappingDataGridView.AutoGenerateColumns = False
        Me.OSDTaxTypeMappingDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.OSDTaxTypeMappingDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.OSDTaxTypeMappingDataGridView.DataSource = Me.OSDTaxTypeMappingBindingSource
        Me.OSDTaxTypeMappingDataGridView.Location = New System.Drawing.Point(3, 6)
        Me.OSDTaxTypeMappingDataGridView.Name = "OSDTaxTypeMappingDataGridView"
        Me.OSDTaxTypeMappingDataGridView.Size = New System.Drawing.Size(732, 421)
        Me.OSDTaxTypeMappingDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "StateCode"
        Me.DataGridViewTextBoxColumn5.HeaderText = "StateCode"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "TaxType"
        Me.DataGridViewTextBoxColumn6.HeaderText = "TaxType"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "SalesUseCode"
        Me.DataGridViewTextBoxColumn7.HeaderText = "SalesUseCode"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "LocationCode"
        Me.DataGridViewTextBoxColumn8.HeaderText = "LocationCode"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'OSDTaxTypeMappingBindingSource
        '
        Me.OSDTaxTypeMappingBindingSource.DataMember = "OSDTaxTypeMapping"
        Me.OSDTaxTypeMappingBindingSource.DataSource = Me.DataSource_MaintainDeterminationMapping
        '
        'OSDLocationMappingTableAdapter
        '
        Me.OSDLocationMappingTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.OSDLocationMappingTableAdapter = Me.OSDLocationMappingTableAdapter
        Me.TableAdapterManager.OSDTaxTypeMappingTableAdapter = Me.OSDTaxTypeMappingTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainDeterminationMappingTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'OSDTaxTypeMappingTableAdapter
        '
        Me.OSDTaxTypeMappingTableAdapter.ClearBeforeFill = True
        '
        'OSDLocationMappingBindingNavigator
        '
        Me.OSDLocationMappingBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.OSDLocationMappingBindingNavigator.BindingSource = Me.OSDLocationMappingBindingSource
        Me.OSDLocationMappingBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.OSDLocationMappingBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.OSDLocationMappingBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.OSDLocationMappingBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.OSDLocationMappingBindingNavigatorSaveItem})
        Me.OSDLocationMappingBindingNavigator.Location = New System.Drawing.Point(0, 471)
        Me.OSDLocationMappingBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.OSDLocationMappingBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.OSDLocationMappingBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.OSDLocationMappingBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.OSDLocationMappingBindingNavigator.Name = "OSDLocationMappingBindingNavigator"
        Me.OSDLocationMappingBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.OSDLocationMappingBindingNavigator.Size = New System.Drawing.Size(776, 25)
        Me.OSDLocationMappingBindingNavigator.TabIndex = 1
        Me.OSDLocationMappingBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'OSDLocationMappingBindingNavigatorSaveItem
        '
        Me.OSDLocationMappingBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.OSDLocationMappingBindingNavigatorSaveItem.Image = CType(resources.GetObject("OSDLocationMappingBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.OSDLocationMappingBindingNavigatorSaveItem.Name = "OSDLocationMappingBindingNavigatorSaveItem"
        Me.OSDLocationMappingBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.OSDLocationMappingBindingNavigatorSaveItem.Text = "Save Data"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(682, 445)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainDeterminationMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 496)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.OSDLocationMappingBindingNavigator)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainDeterminationMapping"
        Me.Text = "Maintain Determination Mapping"
        Me.TabControl1.ResumeLayout(False)
        Me.TabLocationMapping.ResumeLayout(False)
        CType(Me.OSDLocationMappingDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OSDLocationMappingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSource_MaintainDeterminationMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabTaxCategoryMapping.ResumeLayout(False)
        CType(Me.OSDTaxTypeMappingDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OSDTaxTypeMappingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OSDLocationMappingBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OSDLocationMappingBindingNavigator.ResumeLayout(False)
        Me.OSDLocationMappingBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabLocationMapping As TabPage
    Friend WithEvents TabTaxCategoryMapping As TabPage
    Friend WithEvents DataSource_MaintainDeterminationMapping As DataSource_MaintainDeterminationMapping
    Friend WithEvents OSDLocationMappingBindingSource As BindingSource
    Friend WithEvents OSDLocationMappingTableAdapter As DataSource_MaintainDeterminationMappingTableAdapters.OSDLocationMappingTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_MaintainDeterminationMappingTableAdapters.TableAdapterManager
    Friend WithEvents OSDLocationMappingBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents OSDLocationMappingBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents OSDLocationMappingDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents OSDTaxTypeMappingTableAdapter As DataSource_MaintainDeterminationMappingTableAdapters.OSDTaxTypeMappingTableAdapter
    Friend WithEvents OSDTaxTypeMappingBindingSource As BindingSource
    Friend WithEvents OSDTaxTypeMappingDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents btnClose As Button
End Class

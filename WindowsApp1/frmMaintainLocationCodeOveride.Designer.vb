﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainLocationCodeOveride
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainLocationCodeOveride))
        Me.DataSource_MaintainLocationCodeOveride = New WindowsApp1.DataSource_MaintainLocationCodeOveride()
        Me.LocationCodeOverrideBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LocationCodeOverrideTableAdapter = New WindowsApp1.DataSource_MaintainLocationCodeOverideTableAdapters.LocationCodeOverrideTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainLocationCodeOverideTableAdapters.TableAdapterManager()
        Me.LocationCodeOverrideBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.LocationCodeOverrideBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.LocationCodeOverrideDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.DataSource_MaintainLocationCodeOveride, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocationCodeOverrideBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocationCodeOverrideBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LocationCodeOverrideBindingNavigator.SuspendLayout()
        CType(Me.LocationCodeOverrideDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSource_MaintainLocationCodeOveride
        '
        Me.DataSource_MaintainLocationCodeOveride.DataSetName = "DataSource_MaintainLocationCodeOveride"
        Me.DataSource_MaintainLocationCodeOveride.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LocationCodeOverrideBindingSource
        '
        Me.LocationCodeOverrideBindingSource.DataMember = "LocationCodeOverride"
        Me.LocationCodeOverrideBindingSource.DataSource = Me.DataSource_MaintainLocationCodeOveride
        '
        'LocationCodeOverrideTableAdapter
        '
        Me.LocationCodeOverrideTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.LocationCodeOverrideTableAdapter = Me.LocationCodeOverrideTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainLocationCodeOverideTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'LocationCodeOverrideBindingNavigator
        '
        Me.LocationCodeOverrideBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.LocationCodeOverrideBindingNavigator.BindingSource = Me.LocationCodeOverrideBindingSource
        Me.LocationCodeOverrideBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.LocationCodeOverrideBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.LocationCodeOverrideBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.LocationCodeOverrideBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.LocationCodeOverrideBindingNavigatorSaveItem})
        Me.LocationCodeOverrideBindingNavigator.Location = New System.Drawing.Point(0, 472)
        Me.LocationCodeOverrideBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.LocationCodeOverrideBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.LocationCodeOverrideBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.LocationCodeOverrideBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.LocationCodeOverrideBindingNavigator.Name = "LocationCodeOverrideBindingNavigator"
        Me.LocationCodeOverrideBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.LocationCodeOverrideBindingNavigator.Size = New System.Drawing.Size(899, 25)
        Me.LocationCodeOverrideBindingNavigator.TabIndex = 0
        Me.LocationCodeOverrideBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'LocationCodeOverrideBindingNavigatorSaveItem
        '
        Me.LocationCodeOverrideBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.LocationCodeOverrideBindingNavigatorSaveItem.Image = CType(resources.GetObject("LocationCodeOverrideBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.LocationCodeOverrideBindingNavigatorSaveItem.Name = "LocationCodeOverrideBindingNavigatorSaveItem"
        Me.LocationCodeOverrideBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.LocationCodeOverrideBindingNavigatorSaveItem.Text = "Save Data"
        '
        'LocationCodeOverrideDataGridView
        '
        Me.LocationCodeOverrideDataGridView.AutoGenerateColumns = False
        Me.LocationCodeOverrideDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.LocationCodeOverrideDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.LocationCodeOverrideDataGridView.DataSource = Me.LocationCodeOverrideBindingSource
        Me.LocationCodeOverrideDataGridView.Location = New System.Drawing.Point(12, 28)
        Me.LocationCodeOverrideDataGridView.Name = "LocationCodeOverrideDataGridView"
        Me.LocationCodeOverrideDataGridView.Size = New System.Drawing.Size(875, 426)
        Me.LocationCodeOverrideDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "RIABinder"
        Me.DataGridViewTextBoxColumn1.HeaderText = "RIABinder"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "State"
        Me.DataGridViewTextBoxColumn2.HeaderText = "State"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "PeriodID"
        Me.DataGridViewTextBoxColumn3.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "LocationCode"
        Me.DataGridViewTextBoxColumn4.HeaderText = "LocationCode"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "OverrideLocationCode"
        Me.DataGridViewTextBoxColumn5.HeaderText = "OverrideLocationCode"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "SalesUse"
        Me.DataGridViewTextBoxColumn6.HeaderText = "SalesUse"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "OverrideRIABinder"
        Me.DataGridViewTextBoxColumn7.HeaderText = "OverrideRIABinder"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(812, 460)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainLocationCodeOveride
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(899, 497)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.LocationCodeOverrideDataGridView)
        Me.Controls.Add(Me.LocationCodeOverrideBindingNavigator)
        Me.Name = "frmMaintainLocationCodeOveride"
        Me.Text = "Maintain Location Code Overide"
        CType(Me.DataSource_MaintainLocationCodeOveride, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocationCodeOverrideBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocationCodeOverrideBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LocationCodeOverrideBindingNavigator.ResumeLayout(False)
        Me.LocationCodeOverrideBindingNavigator.PerformLayout()
        CType(Me.LocationCodeOverrideDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataSource_MaintainLocationCodeOveride As DataSource_MaintainLocationCodeOveride
    Friend WithEvents LocationCodeOverrideBindingSource As BindingSource
    Friend WithEvents LocationCodeOverrideTableAdapter As DataSource_MaintainLocationCodeOverideTableAdapters.LocationCodeOverrideTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_MaintainLocationCodeOverideTableAdapters.TableAdapterManager
    Friend WithEvents LocationCodeOverrideBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents LocationCodeOverrideBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents LocationCodeOverrideDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents btnClose As Button
End Class

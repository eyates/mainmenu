﻿Public Class frmMaintainRIABinders
    Private Sub RIABinderBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles RIABinderBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.RIABinderBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainRIABinders)

    End Sub

    Private Sub frmMaintainRIABinders_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainRIABinders.RIABinderStateReturn' table. You can move, or remove it, as needed.
        Me.RIABinderStateReturnTableAdapter.Fill(Me.DataSource_MaintainRIABinders.RIABinderStateReturn)
        'TODO: This line of code loads data into the 'DataSource_MaintainRIABinders.RIABinder' table. You can move, or remove it, as needed.
        Me.RIABinderTableAdapter.Fill(Me.DataSource_MaintainRIABinders.RIABinder)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
﻿Public Class frmMaintainRIABinderStateReturn
    Private Sub RIABinderStateReturnBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles RIABinderStateReturnBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.RIABinderStateReturnBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainRIABinderStateReturn)

    End Sub

    Private Sub frmMaintainRIABinderStateReturn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainRIABinderStateReturn.RIABinderStateReturn' table. You can move, or remove it, as needed.
        Me.RIABinderStateReturnTableAdapter.Fill(Me.DataSource_MaintainRIABinderStateReturn.RIABinderStateReturn)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
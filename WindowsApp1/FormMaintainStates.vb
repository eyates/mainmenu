﻿Public Class FormMaintainStates
    Private Sub StateBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.StateBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.SalesUse_UniDataSet1)

    End Sub

    Private Sub FormMaintainStates_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SalesUse_UniDataSet1.State' table. You can move, or remove it, as needed.
        Me.StateTableAdapter.Fill(Me.SalesUse_UniDataSet1.State)

    End Sub
End Class
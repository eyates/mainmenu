﻿Imports System.Data.SqlClient

Public Class frmMaintainAccountsToExclude

    Private Sub frmMaintainAccountsToExclude_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadPeriodCombo()
        LoadDGV_MaintainAccountsToExclude()
    End Sub

    Private Sub LoadPeriodCombo()
        Dim SQL As String = "SELECT Period.Period, Period.CurrentPeriodFlag FROM Period ORDER BY Period DESC "
        Using connection As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(SQL, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                cboPeriod.ValueMember = "Period"
                cboPeriod.DisplayMember = "Period"
                cboPeriod.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub LoadDGV_MaintainAccountsToExclude()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT AccountsToExclude.PeriodID, AccountsToExclude.AccountNumber, AccountsToExclude.SystemID, AccountsToExclude.Inactive, AccountsToExclude.Comment FROM AccountsToExclude "
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " WHERE PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_MaintainAccountsToExclude.DataSource = dtRecord
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriod.SelectedIndexChanged
        cboPeriod.Text = cboPeriod.SelectedValue
        LoadDGV_MaintainAccountsToExclude()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
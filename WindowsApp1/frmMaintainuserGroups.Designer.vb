﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMaintainuserGroups
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainuserGroups))
        Me.UserRightsBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.UserRightsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSource_MaintainUserGroups = New WindowsApp1.DataSource_MaintainUserGroups()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.UserRightsBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripComboBox1 = New System.Windows.Forms.ToolStripComboBox()
        Me.UserRightsDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UserRightsTableAdapter = New WindowsApp1.DataSource_MaintainUserGroupsTableAdapters.UserRightsTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainUserGroupsTableAdapters.TableAdapterManager()
        Me.UserGroupRightsTableAdapter = New WindowsApp1.DataSource_MaintainUserGroupsTableAdapters.UserGroupRightsTableAdapter()
        Me.UserGroupRightsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UserGroupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UserGroupTableAdapter = New WindowsApp1.DataSource_MaintainUserGroupsTableAdapters.UserGroupTableAdapter()
        Me.UserGroupComboBox = New System.Windows.Forms.ComboBox()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.UserRightsBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UserRightsBindingNavigator.SuspendLayout()
        CType(Me.UserRightsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSource_MaintainUserGroups, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UserRightsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UserGroupRightsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UserGroupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UserRightsBindingNavigator
        '
        Me.UserRightsBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.UserRightsBindingNavigator.BindingSource = Me.UserRightsBindingSource
        Me.UserRightsBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.UserRightsBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.UserRightsBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UserRightsBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.UserRightsBindingNavigatorSaveItem, Me.ToolStripComboBox1})
        Me.UserRightsBindingNavigator.Location = New System.Drawing.Point(0, 298)
        Me.UserRightsBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.UserRightsBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.UserRightsBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.UserRightsBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.UserRightsBindingNavigator.Name = "UserRightsBindingNavigator"
        Me.UserRightsBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.UserRightsBindingNavigator.Size = New System.Drawing.Size(710, 25)
        Me.UserRightsBindingNavigator.TabIndex = 0
        Me.UserRightsBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'UserRightsBindingSource
        '
        Me.UserRightsBindingSource.DataMember = "UserRights"
        Me.UserRightsBindingSource.DataSource = Me.DataSource_MaintainUserGroups
        '
        'DataSource_MaintainUserGroups
        '
        Me.DataSource_MaintainUserGroups.DataSetName = "DataSource_MaintainUserGroups"
        Me.DataSource_MaintainUserGroups.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'UserRightsBindingNavigatorSaveItem
        '
        Me.UserRightsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.UserRightsBindingNavigatorSaveItem.Image = CType(resources.GetObject("UserRightsBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.UserRightsBindingNavigatorSaveItem.Name = "UserRightsBindingNavigatorSaveItem"
        Me.UserRightsBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.UserRightsBindingNavigatorSaveItem.Text = "Save Data"
        '
        'ToolStripComboBox1
        '
        Me.ToolStripComboBox1.Name = "ToolStripComboBox1"
        Me.ToolStripComboBox1.Size = New System.Drawing.Size(121, 25)
        '
        'UserRightsDataGridView
        '
        Me.UserRightsDataGridView.AutoGenerateColumns = False
        Me.UserRightsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.UserRightsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.UserRightsDataGridView.DataSource = Me.UserRightsBindingSource
        Me.UserRightsDataGridView.Location = New System.Drawing.Point(12, 28)
        Me.UserRightsDataGridView.Name = "UserRightsDataGridView"
        Me.UserRightsDataGridView.Size = New System.Drawing.Size(300, 220)
        Me.UserRightsDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "UserRightsKey"
        Me.DataGridViewTextBoxColumn1.HeaderText = "UserRightsKey"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "RightDescription"
        Me.DataGridViewTextBoxColumn2.HeaderText = "RightDescription"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'UserRightsTableAdapter
        '
        Me.UserRightsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainUserGroupsTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.UserGroupRightsTableAdapter = Me.UserGroupRightsTableAdapter
        Me.TableAdapterManager.UserGroupTableAdapter = Nothing
        Me.TableAdapterManager.UserRightsTableAdapter = Me.UserRightsTableAdapter
        '
        'UserGroupRightsTableAdapter
        '
        Me.UserGroupRightsTableAdapter.ClearBeforeFill = True
        '
        'UserGroupRightsBindingSource
        '
        Me.UserGroupRightsBindingSource.DataMember = "UserGroupRights"
        Me.UserGroupRightsBindingSource.DataSource = Me.DataSource_MaintainUserGroups
        '
        'UserGroupBindingSource
        '
        Me.UserGroupBindingSource.DataMember = "UserGroup"
        Me.UserGroupBindingSource.DataSource = Me.DataSource_MaintainUserGroups
        '
        'UserGroupTableAdapter
        '
        Me.UserGroupTableAdapter.ClearBeforeFill = True
        '
        'UserGroupComboBox
        '
        Me.UserGroupComboBox.DataSource = Me.UserGroupBindingSource
        Me.UserGroupComboBox.DisplayMember = "UserGroup"
        Me.UserGroupComboBox.FormattingEnabled = True
        Me.UserGroupComboBox.Location = New System.Drawing.Point(329, 28)
        Me.UserGroupComboBox.Name = "UserGroupComboBox"
        Me.UserGroupComboBox.Size = New System.Drawing.Size(300, 21)
        Me.UserGroupComboBox.TabIndex = 2
        Me.UserGroupComboBox.ValueMember = "UserGroup"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(351, 225)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainuserGroups
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(710, 323)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.UserGroupComboBox)
        Me.Controls.Add(Me.UserRightsDataGridView)
        Me.Controls.Add(Me.UserRightsBindingNavigator)
        Me.Name = "frmMaintainuserGroups"
        Me.Text = "frmMaintainuserGroups"
        CType(Me.UserRightsBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UserRightsBindingNavigator.ResumeLayout(False)
        Me.UserRightsBindingNavigator.PerformLayout()
        CType(Me.UserRightsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSource_MaintainUserGroups, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UserRightsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UserGroupRightsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UserGroupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataSource_MaintainUserGroups As DataSource_MaintainUserGroups
    Friend WithEvents UserRightsBindingSource As BindingSource
    Friend WithEvents UserRightsTableAdapter As DataSource_MaintainUserGroupsTableAdapters.UserRightsTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_MaintainUserGroupsTableAdapters.TableAdapterManager
    Friend WithEvents UserRightsBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents UserRightsBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents UserGroupRightsTableAdapter As DataSource_MaintainUserGroupsTableAdapters.UserGroupRightsTableAdapter
    Friend WithEvents ToolStripComboBox1 As ToolStripComboBox
    Friend WithEvents UserRightsDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents UserGroupRightsBindingSource As BindingSource
    Friend WithEvents UserGroupBindingSource As BindingSource
    Friend WithEvents UserGroupTableAdapter As DataSource_MaintainUserGroupsTableAdapters.UserGroupTableAdapter
    Friend WithEvents UserGroupComboBox As ComboBox
    Friend WithEvents btnClose As Button
End Class

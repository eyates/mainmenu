



Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
'Imports Microsoft.ReportingServices.Rendering.ExcelRenderer
'Imports Excel = Microsoft.Office.Interop.Excel


'   Unless otherwise noted, all functions are written and copyrighted by
'   Front Row Systems, Inc.  Any use outside of this application is prohibited
'
' Summary of Changes
' 10/07/2004  new version posted for Invoice to review report to have outer join
'             to account numbers. Craig Gilbert
' 12/16/04    Added reports and selection form for RIA Input by city county to help
'             prepare business licenses
' 1/14/04     publish new client with ability to maintain tax vendors. change for to
'             set edit and add rights
Public Class [Global] ' Module
    Friend Const gsSuperPath = "C:\SuperClient" 'As System.Void
    Friend Const gsSuperPath_UNI = "C:\Program Files\SuperClientUni" 'As System.Void
    Friend Const gsSuperPath_GP = "C:\Program Files\SuperClient" 'As System.Void
    Friend Const gsTestPath = "C:\Program Files\SuperClientTest" 'As System.Void
    Friend Const gsDevPath = "J:\CUSTOM\UniSource" 'As System.Void
    Friend gsLocalPath As String
    Private gvAPProductionServer As [Object]
    Private gvGLProductionServer As [Object]
    Friend cnnADO As String
    Friend gsSQLConnect As String
    Friend gsExportPath As String
    Friend gsNetworkMacroPath As String
    Friend gsLocalMacroPath As String
    Friend gsLogsPath As String
    Friend gsLocalISUMacroPath As String
    Friend gsAPUploadPath As String
    Friend gsUnisourceBBSPath As String
    Friend gsUnisourceDataPath As String
    Friend gsGLUploadPath As String
    Friend gsSAPImportPath As String
    Friend gsUserName As String
    Friend glCurrentPeriod As Long
    Friend gvUpdated As [Object]
    Public Shared gstrPeriod As String
    'Connection String Vales
    Public Const cstrconnection As String = "Provider=SQLNCLI11;Server=DATLPOSSQL01;Database=SalesUse_Uni;Trusted_Connection=yes;"
    Public Const cstrQDFconnection As String = "ODBC;Driver={SQL Server};Server=DATLPOSSQL01;DATABASE=SalesUse_Uni;PWD;Trusted_Connection=Yes;" '"ODBC;Driver={SQL Server};Server=DATLPOSSQL01;DATABASE=SalesUse_Uni;PWD;Trusted_Connection=Yes;"
    Public Const cstrUser As String = "supertest" '"dba"
    Public Const cstrPW As String = "Football1" '"password"
    Public Const cstrconnection2 = "Server=DESKTOP-RAL4PCA;Database=SalesUse_Uni;Trusted_Connection=True"
    'Period ID varible for use on record navigation



    'Friend Overridable Sub SetGlobalVariables()
    Public Sub SetGlobalVariables()
        'gsLocalPath = UCase(CodeProject.Path)

        Select Case gsLocalPath
            Case UCase(gsTestPath)
                ' MsgBox "You are now connecting to the SUPER TEST environment."
                gvAPProductionServer = False
                gvGLProductionServer = False
                gsSQLConnect = "ODBC;Driver={SQL Server};Server=DATLPOSSQL01;DATABASE=SalesUse_Uni;PWD;Trusted_Connection=Yes;"
                '        gsSQLConnect = "ODBC;Driver={SQL Server};Server=DATLPOSSQL01;DATABASE=SalesUse_Uni;UID=;PWD;Trusted_Connection=Yes"
                cnnADO = "provider=sqloledb;server=DATLPOSSQL01;DATABASE=SalesUse_Uni;UID=;PWD;Trusted_Connection=Yes"
                'gsSQLConnect = "ODBC;Driver={SQL Server};Server=HQSQLUCASH1;DATABASE=SalesUse_Uni;UID=;PWD;Trusted_Connection=Yes"
                gsExportPath = gsTestPath & "\ASCII\"
                gsNetworkMacroPath = "U:\CLRIX\ISU2007\MACRO\"
                gsLocalMacroPath = "U:\CLRIX\ISU2007\MACRO\"
                gsLogsPath = "C:\CLRIX\INSOURCE\2007\LOGS\"
                gsLocalISUMacroPath = "C:\CLRIX\FTAPP06W\"
                gsAPUploadPath = gsTestPath & "\UPLOAD\"
                gsUnisourceBBSPath = gsTestPath & "\SUTXUNIS"  ' enter without trailing /
                gsUnisourceDataPath = gsTestPath & "\FROZENUNIDATA"  ' enter without trailing /
                gsGLUploadPath = gsTestPath & "\UPLOAD\"
                gsSAPImportPath = gsTestPath & "\SAPIMPORT\"
            Case UCase(gsSuperPath), UCase(gsSuperPath_GP)
                'MsgBox "SUPER"
                gvAPProductionServer = True
                gvGLProductionServer = True
                gsSQLConnect = "ODBC;Driver={SQL Server};Server=HQSQLTAX\HQSQLTAX;DATABASE=SalesUseDb;UID=;PWD=;Trusted_Connection=Yes"
                gsExportPath = "M:\SUPER\ASCII\"
                gsNetworkMacroPath = "R:\CLRIX\ISU2007\MACRO\"
                gsLocalMacroPath = "R:\CLRIX\ISU2007\MACRO\"
                gsLogsPath = "C:\CLRIX\INSOURCE\2007\LOGS\"
                gsLocalISUMacroPath = "C:\CLRIX\FTAPP06W\"
                gsAPUploadPath = "M:\SUPER\UPLOAD\"
                gsUnisourceBBSPath = "M:\Uni\SUTXUNIS"  ' enter without trailing /
                gsUnisourceDataPath = "M:\SUPER\UNISOURCE\FROZENUNIDATA"  ' enter without trailing /
                gsGLUploadPath = "M:\SUPER\UPLOAD\"
                gsSAPImportPath = "M:\SUPER\SAPIMPORT\"
            Case UCase(gsSuperPath_UNI)
                'MsgBox "SUPER UNI"
                gvAPProductionServer = True
                gvGLProductionServer = True
                gsSQLConnect = "ODBC;Driver={SQL Server};Server=UWQATLCASH01;DATABASE=SalesUse_Uni;UID=;PWD;Trusted_Connection=Yes"
                'gsSQLConnect = "ODBC;Driver={SQL Server};Server=UWQATLGPC17;DATABASE=SalesUse_Uni;UID=;PWD;Trusted_Connection=Yes"
                gsExportPath = "V:\SUPER\ASCII\"
                gsNetworkMacroPath = "U:\CLRIX\ISU2013\MACRO\"
                gsLocalMacroPath = "U:\CLRIX\ISU2013\MACRO\"
                gsLogsPath = "V:\SUPER\"
                gsLocalISUMacroPath = "C:\CLRIX\FTAPP12W\" 'FTAPP09W
                gsAPUploadPath = "V:\SUPER\UPLOAD\"
                gsUnisourceBBSPath = "M:\Uni\SUTXUNIS"  ' enter without trailing /
                gsUnisourceDataPath = "V:\SUPER\UNISOURCE\FROZENUNIDATA"  ' enter without trailing /
                gsGLUploadPath = "V:\SUPER\UPLOAD\"
                gsSAPImportPath = "V:\SUPER\SAPIMPORT\"
            Case UCase(gsDevPath)
                'MsgBox "You are now connecting to the SUPER TEST environment."
                gvAPProductionServer = False
                gvGLProductionServer = False
                gsSQLConnect = "ODBC;Driver={SQL Server};Server=FRSWS2008;UID=;PWD=;DATABASE=SalesUseDB_UNI;Trusted_Connection=Yes"
                gsExportPath = gsDevPath & "\ASCII\"
                gsNetworkMacroPath = "C:\CLRIX\ISU2004\MACRO\"
                gsLocalMacroPath = "C:\CLRIX\ISU2004\MACRO\"
                gsLogsPath = "C:\CLRIX\ISU2004\LOGS\"
                gsLocalISUMacroPath = "C:\CLRIX\FTAPP06W\"
                gsAPUploadPath = gsDevPath & "\UPLOAD\"
                gsUnisourceBBSPath = gsDevPath & "\SUTXUNIS"  ' enter without trailing /
                gsUnisourceDataPath = gsDevPath & "\FROZENUNIDATA"  ' enter without trailing /
                gsGLUploadPath = gsDevPath & "\UPLOAD\"
                gsSAPImportPath = gsDevPath & "\SAPIMPORT\"
            Case Else
                MsgBox("Your system is not configured properly to run SUPER.  Please contact the Tax IR Group.")
                End
        End Select

    End Sub

    'Friend Overridable Function SuperEnvironment() As Object
    Public Shared Function SuperEnvironment() As Object
        Dim msEnvironment As String = String.Empty

        Select Case "xx"' gsLocalPath
            Case UCase(gsTestPath)
                msEnvironment = "SU_Test Environment"
            Case UCase(gsSuperPath), UCase(gsSuperPath_GP)
                msEnvironment = "GP Super Environment"
            Case UCase(gsSuperPath_UNI)
                msEnvironment = "UNI Super Environment"
            Case UCase(gsDevPath)
                msEnvironment = "Development Environment"
            Case Else
                msEnvironment = "No Environment Set"
        End Select

        SuperEnvironment = msEnvironment


    End Function

    Friend Overridable Function Get_OpenFileName(ByRef msFileName As String, ByRef msDfltPath As String, ByVal msDfltExtension As String, ByVal msTitle As String) As Long
        'On Error GoTo err_Get_OpenFileName'TODO - On Error must be replaced with Try, Catch, Finally

        Dim ofnOpenFile As Object 'OPENFILENAME

        Const OFN_ALLOWMULTISELECT = &H200
        Const OFN_CREATEPROMPT = &H2000
        Const OFN_ENABLEHOOK = &H20
        Const OFN_ENABLESIZING = &H800000
        Const OFN_ENABLETEMPLATE = &H40
        Const OFN_ENABLETEMPLATEHANDLE = &H80
        Const OFN_EXPLORER = &H80000
        Const OFN_EXTENSIONDIFFERENT = &H400
        Const OFN_FILEMUSTEXIST = &H1000
        Const OFN_HIDEREADONLY = &H4
        Const OFN_LONGNAMES = &H200000
        Const OFN_NOCHANGEDIR = &H8
        Const OFN_NODEREFERENCELINKS = &H100000
        Const OFN_NOLONGNAMES = &H40000
        Const OFN_NONETWORKBUTTON = &H20000
        Const OFN_NOREADONLYRETURN = &H8000
        Const OFN_NOTESTFILECREATE = &H10000
        Const OFN_NOVALIDATE = &H100
        Const OFN_OVERWRITEPROMPT = &H2
        Const OFN_PATHMUSTEXIST = &H800
        Const OFN_READONLY = &H1
        Const OFN_SHAREAWARE = &H4000
        Const OFN_SHOWHELP = &H10

        With ofnOpenFile
            .lStructSize = Len(ofnOpenFile)
            '    .lpstrFilter = "All Files" & vbNullChar & "*.*" & vbNullChar
            '    .nMaxCustFilter = 0
            '    .nFilterIndex = 1
            .lpstrFile = msFileName & vbNullChar
            .nMaxFile = Len(ofnOpenFile.lpstrFile)
            .lpstrFileTitle = Space(256) & vbNullChar
            .nMaxFileTitle = Len(.lpstrFileTitle)
            .lpstrInitialDir = msDfltPath & vbNullChar
            .lpstrTitle = msTitle & vbNullChar
            .lpstrDefExt = msDfltExtension & vbNullChar
            .flags = OFN_PATHMUSTEXIST Or OFN_FILEMUSTEXIST Or OFN_HIDEREADONLY
        End With
        'Get_OpenFileName = GetOpenFileName(ofnOpenFile)
        'MsgBox ofnOpenFile.lpstrFile

        msFileName = Microsoft.VisualBasic.Left(ofnOpenFile.lpstrFileTitle, ofnOpenFile.nMaxFileTitle)
        msDfltPath = ofnOpenFile.lpstrInitialDir


        'exit_Get_OpenFileName: - Not supported 
        Exit Function

        'err_Get_OpenFileName: - Not supported 
        'Dim e As Object
        'For Each e In Errors
        '   MsgBox e.Number & vbCrLf & e.Description & vbCrLf & e.Source, vbCritical, "Error"
        'Next
        ' MsgBox "Global_Module" & vbCrLf & "Get_OpenFileName" & vbCrLf & vbCrLf _
        ' & Err.Number & vbCrLf & vbCrLf & Err.Description, vbCritical, "Error"
        'Resume exit_Get_OpenFileName



    End Function

    Public Shared Function SecurityCheck(User As String, Right As String) As Boolean
        Try
            If User <> "" And Right <> "" Then
                Dim SCSQL As String = " Select Users.UserSignOnID, Users.Inactive, UserGroup.UserGroup, UserGroupRights.UserRightsKey, "
                SCSQL = SCSQL & " UserRights.RightDescription "
                SCSQL = SCSQL & "  FROM((Users INNER JOIN UserGroup ON Users.[UserGroup] = UserGroup.[UserGroup]) "
                SCSQL = SCSQL & "     INNER Join UserGroupRights ON UserGroup.[UserGroup] = UserGroupRights.[UserGroup]) "
                SCSQL = SCSQL & "  INNER Join UserRights ON UserGroupRights.[UserRightsKey] = UserRights.[UserRightsKey] "
                SCSQL = SCSQL & " WHERE(((Users.Inactive) = 0)) "
                SCSQL = SCSQL & " AND UserSignonID = '" & User & "'"
                SCSQL = SCSQL & " AND RightDescription = '" & Right & "'"
                Dim sqlConnection1 As New SqlConnection(cstrconnection2)
                Dim cmd2 As New SqlCommand
                Dim reader As SqlDataReader

                cmd2.CommandText = SCSQL
                cmd2.CommandType = CommandType.Text
                cmd2.Connection = sqlConnection1

                sqlConnection1.Open()

                reader = cmd2.ExecuteReader()
                If reader.HasRows Then
                    sqlConnection1.Close()
                    Return True
                Else
                    sqlConnection1.Close()
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception

        End Try


        ' Data is accessible through the DataReader object here.
        'While reader.Read()
        ' Console.WriteLine(reader("UserSignOnID"))
        'End While

    End Function
    Public Shared Function DlookUpVB(ByVal strLookUpFieldName As String,
                            ByVal strTableName As String,
                            Optional ByVal Critera As String = "1=1") As String
        Try
            Dim con As OleDbConnection
            con = New OleDbConnection(cstrconnection)
            Dim dbCmd As OleDbCommand = New OleDbCommand("select " & strLookUpFieldName & " from " & strTableName &
                      " WHERE " & Critera, con)
            con.Open()
            Return dbCmd.ExecuteScalar().ToString
            con.Close()

        Catch ex As Exception
            Return ""
        End Try

    End Function
    Public Shared Function DCountVB(ByVal strCountFieldName As String,
                        ByVal strTableName As String,
                        Optional ByVal Critera As String = "1=1") As Int64
        Try
            Dim con As OleDbConnection
            con = New OleDbConnection(cstrconnection)
            Dim dbCmd As OleDbCommand = New OleDbCommand("select count (" & strCountFieldName & ") from " & strTableName &
                      " WHERE " & Critera, con)
            con.Open()
            Return dbCmd.ExecuteScalar().ToString
            con.Close()

        Catch ex As Exception
            Return 0
        End Try

    End Function
    Public Shared Function RunSP(SPToRun As String, Optional ByVal SPParamName As String = "", Optional ByVal SPParam As String = "") As Integer
        Try

            Dim sqlCon As New SqlConnection
            sqlCon.ConnectionString = cstrconnection2

            Using (sqlCon)
                Dim sqlComm As New SqlCommand
                sqlComm.Connection = sqlCon
                sqlComm.CommandText = SPToRun
                sqlComm.CommandType = CommandType.StoredProcedure
                If SPParam <> "" Then
                    sqlComm.Parameters.AddWithValue(SPParamName, SPParam)
                End If
                sqlCon.Open()
                sqlComm.ExecuteNonQuery()
                Return 1
            End Using

        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Shared Function RunSQLStmt(stmt As String) As Boolean
        Try
            If stmt <> "" Then 'And Right() <> "" Then
                Dim sqlConnection1 As New SqlConnection(cstrconnection2)
                Dim cmd2 As New SqlCommand
                Dim reader As SqlDataReader

                cmd2.CommandText = stmt
                cmd2.CommandType = CommandType.Text
                cmd2.Connection = sqlConnection1

                sqlConnection1.Open()

                reader = cmd2.ExecuteReader()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try


        ' Data is accessible through the DataReader object here.
        'While reader.Read()
        ' Console.WriteLine(reader("UserSignOnID"))
        'End While

    End Function
    Public Shared Sub LoadDGVfromSQL(control1 As DataGridView, SQL As String)

        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = SQL '& Sqlwhere
        'If MaskedTextBox1.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE State = '" & Trim(MaskedTextBox1.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        control1.DataSource = dtRecord
    End Sub
    Public Shared Sub LoadDGVfromSP(control1 As DataGridView, SQL As String)

        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.StoredProcedure
        sqlCmd.CommandText = SQL '& Sqlwhere
        'If MaskedTextBox1.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE State = '" & Trim(MaskedTextBox1.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        control1.DataSource = dtRecord
    End Sub
    Public Shared Function LoadAndReturnDataTable(SQL As String) As DataTable
        'Using connection As New SqlConnection(cstrconnection)
        '    Dim command As New SqlCommand(SQL, connection)
        '    command.Connection.Open()
        '    SqlDataReader reader = command.ExecuteReader()

        'End Using
        Try


            Dim Con = New OleDbConnection(cstrconnection)
            Con.Open() 'Open the connection
            Dim Cmd As New OleDbCommand(SQL, Con)

            Cmd.CommandType = CommandType.Text
            Dim Rdr As OleDbDataReader = Cmd.ExecuteReader()
            Dim schemaTable As DataTable = Rdr.GetSchemaTable()
            Return schemaTable
        Catch ex As Exception
            Return Nothing
        End Try
        'Dim row As DataRow
        'Dim column As DataColumn

        'For Each row In schemaTable.Rows
        '    For Each column In schemaTable.Columns
        '        If row.Item("fDateAdded") = "" Then '* your match criteria* Then
        '            'row.Item("fName")
        '        End If
        '    Next
        'Next

    End Function
    Public Shared Sub WriteSQLToExcel(SQL As String)
        Dim cnn As SqlConnection
        'Dim connectionString As String
        Dim i, j As Integer

        '        Dim xlApp As Excel.Application
        '        Dim xlWorkBook As Excel.Workbook
        '        Dim xlWorkSheet As Excel.Worksheet
        '       Dim misValue As Object = System.Reflection.Missing.Value

        '      xlApp = New Excel.ApplicationClass
        '      xlWorkBook = xlApp.Workbooks.Add(misValue)
        '     xlWorkSheet = xlWorkBook.Sheets("sheet1")

        'connectionString = "data source=servername;" "initial catalog=databasename;user id=username;password=password;"
        cnn = New SqlConnection(cstrconnection)
        cnn.Open()

        Dim dscmd As New SqlDataAdapter(SQL, cnn)
        Dim ds As New DataSet
        dscmd.Fill(ds)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            For j = 0 To ds.Tables(0).Columns.Count - 1
                '        xlWorkSheet.Cells(i + 1, j + 1) =
                'ds.Tables(0).Rows(i).Item(j)
            Next
        Next

        'xlWorkSheet.SaveAs(My.Application.Info.DirectoryPath & "vbexcel.xlsx")
        'xlWorkBook.Close()
        'xlApp.Quit()

        'releaseObject(xlApp)
        'releaseObject(xlWorkBook)
        'releaseObject(xlWorkSheet)

        cnn.Close()

        MsgBox("You can find the file " & My.Application.Info.DirectoryPath & "\vbexcel.xlsx")
    End Sub

    Public Shared Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Public Shared Function ifTableExists(tblName As String) As Boolean

        If DCountVB("[Name]", "SysObjects", "[Name] = '" & tblName & "'") = 1 Then
            ifTableExists = True
        Else
            ifTableExists = False
        End If
    End Function
End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormStartUp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnMaintainGPJurisdictions = New System.Windows.Forms.Button()
        Me.btnMaintainStates = New System.Windows.Forms.Button()
        Me.btnMaintainFilingEntities = New System.Windows.Forms.Button()
        Me.btnMaintainContacts = New System.Windows.Forms.Button()
        Me.btnMaintainDivision = New System.Windows.Forms.Button()
        Me.btnMaintainUsers = New System.Windows.Forms.Button()
        Me.btnMaintainuserGroups = New System.Windows.Forms.Button()
        Me.btnMaintainWebInterfaceTables = New System.Windows.Forms.Button()
        Me.btnMaintainVertexMappingTables = New System.Windows.Forms.Button()
        Me.btnMaintainDeterminationMapping = New System.Windows.Forms.Button()
        Me.btnMaintainAR027Mapping = New System.Windows.Forms.Button()
        Me.btnMaintainJurisdictionXRef = New System.Windows.Forms.Button()
        Me.btnMaintainRIABinders = New System.Windows.Forms.Button()
        Me.btnMaintainRIABinderStateReturn = New System.Windows.Forms.Button()
        Me.btnMaintainAccountsToExlude = New System.Windows.Forms.Button()
        Me.btnMaintainLocationCodeOveride = New System.Windows.Forms.Button()
        Me.btnMaintainGroup1ReportingGroups = New System.Windows.Forms.Button()
        Me.btnViewSystemIntegrityErrors = New System.Windows.Forms.Button()
        Me.btnViewJurisdictionChecks = New System.Windows.Forms.Button()
        Me.btnYearEndMaintenance = New System.Windows.Forms.Button()
        Me.btnMaintainReferenceTables = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnMaintainGPJurisdictions
        '
        Me.btnMaintainGPJurisdictions.Location = New System.Drawing.Point(12, 12)
        Me.btnMaintainGPJurisdictions.Name = "btnMaintainGPJurisdictions"
        Me.btnMaintainGPJurisdictions.Size = New System.Drawing.Size(171, 26)
        Me.btnMaintainGPJurisdictions.TabIndex = 0
        Me.btnMaintainGPJurisdictions.Text = "Maintain GP Jurisdictions"
        Me.btnMaintainGPJurisdictions.UseVisualStyleBackColor = True
        '
        'btnMaintainStates
        '
        Me.btnMaintainStates.Location = New System.Drawing.Point(12, 44)
        Me.btnMaintainStates.Name = "btnMaintainStates"
        Me.btnMaintainStates.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainStates.TabIndex = 1
        Me.btnMaintainStates.Text = "Maintain States"
        Me.btnMaintainStates.UseVisualStyleBackColor = True
        '
        'btnMaintainFilingEntities
        '
        Me.btnMaintainFilingEntities.Location = New System.Drawing.Point(12, 75)
        Me.btnMaintainFilingEntities.Name = "btnMaintainFilingEntities"
        Me.btnMaintainFilingEntities.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainFilingEntities.TabIndex = 2
        Me.btnMaintainFilingEntities.Text = "Maintain Filing Entities"
        Me.btnMaintainFilingEntities.UseVisualStyleBackColor = True
        '
        'btnMaintainContacts
        '
        Me.btnMaintainContacts.Location = New System.Drawing.Point(12, 106)
        Me.btnMaintainContacts.Name = "btnMaintainContacts"
        Me.btnMaintainContacts.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainContacts.TabIndex = 3
        Me.btnMaintainContacts.Text = "Maintain Contacts"
        Me.btnMaintainContacts.UseVisualStyleBackColor = True
        '
        'btnMaintainDivision
        '
        Me.btnMaintainDivision.Location = New System.Drawing.Point(12, 137)
        Me.btnMaintainDivision.Name = "btnMaintainDivision"
        Me.btnMaintainDivision.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainDivision.TabIndex = 4
        Me.btnMaintainDivision.Text = "Maintain Division"
        Me.btnMaintainDivision.UseVisualStyleBackColor = True
        '
        'btnMaintainUsers
        '
        Me.btnMaintainUsers.Location = New System.Drawing.Point(12, 168)
        Me.btnMaintainUsers.Name = "btnMaintainUsers"
        Me.btnMaintainUsers.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainUsers.TabIndex = 5
        Me.btnMaintainUsers.Text = "Maintain Users"
        Me.btnMaintainUsers.UseVisualStyleBackColor = True
        '
        'btnMaintainuserGroups
        '
        Me.btnMaintainuserGroups.Location = New System.Drawing.Point(12, 199)
        Me.btnMaintainuserGroups.Name = "btnMaintainuserGroups"
        Me.btnMaintainuserGroups.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainuserGroups.TabIndex = 6
        Me.btnMaintainuserGroups.Text = "Maintain User Groups"
        Me.btnMaintainuserGroups.UseVisualStyleBackColor = True
        '
        'btnMaintainWebInterfaceTables
        '
        Me.btnMaintainWebInterfaceTables.Location = New System.Drawing.Point(12, 230)
        Me.btnMaintainWebInterfaceTables.Name = "btnMaintainWebInterfaceTables"
        Me.btnMaintainWebInterfaceTables.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainWebInterfaceTables.TabIndex = 7
        Me.btnMaintainWebInterfaceTables.Text = "Maintain Web Interface Tables"
        Me.btnMaintainWebInterfaceTables.UseVisualStyleBackColor = True
        '
        'btnMaintainVertexMappingTables
        '
        Me.btnMaintainVertexMappingTables.Location = New System.Drawing.Point(12, 261)
        Me.btnMaintainVertexMappingTables.Name = "btnMaintainVertexMappingTables"
        Me.btnMaintainVertexMappingTables.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainVertexMappingTables.TabIndex = 8
        Me.btnMaintainVertexMappingTables.Text = "MaintainVertexMappingTables"
        Me.btnMaintainVertexMappingTables.UseVisualStyleBackColor = True
        '
        'btnMaintainDeterminationMapping
        '
        Me.btnMaintainDeterminationMapping.Location = New System.Drawing.Point(12, 292)
        Me.btnMaintainDeterminationMapping.Name = "btnMaintainDeterminationMapping"
        Me.btnMaintainDeterminationMapping.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainDeterminationMapping.TabIndex = 9
        Me.btnMaintainDeterminationMapping.Text = "Maintain Determination Mapping"
        Me.btnMaintainDeterminationMapping.UseVisualStyleBackColor = True
        '
        'btnMaintainAR027Mapping
        '
        Me.btnMaintainAR027Mapping.Location = New System.Drawing.Point(12, 323)
        Me.btnMaintainAR027Mapping.Name = "btnMaintainAR027Mapping"
        Me.btnMaintainAR027Mapping.Size = New System.Drawing.Size(171, 25)
        Me.btnMaintainAR027Mapping.TabIndex = 10
        Me.btnMaintainAR027Mapping.Text = "Maintain AR027 Mapping"
        Me.btnMaintainAR027Mapping.UseVisualStyleBackColor = True
        '
        'btnMaintainJurisdictionXRef
        '
        Me.btnMaintainJurisdictionXRef.Location = New System.Drawing.Point(189, 12)
        Me.btnMaintainJurisdictionXRef.Name = "btnMaintainJurisdictionXRef"
        Me.btnMaintainJurisdictionXRef.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainJurisdictionXRef.TabIndex = 11
        Me.btnMaintainJurisdictionXRef.Text = "Maintain Jurisdiction XRef"
        Me.btnMaintainJurisdictionXRef.UseVisualStyleBackColor = True
        '
        'btnMaintainRIABinders
        '
        Me.btnMaintainRIABinders.Location = New System.Drawing.Point(189, 44)
        Me.btnMaintainRIABinders.Name = "btnMaintainRIABinders"
        Me.btnMaintainRIABinders.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainRIABinders.TabIndex = 12
        Me.btnMaintainRIABinders.Text = "Maintain RIABinders"
        Me.btnMaintainRIABinders.UseVisualStyleBackColor = True
        '
        'btnMaintainRIABinderStateReturn
        '
        Me.btnMaintainRIABinderStateReturn.Location = New System.Drawing.Point(189, 75)
        Me.btnMaintainRIABinderStateReturn.Name = "btnMaintainRIABinderStateReturn"
        Me.btnMaintainRIABinderStateReturn.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainRIABinderStateReturn.TabIndex = 13
        Me.btnMaintainRIABinderStateReturn.Text = "Maintain RIABinder State Return"
        Me.btnMaintainRIABinderStateReturn.UseVisualStyleBackColor = True
        '
        'btnMaintainAccountsToExlude
        '
        Me.btnMaintainAccountsToExlude.Location = New System.Drawing.Point(189, 106)
        Me.btnMaintainAccountsToExlude.Name = "btnMaintainAccountsToExlude"
        Me.btnMaintainAccountsToExlude.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainAccountsToExlude.TabIndex = 14
        Me.btnMaintainAccountsToExlude.Text = "Maintain Accounts to Exlude"
        Me.btnMaintainAccountsToExlude.UseVisualStyleBackColor = True
        '
        'btnMaintainLocationCodeOveride
        '
        Me.btnMaintainLocationCodeOveride.Location = New System.Drawing.Point(189, 137)
        Me.btnMaintainLocationCodeOveride.Name = "btnMaintainLocationCodeOveride"
        Me.btnMaintainLocationCodeOveride.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainLocationCodeOveride.TabIndex = 15
        Me.btnMaintainLocationCodeOveride.Text = "Maintain Location Code Overide"
        Me.btnMaintainLocationCodeOveride.UseVisualStyleBackColor = True
        '
        'btnMaintainGroup1ReportingGroups
        '
        Me.btnMaintainGroup1ReportingGroups.Location = New System.Drawing.Point(189, 168)
        Me.btnMaintainGroup1ReportingGroups.Name = "btnMaintainGroup1ReportingGroups"
        Me.btnMaintainGroup1ReportingGroups.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainGroup1ReportingGroups.TabIndex = 16
        Me.btnMaintainGroup1ReportingGroups.Text = "Maintaning Group1 Reporting Groups"
        Me.btnMaintainGroup1ReportingGroups.UseVisualStyleBackColor = True
        '
        'btnViewSystemIntegrityErrors
        '
        Me.btnViewSystemIntegrityErrors.Location = New System.Drawing.Point(189, 199)
        Me.btnViewSystemIntegrityErrors.Name = "btnViewSystemIntegrityErrors"
        Me.btnViewSystemIntegrityErrors.Size = New System.Drawing.Size(205, 25)
        Me.btnViewSystemIntegrityErrors.TabIndex = 17
        Me.btnViewSystemIntegrityErrors.Text = "View System Integrity Errors"
        Me.btnViewSystemIntegrityErrors.UseVisualStyleBackColor = True
        '
        'btnViewJurisdictionChecks
        '
        Me.btnViewJurisdictionChecks.Location = New System.Drawing.Point(189, 230)
        Me.btnViewJurisdictionChecks.Name = "btnViewJurisdictionChecks"
        Me.btnViewJurisdictionChecks.Size = New System.Drawing.Size(205, 25)
        Me.btnViewJurisdictionChecks.TabIndex = 18
        Me.btnViewJurisdictionChecks.Text = "View Jurisdiction Checks"
        Me.btnViewJurisdictionChecks.UseVisualStyleBackColor = True
        '
        'btnYearEndMaintenance
        '
        Me.btnYearEndMaintenance.Location = New System.Drawing.Point(189, 261)
        Me.btnYearEndMaintenance.Name = "btnYearEndMaintenance"
        Me.btnYearEndMaintenance.Size = New System.Drawing.Size(205, 25)
        Me.btnYearEndMaintenance.TabIndex = 19
        Me.btnYearEndMaintenance.Text = "Year End Maintenance"
        Me.btnYearEndMaintenance.UseVisualStyleBackColor = True
        '
        'btnMaintainReferenceTables
        '
        Me.btnMaintainReferenceTables.Location = New System.Drawing.Point(189, 292)
        Me.btnMaintainReferenceTables.Name = "btnMaintainReferenceTables"
        Me.btnMaintainReferenceTables.Size = New System.Drawing.Size(205, 25)
        Me.btnMaintainReferenceTables.TabIndex = 20
        Me.btnMaintainReferenceTables.Text = "Maintain Reference Tables"
        Me.btnMaintainReferenceTables.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(397, 325)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 21
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'FormStartUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(476, 358)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnMaintainReferenceTables)
        Me.Controls.Add(Me.btnYearEndMaintenance)
        Me.Controls.Add(Me.btnViewJurisdictionChecks)
        Me.Controls.Add(Me.btnViewSystemIntegrityErrors)
        Me.Controls.Add(Me.btnMaintainGroup1ReportingGroups)
        Me.Controls.Add(Me.btnMaintainLocationCodeOveride)
        Me.Controls.Add(Me.btnMaintainAccountsToExlude)
        Me.Controls.Add(Me.btnMaintainRIABinderStateReturn)
        Me.Controls.Add(Me.btnMaintainRIABinders)
        Me.Controls.Add(Me.btnMaintainJurisdictionXRef)
        Me.Controls.Add(Me.btnMaintainAR027Mapping)
        Me.Controls.Add(Me.btnMaintainDeterminationMapping)
        Me.Controls.Add(Me.btnMaintainVertexMappingTables)
        Me.Controls.Add(Me.btnMaintainWebInterfaceTables)
        Me.Controls.Add(Me.btnMaintainuserGroups)
        Me.Controls.Add(Me.btnMaintainUsers)
        Me.Controls.Add(Me.btnMaintainDivision)
        Me.Controls.Add(Me.btnMaintainContacts)
        Me.Controls.Add(Me.btnMaintainFilingEntities)
        Me.Controls.Add(Me.btnMaintainStates)
        Me.Controls.Add(Me.btnMaintainGPJurisdictions)
        Me.Name = "FormStartUp"
        Me.Text = "M"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnMaintainGPJurisdictions As Button
    Friend WithEvents btnMaintainStates As Button
    Friend WithEvents btnMaintainFilingEntities As Button
    Friend WithEvents btnMaintainContacts As Button
    Friend WithEvents btnMaintainDivision As Button
    Friend WithEvents btnMaintainUsers As Button
    Friend WithEvents btnMaintainuserGroups As Button
    Friend WithEvents btnMaintainWebInterfaceTables As Button
    Friend WithEvents btnMaintainVertexMappingTables As Button
    Friend WithEvents btnMaintainDeterminationMapping As Button
    Friend WithEvents btnMaintainAR027Mapping As Button
    Friend WithEvents btnMaintainJurisdictionXRef As Button
    Friend WithEvents btnMaintainRIABinders As Button
    Friend WithEvents btnMaintainRIABinderStateReturn As Button
    Friend WithEvents btnMaintainAccountsToExlude As Button
    Friend WithEvents btnMaintainLocationCodeOveride As Button
    Friend WithEvents btnMaintainGroup1ReportingGroups As Button
    Friend WithEvents btnViewSystemIntegrityErrors As Button
    Friend WithEvents btnViewJurisdictionChecks As Button
    Friend WithEvents btnYearEndMaintenance As Button
    Friend WithEvents btnMaintainReferenceTables As Button
    Friend WithEvents btnClose As Button
End Class

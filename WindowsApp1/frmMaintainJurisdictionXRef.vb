﻿Imports System.Data.SqlClient

Public Class frmMaintainJurisdictionXRef
    Private Sub JurisdictionXRefBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainJurisdictionXRef)

    End Sub

    Private Sub frmMaintainJurisdictionXRef_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainJurisdictionXRef.JurisdictionXRefOverride' table. You can move, or remove it, as needed.
        'Me.JurisdictionXRefOverrideTableAdapter.Fill(Me.DataSource_MaintainJurisdictionXRef.JurisdictionXRefOverride, cboPeriod.Text)
        'TODO: This line of code loads data into the 'DataSource_MaintainJurisdictionXRef.Jurisdiction' table. You can move, or remove it, as needed.
        LoadComboBox()
        LoadDGV_SourceJurisdictionListing()
        LoadDGV_SourceJurisdictionOverides()
    End Sub

    Private Sub LoadComboBox()
        Dim sqlquery As String
        sqlquery = "SELECT Period, CurrentPeriodFlag FROM Period with (NOLOCK) ORDER BY Period Desc"
        Using connection As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(sqlquery, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                ' as an example set the ValueMember and DisplayMember'
                ' to two columns of the returned table'
                cboPeriod.ValueMember = "Period"
                cboPeriod.DisplayMember = "Period"
                cboPeriod.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub LoadDGV_SourceJurisdictionListing()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM JurisdictionXRef with (NOLOCK)"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " WHERE PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_SourceJurisdictionListing.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_SourceJurisdictionOverides()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM JurisdictionXRefOverride with (NOLOCK)"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " WHERE PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_SourceJurisdictionOverides.DataSource = dtRecord
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriod.SelectedIndexChanged
        cboPeriod.Text = cboPeriod.SelectedValue
        LoadDGV_SourceJurisdictionListing()
        LoadDGV_SourceJurisdictionOverides()
        Me.JurisdictionXRefTableAdapter.Fill(Me.DataSource_MaintainJurisdictionXRef.JurisdictionXRef, cboPeriod.SelectedValue)
        Me.JurisdictionTableAdapter.Fill(Me.DataSource_MaintainJurisdictionXRef.Jurisdiction, cboPeriod.Text)
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainAccountsToExclude
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.DGV_MaintainAccountsToExclude = New System.Windows.Forms.DataGridView()
        Me.cboPeriod = New System.Windows.Forms.ComboBox()
        Me.lblPeriod = New System.Windows.Forms.Label()
        CType(Me.DGV_MaintainAccountsToExclude, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(752, 455)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DGV_MaintainAccountsToExclude
        '
        Me.DGV_MaintainAccountsToExclude.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_MaintainAccountsToExclude.Location = New System.Drawing.Point(12, 42)
        Me.DGV_MaintainAccountsToExclude.Name = "DGV_MaintainAccountsToExclude"
        Me.DGV_MaintainAccountsToExclude.Size = New System.Drawing.Size(720, 436)
        Me.DGV_MaintainAccountsToExclude.TabIndex = 5
        '
        'cboPeriod
        '
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(66, 12)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 6
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(12, 15)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(37, 13)
        Me.lblPeriod.TabIndex = 7
        Me.lblPeriod.Text = "Period"
        '
        'frmMaintainAccountsToExclude
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 490)
        Me.Controls.Add(Me.lblPeriod)
        Me.Controls.Add(Me.cboPeriod)
        Me.Controls.Add(Me.DGV_MaintainAccountsToExclude)
        Me.Controls.Add(Me.btnClose)
        Me.Name = "frmMaintainAccountsToExclude"
        Me.Text = "Maintain Accounts To Exclude"
        CType(Me.DGV_MaintainAccountsToExclude, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As Button
    Friend WithEvents DGV_MaintainAccountsToExclude As DataGridView
    Friend WithEvents cboPeriod As ComboBox
    Friend WithEvents lblPeriod As Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMaintainJurisdiction
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim JurisdictionKeyLabel As System.Windows.Forms.Label
        Dim PeriodIDLabel As System.Windows.Forms.Label
        Dim StateLabel As System.Windows.Forms.Label
        Dim GPJurisDescriptionLabel As System.Windows.Forms.Label
        Dim CountyNameLabel As System.Windows.Forms.Label
        Dim ZipCodeLabel As System.Windows.Forms.Label
        Dim ZipExtensionLabel As System.Windows.Forms.Label
        Dim ExcludeCountyLabel As System.Windows.Forms.Label
        Dim ExcludeCityLabel As System.Windows.Forms.Label
        Dim ExcludeDistrictLabel As System.Windows.Forms.Label
        Dim DiscreteStateLabel As System.Windows.Forms.Label
        Dim ExcludeStateLabel As System.Windows.Forms.Label
        Dim DiscreteLocalsLabel As System.Windows.Forms.Label
        Dim HandlingCodeLabel As System.Windows.Forms.Label
        Dim ReportGrossSalesLabel As System.Windows.Forms.Label
        Dim ChangeByLabel As System.Windows.Forms.Label
        Dim ChangeDateLabel As System.Windows.Forms.Label
        Dim RIARecordOverrideLabel As System.Windows.Forms.Label
        Dim RIANoState_CalculationLabel As System.Windows.Forms.Label
        Dim RIAMultipleZipsLabel As System.Windows.Forms.Label
        Dim VtGeoCodeLabel As System.Windows.Forms.Label
        Dim DefaultForZipLabel As System.Windows.Forms.Label
        Dim DefaultforGEOLabel As System.Windows.Forms.Label
        Dim ZipCountyLabel As System.Windows.Forms.Label
        Dim ZipLocalLabel As System.Windows.Forms.Label
        Dim ZipTransitLabel As System.Windows.Forms.Label
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabJurisdictionListing = New System.Windows.Forms.TabPage()
        Me.JurisdictionDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn4 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn5 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn6 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JurisdictionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SalesUse_UniDataSet = New WindowsApp1.SalesUse_UniDataSet()
        Me.TabJurisdictionDetail = New System.Windows.Forms.TabPage()
        Me.TabJursidictionCrossRef = New System.Windows.Forms.TabPage()
        Me.TabCrossRefOverides = New System.Windows.Forms.TabPage()
        Me.JurisdictionTableAdapter = New WindowsApp1.SalesUse_UniDataSetTableAdapters.JurisdictionTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.SalesUse_UniDataSetTableAdapters.TableAdapterManager()
        Me.PeriodBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PeriodBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PeriodBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PeriodBindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PeriodBindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PeriodBindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.cboPeriodID = New System.Windows.Forms.ComboBox()
        Me.JurisdictionXRefBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JurisdictionXRefTableAdapter = New WindowsApp1.SalesUse_UniDataSetTableAdapters.JurisdictionXRefTableAdapter()
        Me.JurisdictionKeyTextBox = New System.Windows.Forms.TextBox()
        Me.PeriodIDTextBox = New System.Windows.Forms.TextBox()
        Me.StateTextBox = New System.Windows.Forms.TextBox()
        Me.GPJurisDescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.CountyNameTextBox = New System.Windows.Forms.TextBox()
        Me.ZipCodeTextBox = New System.Windows.Forms.TextBox()
        Me.ZipExtensionTextBox = New System.Windows.Forms.TextBox()
        Me.ExcludeCountyTextBox = New System.Windows.Forms.TextBox()
        Me.ExcludeCityTextBox = New System.Windows.Forms.TextBox()
        Me.JurisdictionXRefDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn7 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JurisdictionXRefOverrideBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JurisdictionXRefOverrideTableAdapter = New WindowsApp1.SalesUse_UniDataSetTableAdapters.JurisdictionXRefOverrideTableAdapter()
        Me.JurisdictionXRefOverrideDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LabelPeriod = New System.Windows.Forms.Label()
        Me.ExcludeDistrictTextBox = New System.Windows.Forms.TextBox()
        Me.DiscreteStateCheckBox = New System.Windows.Forms.CheckBox()
        Me.ExcludeStateTextBox = New System.Windows.Forms.TextBox()
        Me.DiscreteLocalsCheckBox = New System.Windows.Forms.CheckBox()
        Me.HandlingCodeTextBox = New System.Windows.Forms.TextBox()
        Me.ReportGrossSalesCheckBox = New System.Windows.Forms.CheckBox()
        Me.ChangeByTextBox = New System.Windows.Forms.TextBox()
        Me.ChangeDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.RIARecordOverrideTextBox = New System.Windows.Forms.TextBox()
        Me.RIANoState_CalculationTextBox = New System.Windows.Forms.TextBox()
        Me.RIAMultipleZipsTextBox = New System.Windows.Forms.TextBox()
        Me.VtGeoCodeTextBox = New System.Windows.Forms.TextBox()
        Me.DefaultForZipCheckBox = New System.Windows.Forms.CheckBox()
        Me.DefaultforGEOCheckBox = New System.Windows.Forms.CheckBox()
        Me.ZipCountyTextBox = New System.Windows.Forms.TextBox()
        Me.ZipLocalTextBox = New System.Windows.Forms.TextBox()
        Me.ZipTransitTextBox = New System.Windows.Forms.TextBox()
        JurisdictionKeyLabel = New System.Windows.Forms.Label()
        PeriodIDLabel = New System.Windows.Forms.Label()
        StateLabel = New System.Windows.Forms.Label()
        GPJurisDescriptionLabel = New System.Windows.Forms.Label()
        CountyNameLabel = New System.Windows.Forms.Label()
        ZipCodeLabel = New System.Windows.Forms.Label()
        ZipExtensionLabel = New System.Windows.Forms.Label()
        ExcludeCountyLabel = New System.Windows.Forms.Label()
        ExcludeCityLabel = New System.Windows.Forms.Label()
        ExcludeDistrictLabel = New System.Windows.Forms.Label()
        DiscreteStateLabel = New System.Windows.Forms.Label()
        ExcludeStateLabel = New System.Windows.Forms.Label()
        DiscreteLocalsLabel = New System.Windows.Forms.Label()
        HandlingCodeLabel = New System.Windows.Forms.Label()
        ReportGrossSalesLabel = New System.Windows.Forms.Label()
        ChangeByLabel = New System.Windows.Forms.Label()
        ChangeDateLabel = New System.Windows.Forms.Label()
        RIARecordOverrideLabel = New System.Windows.Forms.Label()
        RIANoState_CalculationLabel = New System.Windows.Forms.Label()
        RIAMultipleZipsLabel = New System.Windows.Forms.Label()
        VtGeoCodeLabel = New System.Windows.Forms.Label()
        DefaultForZipLabel = New System.Windows.Forms.Label()
        DefaultforGEOLabel = New System.Windows.Forms.Label()
        ZipCountyLabel = New System.Windows.Forms.Label()
        ZipLocalLabel = New System.Windows.Forms.Label()
        ZipTransitLabel = New System.Windows.Forms.Label()
        Me.TabControl.SuspendLayout()
        Me.TabJurisdictionListing.SuspendLayout()
        CType(Me.JurisdictionDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JurisdictionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SalesUse_UniDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabJurisdictionDetail.SuspendLayout()
        Me.TabJursidictionCrossRef.SuspendLayout()
        Me.TabCrossRefOverides.SuspendLayout()
        CType(Me.PeriodBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodBindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodBindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodBindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JurisdictionXRefBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JurisdictionXRefDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JurisdictionXRefOverrideBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JurisdictionXRefOverrideDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabJurisdictionListing)
        Me.TabControl.Controls.Add(Me.TabJurisdictionDetail)
        Me.TabControl.Controls.Add(Me.TabJursidictionCrossRef)
        Me.TabControl.Controls.Add(Me.TabCrossRefOverides)
        Me.TabControl.Location = New System.Drawing.Point(12, 30)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(993, 401)
        Me.TabControl.TabIndex = 0
        Me.TabControl.Tag = ""
        '
        'TabJurisdictionListing
        '
        Me.TabJurisdictionListing.AutoScroll = True
        Me.TabJurisdictionListing.Controls.Add(Me.JurisdictionDataGridView)
        Me.TabJurisdictionListing.Location = New System.Drawing.Point(4, 22)
        Me.TabJurisdictionListing.Name = "TabJurisdictionListing"
        Me.TabJurisdictionListing.Padding = New System.Windows.Forms.Padding(3)
        Me.TabJurisdictionListing.Size = New System.Drawing.Size(985, 375)
        Me.TabJurisdictionListing.TabIndex = 0
        Me.TabJurisdictionListing.Text = "Jurisdiction_Listing"
        Me.TabJurisdictionListing.UseVisualStyleBackColor = True
        '
        'JurisdictionDataGridView
        '
        Me.JurisdictionDataGridView.AutoGenerateColumns = False
        Me.JurisdictionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.JurisdictionDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewCheckBoxColumn2, Me.DataGridViewCheckBoxColumn3, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewCheckBoxColumn4, Me.DataGridViewCheckBoxColumn5, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewCheckBoxColumn6, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22})
        Me.JurisdictionDataGridView.DataSource = Me.JurisdictionBindingSource
        Me.JurisdictionDataGridView.Location = New System.Drawing.Point(5, 3)
        Me.JurisdictionDataGridView.Name = "JurisdictionDataGridView"
        Me.JurisdictionDataGridView.Size = New System.Drawing.Size(974, 366)
        Me.JurisdictionDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "JurisdictionKey"
        Me.DataGridViewTextBoxColumn1.HeaderText = "JurisdictionKey"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "PeriodID"
        Me.DataGridViewTextBoxColumn2.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "State"
        Me.DataGridViewTextBoxColumn3.HeaderText = "State"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "GPJurisDescription"
        Me.DataGridViewTextBoxColumn4.HeaderText = "GPJurisDescription"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "ZipCounty"
        Me.DataGridViewTextBoxColumn5.HeaderText = "ZipCounty"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "ZipLocal"
        Me.DataGridViewTextBoxColumn6.HeaderText = "ZipLocal"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "ZipTransit"
        Me.DataGridViewTextBoxColumn7.HeaderText = "ZipTransit"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "CountyName"
        Me.DataGridViewTextBoxColumn8.HeaderText = "CountyName"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "LocationName"
        Me.DataGridViewTextBoxColumn9.HeaderText = "LocationName"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "DefaultZipForSales"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "DefaultZipForSales"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "RIARecordOverride"
        Me.DataGridViewTextBoxColumn10.HeaderText = "RIARecordOverride"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "RIANoState_Calculation"
        Me.DataGridViewTextBoxColumn11.HeaderText = "RIANoState_Calculation"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "RIAMultipleZips"
        Me.DataGridViewTextBoxColumn12.HeaderText = "RIAMultipleZips"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "DiscreteState"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "DiscreteState"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        '
        'DataGridViewCheckBoxColumn3
        '
        Me.DataGridViewCheckBoxColumn3.DataPropertyName = "DiscreteLocals"
        Me.DataGridViewCheckBoxColumn3.HeaderText = "DiscreteLocals"
        Me.DataGridViewCheckBoxColumn3.Name = "DataGridViewCheckBoxColumn3"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "HandlingCode"
        Me.DataGridViewTextBoxColumn13.HeaderText = "HandlingCode"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "vtGeoCode"
        Me.DataGridViewTextBoxColumn14.HeaderText = "vtGeoCode"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewCheckBoxColumn4
        '
        Me.DataGridViewCheckBoxColumn4.DataPropertyName = "DefaultForZip"
        Me.DataGridViewCheckBoxColumn4.HeaderText = "DefaultForZip"
        Me.DataGridViewCheckBoxColumn4.Name = "DataGridViewCheckBoxColumn4"
        '
        'DataGridViewCheckBoxColumn5
        '
        Me.DataGridViewCheckBoxColumn5.DataPropertyName = "DefaultforGEO"
        Me.DataGridViewCheckBoxColumn5.HeaderText = "DefaultforGEO"
        Me.DataGridViewCheckBoxColumn5.Name = "DataGridViewCheckBoxColumn5"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "ChangeBy"
        Me.DataGridViewTextBoxColumn15.HeaderText = "ChangeBy"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "ChangeDate"
        Me.DataGridViewTextBoxColumn16.HeaderText = "ChangeDate"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewCheckBoxColumn6
        '
        Me.DataGridViewCheckBoxColumn6.DataPropertyName = "ReportGrossSales"
        Me.DataGridViewCheckBoxColumn6.HeaderText = "ReportGrossSales"
        Me.DataGridViewCheckBoxColumn6.Name = "DataGridViewCheckBoxColumn6"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "ZipCode"
        Me.DataGridViewTextBoxColumn17.HeaderText = "ZipCode"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "ZipExtension"
        Me.DataGridViewTextBoxColumn18.HeaderText = "ZipExtension"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "ExcludeState"
        Me.DataGridViewTextBoxColumn19.HeaderText = "ExcludeState"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "ExcludeCounty"
        Me.DataGridViewTextBoxColumn20.HeaderText = "ExcludeCounty"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "ExcludeCity"
        Me.DataGridViewTextBoxColumn21.HeaderText = "ExcludeCity"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "ExcludeDistrict"
        Me.DataGridViewTextBoxColumn22.HeaderText = "ExcludeDistrict"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'JurisdictionBindingSource
        '
        Me.JurisdictionBindingSource.DataMember = "Jurisdiction"
        Me.JurisdictionBindingSource.DataSource = Me.SalesUse_UniDataSet
        '
        'SalesUse_UniDataSet
        '
        Me.SalesUse_UniDataSet.DataSetName = "SalesUse_UniDataSet"
        Me.SalesUse_UniDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabJurisdictionDetail
        '
        Me.TabJurisdictionDetail.AutoScroll = True
        Me.TabJurisdictionDetail.Controls.Add(ZipTransitLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ZipTransitTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ZipLocalLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ZipLocalTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ZipCountyLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ZipCountyTextBox)
        Me.TabJurisdictionDetail.Controls.Add(DefaultforGEOLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.DefaultforGEOCheckBox)
        Me.TabJurisdictionDetail.Controls.Add(DefaultForZipLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.DefaultForZipCheckBox)
        Me.TabJurisdictionDetail.Controls.Add(VtGeoCodeLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.VtGeoCodeTextBox)
        Me.TabJurisdictionDetail.Controls.Add(RIAMultipleZipsLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.RIAMultipleZipsTextBox)
        Me.TabJurisdictionDetail.Controls.Add(RIANoState_CalculationLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.RIANoState_CalculationTextBox)
        Me.TabJurisdictionDetail.Controls.Add(RIARecordOverrideLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.RIARecordOverrideTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ChangeDateLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ChangeDateDateTimePicker)
        Me.TabJurisdictionDetail.Controls.Add(ChangeByLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ChangeByTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ReportGrossSalesLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ReportGrossSalesCheckBox)
        Me.TabJurisdictionDetail.Controls.Add(HandlingCodeLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.HandlingCodeTextBox)
        Me.TabJurisdictionDetail.Controls.Add(DiscreteLocalsLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.DiscreteLocalsCheckBox)
        Me.TabJurisdictionDetail.Controls.Add(ExcludeStateLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ExcludeStateTextBox)
        Me.TabJurisdictionDetail.Controls.Add(DiscreteStateLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.DiscreteStateCheckBox)
        Me.TabJurisdictionDetail.Controls.Add(ExcludeDistrictLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ExcludeDistrictTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ExcludeCityLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ExcludeCityTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ExcludeCountyLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ExcludeCountyTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ZipExtensionLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ZipExtensionTextBox)
        Me.TabJurisdictionDetail.Controls.Add(ZipCodeLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.ZipCodeTextBox)
        Me.TabJurisdictionDetail.Controls.Add(CountyNameLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.CountyNameTextBox)
        Me.TabJurisdictionDetail.Controls.Add(GPJurisDescriptionLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.GPJurisDescriptionTextBox)
        Me.TabJurisdictionDetail.Controls.Add(StateLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.StateTextBox)
        Me.TabJurisdictionDetail.Controls.Add(PeriodIDLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.PeriodIDTextBox)
        Me.TabJurisdictionDetail.Controls.Add(JurisdictionKeyLabel)
        Me.TabJurisdictionDetail.Controls.Add(Me.JurisdictionKeyTextBox)
        Me.TabJurisdictionDetail.Location = New System.Drawing.Point(4, 22)
        Me.TabJurisdictionDetail.Name = "TabJurisdictionDetail"
        Me.TabJurisdictionDetail.Padding = New System.Windows.Forms.Padding(3)
        Me.TabJurisdictionDetail.Size = New System.Drawing.Size(985, 375)
        Me.TabJurisdictionDetail.TabIndex = 1
        Me.TabJurisdictionDetail.Text = "Jurisdiction Detail"
        Me.TabJurisdictionDetail.UseVisualStyleBackColor = True
        '
        'TabJursidictionCrossRef
        '
        Me.TabJursidictionCrossRef.AutoScroll = True
        Me.TabJursidictionCrossRef.Controls.Add(Me.JurisdictionXRefDataGridView)
        Me.TabJursidictionCrossRef.Location = New System.Drawing.Point(4, 22)
        Me.TabJursidictionCrossRef.Name = "TabJursidictionCrossRef"
        Me.TabJursidictionCrossRef.Padding = New System.Windows.Forms.Padding(3)
        Me.TabJursidictionCrossRef.Size = New System.Drawing.Size(985, 375)
        Me.TabJursidictionCrossRef.TabIndex = 2
        Me.TabJursidictionCrossRef.Text = "Jurisdiction Cross Reference"
        Me.TabJursidictionCrossRef.UseVisualStyleBackColor = True
        '
        'TabCrossRefOverides
        '
        Me.TabCrossRefOverides.AutoScroll = True
        Me.TabCrossRefOverides.Controls.Add(Me.JurisdictionXRefOverrideDataGridView)
        Me.TabCrossRefOverides.Location = New System.Drawing.Point(4, 22)
        Me.TabCrossRefOverides.Name = "TabCrossRefOverides"
        Me.TabCrossRefOverides.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCrossRefOverides.Size = New System.Drawing.Size(985, 375)
        Me.TabCrossRefOverides.TabIndex = 3
        Me.TabCrossRefOverides.Text = "Cross Referene Overides"
        Me.TabCrossRefOverides.UseVisualStyleBackColor = True
        '
        'JurisdictionTableAdapter
        '
        Me.JurisdictionTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.JurisdictionTableAdapter = Me.JurisdictionTableAdapter
        Me.TableAdapterManager.JurisdictionXRefOverrideTableAdapter = Nothing
        Me.TableAdapterManager.JurisdictionXRefTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.SalesUse_UniDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'PeriodBindingSource
        '
        Me.PeriodBindingSource.DataSource = Me.SalesUse_UniDataSet
        Me.PeriodBindingSource.Position = 0
        '
        'PeriodBindingSource1
        '
        Me.PeriodBindingSource1.DataSource = Me.SalesUse_UniDataSet
        Me.PeriodBindingSource1.Position = 0
        '
        'PeriodBindingSource2
        '
        Me.PeriodBindingSource2.DataSource = Me.SalesUse_UniDataSet
        Me.PeriodBindingSource2.Position = 0
        '
        'PeriodBindingSource3
        '
        Me.PeriodBindingSource3.DataSource = Me.SalesUse_UniDataSet
        Me.PeriodBindingSource3.Position = 0
        '
        'PeriodBindingSource4
        '
        Me.PeriodBindingSource4.DataSource = Me.SalesUse_UniDataSet
        Me.PeriodBindingSource4.Position = 0
        '
        'PeriodBindingSource5
        '
        Me.PeriodBindingSource5.DataSource = Me.SalesUse_UniDataSet
        Me.PeriodBindingSource5.Position = 0
        '
        'cboPeriodID
        '
        Me.cboPeriodID.FormattingEnabled = True
        Me.cboPeriodID.Location = New System.Drawing.Point(56, 3)
        Me.cboPeriodID.Name = "cboPeriodID"
        Me.cboPeriodID.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriodID.TabIndex = 1
        '
        'JurisdictionXRefBindingSource
        '
        Me.JurisdictionXRefBindingSource.DataMember = "JurisdictionXRef"
        Me.JurisdictionXRefBindingSource.DataSource = Me.SalesUse_UniDataSet
        '
        'JurisdictionXRefTableAdapter
        '
        Me.JurisdictionXRefTableAdapter.ClearBeforeFill = True
        '
        'JurisdictionKeyLabel
        '
        JurisdictionKeyLabel.AutoSize = True
        JurisdictionKeyLabel.Location = New System.Drawing.Point(8, 9)
        JurisdictionKeyLabel.Name = "JurisdictionKeyLabel"
        JurisdictionKeyLabel.Size = New System.Drawing.Size(83, 13)
        JurisdictionKeyLabel.TabIndex = 0
        JurisdictionKeyLabel.Text = "Jurisdiction Key:"
        '
        'JurisdictionKeyTextBox
        '
        Me.JurisdictionKeyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "JurisdictionKey", True))
        Me.JurisdictionKeyTextBox.Location = New System.Drawing.Point(97, 6)
        Me.JurisdictionKeyTextBox.Name = "JurisdictionKeyTextBox"
        Me.JurisdictionKeyTextBox.Size = New System.Drawing.Size(100, 20)
        Me.JurisdictionKeyTextBox.TabIndex = 1
        '
        'PeriodIDLabel
        '
        PeriodIDLabel.AutoSize = True
        PeriodIDLabel.Location = New System.Drawing.Point(37, 35)
        PeriodIDLabel.Name = "PeriodIDLabel"
        PeriodIDLabel.Size = New System.Drawing.Size(54, 13)
        PeriodIDLabel.TabIndex = 2
        PeriodIDLabel.Text = "Period ID:"
        '
        'PeriodIDTextBox
        '
        Me.PeriodIDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "PeriodID", True))
        Me.PeriodIDTextBox.Location = New System.Drawing.Point(97, 32)
        Me.PeriodIDTextBox.Name = "PeriodIDTextBox"
        Me.PeriodIDTextBox.Size = New System.Drawing.Size(100, 20)
        Me.PeriodIDTextBox.TabIndex = 3
        '
        'StateLabel
        '
        StateLabel.AutoSize = True
        StateLabel.Location = New System.Drawing.Point(56, 61)
        StateLabel.Name = "StateLabel"
        StateLabel.Size = New System.Drawing.Size(35, 13)
        StateLabel.TabIndex = 4
        StateLabel.Text = "State:"
        '
        'StateTextBox
        '
        Me.StateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "State", True))
        Me.StateTextBox.Location = New System.Drawing.Point(97, 58)
        Me.StateTextBox.Name = "StateTextBox"
        Me.StateTextBox.Size = New System.Drawing.Size(100, 20)
        Me.StateTextBox.TabIndex = 5
        '
        'GPJurisDescriptionLabel
        '
        GPJurisDescriptionLabel.AutoSize = True
        GPJurisDescriptionLabel.Location = New System.Drawing.Point(-11, 87)
        GPJurisDescriptionLabel.Name = "GPJurisDescriptionLabel"
        GPJurisDescriptionLabel.Size = New System.Drawing.Size(102, 13)
        GPJurisDescriptionLabel.TabIndex = 6
        GPJurisDescriptionLabel.Text = "GPJuris Description:"
        '
        'GPJurisDescriptionTextBox
        '
        Me.GPJurisDescriptionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "GPJurisDescription", True))
        Me.GPJurisDescriptionTextBox.Location = New System.Drawing.Point(97, 84)
        Me.GPJurisDescriptionTextBox.Name = "GPJurisDescriptionTextBox"
        Me.GPJurisDescriptionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.GPJurisDescriptionTextBox.TabIndex = 7
        '
        'CountyNameLabel
        '
        CountyNameLabel.AutoSize = True
        CountyNameLabel.Location = New System.Drawing.Point(17, 113)
        CountyNameLabel.Name = "CountyNameLabel"
        CountyNameLabel.Size = New System.Drawing.Size(74, 13)
        CountyNameLabel.TabIndex = 8
        CountyNameLabel.Text = "County Name:"
        '
        'CountyNameTextBox
        '
        Me.CountyNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "CountyName", True))
        Me.CountyNameTextBox.Location = New System.Drawing.Point(97, 110)
        Me.CountyNameTextBox.Name = "CountyNameTextBox"
        Me.CountyNameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CountyNameTextBox.TabIndex = 9
        '
        'ZipCodeLabel
        '
        ZipCodeLabel.AutoSize = True
        ZipCodeLabel.Location = New System.Drawing.Point(38, 154)
        ZipCodeLabel.Name = "ZipCodeLabel"
        ZipCodeLabel.Size = New System.Drawing.Size(53, 13)
        ZipCodeLabel.TabIndex = 10
        ZipCodeLabel.Text = "Zip Code:"
        '
        'ZipCodeTextBox
        '
        Me.ZipCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipCode", True))
        Me.ZipCodeTextBox.Location = New System.Drawing.Point(97, 151)
        Me.ZipCodeTextBox.Name = "ZipCodeTextBox"
        Me.ZipCodeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ZipCodeTextBox.TabIndex = 11
        '
        'ZipExtensionLabel
        '
        ZipExtensionLabel.AutoSize = True
        ZipExtensionLabel.Location = New System.Drawing.Point(17, 180)
        ZipExtensionLabel.Name = "ZipExtensionLabel"
        ZipExtensionLabel.Size = New System.Drawing.Size(74, 13)
        ZipExtensionLabel.TabIndex = 12
        ZipExtensionLabel.Text = "Zip Extension:"
        '
        'ZipExtensionTextBox
        '
        Me.ZipExtensionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipExtension", True))
        Me.ZipExtensionTextBox.Location = New System.Drawing.Point(97, 177)
        Me.ZipExtensionTextBox.Name = "ZipExtensionTextBox"
        Me.ZipExtensionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ZipExtensionTextBox.TabIndex = 13
        '
        'ExcludeCountyLabel
        '
        ExcludeCountyLabel.AutoSize = True
        ExcludeCountyLabel.Location = New System.Drawing.Point(7, 233)
        ExcludeCountyLabel.Name = "ExcludeCountyLabel"
        ExcludeCountyLabel.Size = New System.Drawing.Size(84, 13)
        ExcludeCountyLabel.TabIndex = 14
        ExcludeCountyLabel.Text = "Exclude County:"
        '
        'ExcludeCountyTextBox
        '
        Me.ExcludeCountyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeCounty", True))
        Me.ExcludeCountyTextBox.Location = New System.Drawing.Point(97, 230)
        Me.ExcludeCountyTextBox.Name = "ExcludeCountyTextBox"
        Me.ExcludeCountyTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ExcludeCountyTextBox.TabIndex = 15
        '
        'ExcludeCityLabel
        '
        ExcludeCityLabel.AutoSize = True
        ExcludeCityLabel.Location = New System.Drawing.Point(23, 259)
        ExcludeCityLabel.Name = "ExcludeCityLabel"
        ExcludeCityLabel.Size = New System.Drawing.Size(68, 13)
        ExcludeCityLabel.TabIndex = 16
        ExcludeCityLabel.Text = "Exclude City:"
        '
        'ExcludeCityTextBox
        '
        Me.ExcludeCityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeCity", True))
        Me.ExcludeCityTextBox.Location = New System.Drawing.Point(97, 256)
        Me.ExcludeCityTextBox.Name = "ExcludeCityTextBox"
        Me.ExcludeCityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ExcludeCityTextBox.TabIndex = 17
        '
        'JurisdictionXRefDataGridView
        '
        Me.JurisdictionXRefDataGridView.AutoGenerateColumns = False
        Me.JurisdictionXRefDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.JurisdictionXRefDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewCheckBoxColumn7, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34})
        Me.JurisdictionXRefDataGridView.DataSource = Me.JurisdictionXRefBindingSource
        Me.JurisdictionXRefDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.JurisdictionXRefDataGridView.Name = "JurisdictionXRefDataGridView"
        Me.JurisdictionXRefDataGridView.Size = New System.Drawing.Size(979, 369)
        Me.JurisdictionXRefDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "JurisXrefKey"
        Me.DataGridViewTextBoxColumn23.HeaderText = "JurisXrefKey"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "PeriodID"
        Me.DataGridViewTextBoxColumn24.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "SUImportSource"
        Me.DataGridViewTextBoxColumn25.HeaderText = "SUImportSource"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "SourceState"
        Me.DataGridViewTextBoxColumn26.HeaderText = "SourceState"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.DataPropertyName = "SourceJurisdictionCode"
        Me.DataGridViewTextBoxColumn27.HeaderText = "SourceJurisdictionCode"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.DataPropertyName = "SourceJurisdictionName"
        Me.DataGridViewTextBoxColumn28.HeaderText = "SourceJurisdictionName"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.DataPropertyName = "JurisdictionKey"
        Me.DataGridViewTextBoxColumn29.HeaderText = "JurisdictionKey"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        '
        'DataGridViewCheckBoxColumn7
        '
        Me.DataGridViewCheckBoxColumn7.DataPropertyName = "DefaultJurisdiction"
        Me.DataGridViewCheckBoxColumn7.HeaderText = "DefaultJurisdiction"
        Me.DataGridViewCheckBoxColumn7.Name = "DataGridViewCheckBoxColumn7"
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.DataPropertyName = "SalesUse"
        Me.DataGridViewTextBoxColumn30.HeaderText = "SalesUse"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.DataPropertyName = "LocationCode"
        Me.DataGridViewTextBoxColumn31.HeaderText = "LocationCode"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.DataPropertyName = "RIABinder"
        Me.DataGridViewTextBoxColumn32.HeaderText = "RIABinder"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.DataPropertyName = "ChangeBy"
        Me.DataGridViewTextBoxColumn33.HeaderText = "ChangeBy"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.DataPropertyName = "ChangeDate"
        Me.DataGridViewTextBoxColumn34.HeaderText = "ChangeDate"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        '
        'JurisdictionXRefOverrideBindingSource
        '
        Me.JurisdictionXRefOverrideBindingSource.DataMember = "JurisdictionXRefOverride"
        Me.JurisdictionXRefOverrideBindingSource.DataSource = Me.SalesUse_UniDataSet
        '
        'JurisdictionXRefOverrideTableAdapter
        '
        Me.JurisdictionXRefOverrideTableAdapter.ClearBeforeFill = True
        '
        'JurisdictionXRefOverrideDataGridView
        '
        Me.JurisdictionXRefOverrideDataGridView.AutoGenerateColumns = False
        Me.JurisdictionXRefOverrideDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.JurisdictionXRefOverrideDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45})
        Me.JurisdictionXRefOverrideDataGridView.DataSource = Me.JurisdictionXRefOverrideBindingSource
        Me.JurisdictionXRefOverrideDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.JurisdictionXRefOverrideDataGridView.Name = "JurisdictionXRefOverrideDataGridView"
        Me.JurisdictionXRefOverrideDataGridView.Size = New System.Drawing.Size(979, 369)
        Me.JurisdictionXRefOverrideDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.DataPropertyName = "XrefOverrideKey"
        Me.DataGridViewTextBoxColumn35.HeaderText = "XrefOverrideKey"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.ReadOnly = True
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.DataPropertyName = "PeriodID"
        Me.DataGridViewTextBoxColumn36.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.DataPropertyName = "SUImportSource"
        Me.DataGridViewTextBoxColumn37.HeaderText = "SUImportSource"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.DataPropertyName = "SourceJurisdictionCode"
        Me.DataGridViewTextBoxColumn38.HeaderText = "SourceJurisdictionCode"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.DataPropertyName = "AccountNumber"
        Me.DataGridViewTextBoxColumn39.HeaderText = "AccountNumber"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.DataPropertyName = "SourceJurisdictionName"
        Me.DataGridViewTextBoxColumn40.HeaderText = "SourceJurisdictionName"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.DataPropertyName = "JurisdictionKey"
        Me.DataGridViewTextBoxColumn41.HeaderText = "JurisdictionKey"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.DataPropertyName = "SalesUse"
        Me.DataGridViewTextBoxColumn42.HeaderText = "SalesUse"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.DataPropertyName = "LocationCode"
        Me.DataGridViewTextBoxColumn43.HeaderText = "LocationCode"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.DataPropertyName = "RIABinder"
        Me.DataGridViewTextBoxColumn44.HeaderText = "RIABinder"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.DataPropertyName = "SourceState"
        Me.DataGridViewTextBoxColumn45.HeaderText = "SourceState"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'LabelPeriod
        '
        Me.LabelPeriod.AutoSize = True
        Me.LabelPeriod.Location = New System.Drawing.Point(13, 6)
        Me.LabelPeriod.Name = "LabelPeriod"
        Me.LabelPeriod.Size = New System.Drawing.Size(37, 13)
        Me.LabelPeriod.TabIndex = 2
        Me.LabelPeriod.Text = "Period"
        '
        'ExcludeDistrictLabel
        '
        ExcludeDistrictLabel.AutoSize = True
        ExcludeDistrictLabel.Location = New System.Drawing.Point(8, 285)
        ExcludeDistrictLabel.Name = "ExcludeDistrictLabel"
        ExcludeDistrictLabel.Size = New System.Drawing.Size(83, 13)
        ExcludeDistrictLabel.TabIndex = 18
        ExcludeDistrictLabel.Text = "Exclude District:"
        '
        'ExcludeDistrictTextBox
        '
        Me.ExcludeDistrictTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeDistrict", True))
        Me.ExcludeDistrictTextBox.Location = New System.Drawing.Point(97, 282)
        Me.ExcludeDistrictTextBox.Name = "ExcludeDistrictTextBox"
        Me.ExcludeDistrictTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ExcludeDistrictTextBox.TabIndex = 19
        '
        'DiscreteStateLabel
        '
        DiscreteStateLabel.AutoSize = True
        DiscreteStateLabel.Location = New System.Drawing.Point(226, 209)
        DiscreteStateLabel.Name = "DiscreteStateLabel"
        DiscreteStateLabel.Size = New System.Drawing.Size(77, 13)
        DiscreteStateLabel.TabIndex = 20
        DiscreteStateLabel.Text = "Discrete State:"
        '
        'DiscreteStateCheckBox
        '
        Me.DiscreteStateCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DiscreteState", True))
        Me.DiscreteStateCheckBox.Location = New System.Drawing.Point(309, 204)
        Me.DiscreteStateCheckBox.Name = "DiscreteStateCheckBox"
        Me.DiscreteStateCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.DiscreteStateCheckBox.TabIndex = 21
        Me.DiscreteStateCheckBox.Text = "CheckBox1"
        Me.DiscreteStateCheckBox.UseVisualStyleBackColor = True
        '
        'ExcludeStateLabel
        '
        ExcludeStateLabel.AutoSize = True
        ExcludeStateLabel.Location = New System.Drawing.Point(15, 207)
        ExcludeStateLabel.Name = "ExcludeStateLabel"
        ExcludeStateLabel.Size = New System.Drawing.Size(76, 13)
        ExcludeStateLabel.TabIndex = 22
        ExcludeStateLabel.Text = "Exclude State:"
        '
        'ExcludeStateTextBox
        '
        Me.ExcludeStateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeState", True))
        Me.ExcludeStateTextBox.Location = New System.Drawing.Point(97, 204)
        Me.ExcludeStateTextBox.Name = "ExcludeStateTextBox"
        Me.ExcludeStateTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ExcludeStateTextBox.TabIndex = 23
        '
        'DiscreteLocalsLabel
        '
        DiscreteLocalsLabel.AutoSize = True
        DiscreteLocalsLabel.Location = New System.Drawing.Point(220, 235)
        DiscreteLocalsLabel.Name = "DiscreteLocalsLabel"
        DiscreteLocalsLabel.Size = New System.Drawing.Size(83, 13)
        DiscreteLocalsLabel.TabIndex = 24
        DiscreteLocalsLabel.Text = "Discrete Locals:"
        '
        'DiscreteLocalsCheckBox
        '
        Me.DiscreteLocalsCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DiscreteLocals", True))
        Me.DiscreteLocalsCheckBox.Location = New System.Drawing.Point(309, 230)
        Me.DiscreteLocalsCheckBox.Name = "DiscreteLocalsCheckBox"
        Me.DiscreteLocalsCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.DiscreteLocalsCheckBox.TabIndex = 25
        Me.DiscreteLocalsCheckBox.Text = "CheckBox1"
        Me.DiscreteLocalsCheckBox.UseVisualStyleBackColor = True
        '
        'HandlingCodeLabel
        '
        HandlingCodeLabel.AutoSize = True
        HandlingCodeLabel.Location = New System.Drawing.Point(223, 259)
        HandlingCodeLabel.Name = "HandlingCodeLabel"
        HandlingCodeLabel.Size = New System.Drawing.Size(80, 13)
        HandlingCodeLabel.TabIndex = 26
        HandlingCodeLabel.Text = "Handling Code:"
        '
        'HandlingCodeTextBox
        '
        Me.HandlingCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "HandlingCode", True))
        Me.HandlingCodeTextBox.Location = New System.Drawing.Point(309, 256)
        Me.HandlingCodeTextBox.Name = "HandlingCodeTextBox"
        Me.HandlingCodeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.HandlingCodeTextBox.TabIndex = 27
        '
        'ReportGrossSalesLabel
        '
        ReportGrossSalesLabel.AutoSize = True
        ReportGrossSalesLabel.Location = New System.Drawing.Point(202, 290)
        ReportGrossSalesLabel.Name = "ReportGrossSalesLabel"
        ReportGrossSalesLabel.Size = New System.Drawing.Size(101, 13)
        ReportGrossSalesLabel.TabIndex = 28
        ReportGrossSalesLabel.Text = "Report Gross Sales:"
        '
        'ReportGrossSalesCheckBox
        '
        Me.ReportGrossSalesCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "ReportGrossSales", True))
        Me.ReportGrossSalesCheckBox.Location = New System.Drawing.Point(309, 285)
        Me.ReportGrossSalesCheckBox.Name = "ReportGrossSalesCheckBox"
        Me.ReportGrossSalesCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ReportGrossSalesCheckBox.TabIndex = 29
        Me.ReportGrossSalesCheckBox.Text = "CheckBox1"
        Me.ReportGrossSalesCheckBox.UseVisualStyleBackColor = True
        '
        'ChangeByLabel
        '
        ChangeByLabel.AutoSize = True
        ChangeByLabel.Location = New System.Drawing.Point(496, 294)
        ChangeByLabel.Name = "ChangeByLabel"
        ChangeByLabel.Size = New System.Drawing.Size(62, 13)
        ChangeByLabel.TabIndex = 30
        ChangeByLabel.Text = "Change By:"
        '
        'ChangeByTextBox
        '
        Me.ChangeByTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ChangeBy", True))
        Me.ChangeByTextBox.Location = New System.Drawing.Point(564, 291)
        Me.ChangeByTextBox.Name = "ChangeByTextBox"
        Me.ChangeByTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ChangeByTextBox.TabIndex = 31
        '
        'ChangeDateLabel
        '
        ChangeDateLabel.AutoSize = True
        ChangeDateLabel.Location = New System.Drawing.Point(485, 321)
        ChangeDateLabel.Name = "ChangeDateLabel"
        ChangeDateLabel.Size = New System.Drawing.Size(73, 13)
        ChangeDateLabel.TabIndex = 32
        ChangeDateLabel.Text = "Change Date:"
        '
        'ChangeDateDateTimePicker
        '
        Me.ChangeDateDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JurisdictionBindingSource, "ChangeDate", True))
        Me.ChangeDateDateTimePicker.Location = New System.Drawing.Point(564, 317)
        Me.ChangeDateDateTimePicker.Name = "ChangeDateDateTimePicker"
        Me.ChangeDateDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.ChangeDateDateTimePicker.TabIndex = 33
        '
        'RIARecordOverrideLabel
        '
        RIARecordOverrideLabel.AutoSize = True
        RIARecordOverrideLabel.Location = New System.Drawing.Point(611, 38)
        RIARecordOverrideLabel.Name = "RIARecordOverrideLabel"
        RIARecordOverrideLabel.Size = New System.Drawing.Size(106, 13)
        RIARecordOverrideLabel.TabIndex = 34
        RIARecordOverrideLabel.Text = "RIARecord Override:"
        '
        'RIARecordOverrideTextBox
        '
        Me.RIARecordOverrideTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "RIARecordOverride", True))
        Me.RIARecordOverrideTextBox.Location = New System.Drawing.Point(723, 35)
        Me.RIARecordOverrideTextBox.Name = "RIARecordOverrideTextBox"
        Me.RIARecordOverrideTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RIARecordOverrideTextBox.TabIndex = 35
        '
        'RIANoState_CalculationLabel
        '
        RIANoState_CalculationLabel.AutoSize = True
        RIANoState_CalculationLabel.Location = New System.Drawing.Point(592, 64)
        RIANoState_CalculationLabel.Name = "RIANoState_CalculationLabel"
        RIANoState_CalculationLabel.Size = New System.Drawing.Size(125, 13)
        RIANoState_CalculationLabel.TabIndex = 36
        RIANoState_CalculationLabel.Text = "RIANo State Calculation:"
        '
        'RIANoState_CalculationTextBox
        '
        Me.RIANoState_CalculationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "RIANoState_Calculation", True))
        Me.RIANoState_CalculationTextBox.Location = New System.Drawing.Point(723, 61)
        Me.RIANoState_CalculationTextBox.Name = "RIANoState_CalculationTextBox"
        Me.RIANoState_CalculationTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RIANoState_CalculationTextBox.TabIndex = 37
        '
        'RIAMultipleZipsLabel
        '
        RIAMultipleZipsLabel.AutoSize = True
        RIAMultipleZipsLabel.Location = New System.Drawing.Point(630, 90)
        RIAMultipleZipsLabel.Name = "RIAMultipleZipsLabel"
        RIAMultipleZipsLabel.Size = New System.Drawing.Size(87, 13)
        RIAMultipleZipsLabel.TabIndex = 38
        RIAMultipleZipsLabel.Text = "RIAMultiple Zips:"
        '
        'RIAMultipleZipsTextBox
        '
        Me.RIAMultipleZipsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "RIAMultipleZips", True))
        Me.RIAMultipleZipsTextBox.Location = New System.Drawing.Point(723, 87)
        Me.RIAMultipleZipsTextBox.Name = "RIAMultipleZipsTextBox"
        Me.RIAMultipleZipsTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RIAMultipleZipsTextBox.TabIndex = 39
        '
        'VtGeoCodeLabel
        '
        VtGeoCodeLabel.AutoSize = True
        VtGeoCodeLabel.Location = New System.Drawing.Point(647, 116)
        VtGeoCodeLabel.Name = "VtGeoCodeLabel"
        VtGeoCodeLabel.Size = New System.Drawing.Size(70, 13)
        VtGeoCodeLabel.TabIndex = 40
        VtGeoCodeLabel.Text = "vt Geo Code:"
        '
        'VtGeoCodeTextBox
        '
        Me.VtGeoCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "vtGeoCode", True))
        Me.VtGeoCodeTextBox.Location = New System.Drawing.Point(723, 113)
        Me.VtGeoCodeTextBox.Name = "VtGeoCodeTextBox"
        Me.VtGeoCodeTextBox.Size = New System.Drawing.Size(100, 20)
        Me.VtGeoCodeTextBox.TabIndex = 41
        '
        'DefaultForZipLabel
        '
        DefaultForZipLabel.AutoSize = True
        DefaultForZipLabel.Location = New System.Drawing.Point(637, 147)
        DefaultForZipLabel.Name = "DefaultForZipLabel"
        DefaultForZipLabel.Size = New System.Drawing.Size(80, 13)
        DefaultForZipLabel.TabIndex = 42
        DefaultForZipLabel.Text = "Default For Zip:"
        '
        'DefaultForZipCheckBox
        '
        Me.DefaultForZipCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DefaultForZip", True))
        Me.DefaultForZipCheckBox.Location = New System.Drawing.Point(723, 142)
        Me.DefaultForZipCheckBox.Name = "DefaultForZipCheckBox"
        Me.DefaultForZipCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.DefaultForZipCheckBox.TabIndex = 43
        Me.DefaultForZipCheckBox.Text = "CheckBox1"
        Me.DefaultForZipCheckBox.UseVisualStyleBackColor = True
        '
        'DefaultforGEOLabel
        '
        DefaultforGEOLabel.AutoSize = True
        DefaultforGEOLabel.Location = New System.Drawing.Point(635, 171)
        DefaultforGEOLabel.Name = "DefaultforGEOLabel"
        DefaultforGEOLabel.Size = New System.Drawing.Size(82, 13)
        DefaultforGEOLabel.TabIndex = 44
        DefaultforGEOLabel.Text = "Defaultfor GEO:"
        '
        'DefaultforGEOCheckBox
        '
        Me.DefaultforGEOCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DefaultforGEO", True))
        Me.DefaultforGEOCheckBox.Location = New System.Drawing.Point(723, 166)
        Me.DefaultforGEOCheckBox.Name = "DefaultforGEOCheckBox"
        Me.DefaultforGEOCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.DefaultforGEOCheckBox.TabIndex = 45
        Me.DefaultforGEOCheckBox.Text = "CheckBox1"
        Me.DefaultforGEOCheckBox.UseVisualStyleBackColor = True
        '
        'ZipCountyLabel
        '
        ZipCountyLabel.AutoSize = True
        ZipCountyLabel.Location = New System.Drawing.Point(656, 199)
        ZipCountyLabel.Name = "ZipCountyLabel"
        ZipCountyLabel.Size = New System.Drawing.Size(61, 13)
        ZipCountyLabel.TabIndex = 46
        ZipCountyLabel.Text = "Zip County:"
        '
        'ZipCountyTextBox
        '
        Me.ZipCountyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipCounty", True))
        Me.ZipCountyTextBox.Location = New System.Drawing.Point(723, 196)
        Me.ZipCountyTextBox.Name = "ZipCountyTextBox"
        Me.ZipCountyTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ZipCountyTextBox.TabIndex = 47
        '
        'ZipLocalLabel
        '
        ZipLocalLabel.AutoSize = True
        ZipLocalLabel.Location = New System.Drawing.Point(663, 225)
        ZipLocalLabel.Name = "ZipLocalLabel"
        ZipLocalLabel.Size = New System.Drawing.Size(54, 13)
        ZipLocalLabel.TabIndex = 48
        ZipLocalLabel.Text = "Zip Local:"
        '
        'ZipLocalTextBox
        '
        Me.ZipLocalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipLocal", True))
        Me.ZipLocalTextBox.Location = New System.Drawing.Point(723, 222)
        Me.ZipLocalTextBox.Name = "ZipLocalTextBox"
        Me.ZipLocalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ZipLocalTextBox.TabIndex = 49
        '
        'ZipTransitLabel
        '
        ZipTransitLabel.AutoSize = True
        ZipTransitLabel.Location = New System.Drawing.Point(657, 251)
        ZipTransitLabel.Name = "ZipTransitLabel"
        ZipTransitLabel.Size = New System.Drawing.Size(60, 13)
        ZipTransitLabel.TabIndex = 50
        ZipTransitLabel.Text = "Zip Transit:"
        '
        'ZipTransitTextBox
        '
        Me.ZipTransitTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipTransit", True))
        Me.ZipTransitTextBox.Location = New System.Drawing.Point(723, 248)
        Me.ZipTransitTextBox.Name = "ZipTransitTextBox"
        Me.ZipTransitTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ZipTransitTextBox.TabIndex = 51
        '
        'FormMaintainJurisdiction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 443)
        Me.Controls.Add(Me.LabelPeriod)
        Me.Controls.Add(Me.cboPeriodID)
        Me.Controls.Add(Me.TabControl)
        Me.Name = "FormMaintainJurisdiction"
        Me.Text = "Maintain Jurisdiction"
        Me.TabControl.ResumeLayout(False)
        Me.TabJurisdictionListing.ResumeLayout(False)
        CType(Me.JurisdictionDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JurisdictionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SalesUse_UniDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabJurisdictionDetail.ResumeLayout(False)
        Me.TabJurisdictionDetail.PerformLayout()
        Me.TabJursidictionCrossRef.ResumeLayout(False)
        Me.TabCrossRefOverides.ResumeLayout(False)
        CType(Me.PeriodBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodBindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodBindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodBindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JurisdictionXRefBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JurisdictionXRefDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JurisdictionXRefOverrideBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JurisdictionXRefOverrideDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl As TabControl
    Friend WithEvents TabJurisdictionListing As TabPage
    Friend WithEvents TabJurisdictionDetail As TabPage
    Friend WithEvents TabJursidictionCrossRef As TabPage
    Friend WithEvents TabCrossRefOverides As TabPage
    Friend WithEvents SalesUse_UniDataSet As SalesUse_UniDataSet
    Friend WithEvents JurisdictionBindingSource As BindingSource
    Friend WithEvents JurisdictionTableAdapter As SalesUse_UniDataSetTableAdapters.JurisdictionTableAdapter
    Friend WithEvents TableAdapterManager As SalesUse_UniDataSetTableAdapters.TableAdapterManager
    Friend WithEvents JurisdictionDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn4 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn5 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn6 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents PeriodBindingSource As BindingSource
    Friend WithEvents PeriodBindingSource1 As BindingSource
    Friend WithEvents PeriodBindingSource2 As BindingSource
    Friend WithEvents PeriodBindingSource3 As BindingSource
    Friend WithEvents PeriodBindingSource4 As BindingSource
    Friend WithEvents PeriodBindingSource5 As BindingSource
    Friend WithEvents cboPeriodID As ComboBox
    Friend WithEvents JurisdictionXRefBindingSource As BindingSource
    Friend WithEvents JurisdictionXRefTableAdapter As SalesUse_UniDataSetTableAdapters.JurisdictionXRefTableAdapter
    Friend WithEvents StateTextBox As TextBox
    Friend WithEvents PeriodIDTextBox As TextBox
    Friend WithEvents JurisdictionKeyTextBox As TextBox
    Friend WithEvents ExcludeCityTextBox As TextBox
    Friend WithEvents ExcludeCountyTextBox As TextBox
    Friend WithEvents ZipExtensionTextBox As TextBox
    Friend WithEvents ZipCodeTextBox As TextBox
    Friend WithEvents CountyNameTextBox As TextBox
    Friend WithEvents GPJurisDescriptionTextBox As TextBox
    Friend WithEvents JurisdictionXRefDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn23 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn7 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As DataGridViewTextBoxColumn
    Friend WithEvents JurisdictionXRefOverrideBindingSource As BindingSource
    Friend WithEvents JurisdictionXRefOverrideTableAdapter As SalesUse_UniDataSetTableAdapters.JurisdictionXRefOverrideTableAdapter
    Friend WithEvents JurisdictionXRefOverrideDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn35 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As DataGridViewTextBoxColumn
    Friend WithEvents LabelPeriod As Label
    Friend WithEvents DiscreteLocalsCheckBox As CheckBox
    Friend WithEvents ExcludeStateTextBox As TextBox
    Friend WithEvents DiscreteStateCheckBox As CheckBox
    Friend WithEvents ExcludeDistrictTextBox As TextBox
    Friend WithEvents ChangeDateDateTimePicker As DateTimePicker
    Friend WithEvents ChangeByTextBox As TextBox
    Friend WithEvents ReportGrossSalesCheckBox As CheckBox
    Friend WithEvents HandlingCodeTextBox As TextBox
    Friend WithEvents ZipTransitTextBox As TextBox
    Friend WithEvents ZipLocalTextBox As TextBox
    Friend WithEvents ZipCountyTextBox As TextBox
    Friend WithEvents DefaultforGEOCheckBox As CheckBox
    Friend WithEvents DefaultForZipCheckBox As CheckBox
    Friend WithEvents VtGeoCodeTextBox As TextBox
    Friend WithEvents RIAMultipleZipsTextBox As TextBox
    Friend WithEvents RIANoState_CalculationTextBox As TextBox
    Friend WithEvents RIARecordOverrideTextBox As TextBox
End Class

﻿Public Class frmMaintainDeterminationMapping
    Private Sub OSDLocationMappingBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles OSDLocationMappingBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.OSDLocationMappingBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainDeterminationMapping)

    End Sub

    Private Sub frmMaintainDeterminationMapping_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainDeterminationMapping.OSDTaxTypeMapping' table. You can move, or remove it, as needed.
        Me.OSDTaxTypeMappingTableAdapter.Fill(Me.DataSource_MaintainDeterminationMapping.OSDTaxTypeMapping)
        'TODO: This line of code loads data into the 'DataSource_MaintainDeterminationMapping.OSDLocationMapping' table. You can move, or remove it, as needed.
        Me.OSDLocationMappingTableAdapter.Fill(Me.DataSource_MaintainDeterminationMapping.OSDLocationMapping)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
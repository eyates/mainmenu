﻿Imports System.Data.SqlClient

Public Class frmMaintainFilingEntities

    Private Sub frmMaintainFilingEntities_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadDGV_FilingEntity()
        LoadDGV_States()
        LoadDGV_Group1()
        LoadDGV_Group2()
        LoadDGV_Account()
        LoadDGV_RecLoc()
        LoadDGV_Binder()

    End Sub

    Private Sub LoadDGV_FilingEntity()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntity"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_FilingEntity.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_States()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntityState"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_States.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_Group1()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntityG1State"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Group1.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_Group2()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntityG1G2Exceptions"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Group2.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_Account()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntityStateAccount"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Account.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_RecLoc()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntityStateRecLoc"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_RecLoc.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_Binder()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM FilingEntityStateBinder"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Binder.DataSource = dtRecord
    End Sub

    Private Sub btnClose_Click_1(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
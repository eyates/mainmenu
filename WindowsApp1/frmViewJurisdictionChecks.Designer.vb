﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmViewJurisdictionChecks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cboPeriod = New System.Windows.Forms.ComboBox()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.TabNoZips = New System.Windows.Forms.TabPage()
        Me.DGV_NoZips = New System.Windows.Forms.DataGridView()
        Me.TabZip99999NotAllowed = New System.Windows.Forms.TabPage()
        Me.DGV_Zip99999NotAllowed = New System.Windows.Forms.DataGridView()
        Me.TabZip99999Flags = New System.Windows.Forms.TabPage()
        Me.DGV_Zip99999Flags = New System.Windows.Forms.DataGridView()
        Me.TabMultiZip = New System.Windows.Forms.TabPage()
        Me.DGV_MultiZip = New System.Windows.Forms.DataGridView()
        Me.TabDiscreateState = New System.Windows.Forms.TabPage()
        Me.DGV_DiscreteState = New System.Windows.Forms.DataGridView()
        Me.TabRIARates = New System.Windows.Forms.TabPage()
        Me.DGV_RIARates = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.TabNoZips.SuspendLayout()
        CType(Me.DGV_NoZips, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabZip99999NotAllowed.SuspendLayout()
        CType(Me.DGV_Zip99999NotAllowed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabZip99999Flags.SuspendLayout()
        CType(Me.DGV_Zip99999Flags, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabMultiZip.SuspendLayout()
        CType(Me.DGV_MultiZip, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabDiscreateState.SuspendLayout()
        CType(Me.DGV_DiscreteState, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabRIARates.SuspendLayout()
        CType(Me.DGV_RIARates, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboPeriod
        '
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(66, 6)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(12, 9)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(37, 13)
        Me.lblPeriod.TabIndex = 1
        Me.lblPeriod.Text = "Period"
        '
        'TabNoZips
        '
        Me.TabNoZips.Controls.Add(Me.DGV_NoZips)
        Me.TabNoZips.Location = New System.Drawing.Point(4, 22)
        Me.TabNoZips.Name = "TabNoZips"
        Me.TabNoZips.Padding = New System.Windows.Forms.Padding(3)
        Me.TabNoZips.Size = New System.Drawing.Size(870, 411)
        Me.TabNoZips.TabIndex = 5
        Me.TabNoZips.Text = "No Zips"
        Me.TabNoZips.UseVisualStyleBackColor = True
        '
        'DGV_NoZips
        '
        Me.DGV_NoZips.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_NoZips.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_NoZips.Location = New System.Drawing.Point(3, 3)
        Me.DGV_NoZips.Name = "DGV_NoZips"
        Me.DGV_NoZips.Size = New System.Drawing.Size(864, 405)
        Me.DGV_NoZips.TabIndex = 1
        '
        'TabZip99999NotAllowed
        '
        Me.TabZip99999NotAllowed.Controls.Add(Me.DGV_Zip99999NotAllowed)
        Me.TabZip99999NotAllowed.Location = New System.Drawing.Point(4, 22)
        Me.TabZip99999NotAllowed.Name = "TabZip99999NotAllowed"
        Me.TabZip99999NotAllowed.Padding = New System.Windows.Forms.Padding(3)
        Me.TabZip99999NotAllowed.Size = New System.Drawing.Size(870, 411)
        Me.TabZip99999NotAllowed.TabIndex = 4
        Me.TabZip99999NotAllowed.Text = "Zip 99999 Not Allowed"
        Me.TabZip99999NotAllowed.UseVisualStyleBackColor = True
        '
        'DGV_Zip99999NotAllowed
        '
        Me.DGV_Zip99999NotAllowed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Zip99999NotAllowed.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_Zip99999NotAllowed.Location = New System.Drawing.Point(3, 3)
        Me.DGV_Zip99999NotAllowed.Name = "DGV_Zip99999NotAllowed"
        Me.DGV_Zip99999NotAllowed.Size = New System.Drawing.Size(864, 405)
        Me.DGV_Zip99999NotAllowed.TabIndex = 0
        '
        'TabZip99999Flags
        '
        Me.TabZip99999Flags.Controls.Add(Me.DGV_Zip99999Flags)
        Me.TabZip99999Flags.Location = New System.Drawing.Point(4, 22)
        Me.TabZip99999Flags.Name = "TabZip99999Flags"
        Me.TabZip99999Flags.Padding = New System.Windows.Forms.Padding(3)
        Me.TabZip99999Flags.Size = New System.Drawing.Size(870, 411)
        Me.TabZip99999Flags.TabIndex = 3
        Me.TabZip99999Flags.Text = "Zip 99999 Flags"
        Me.TabZip99999Flags.UseVisualStyleBackColor = True
        '
        'DGV_Zip99999Flags
        '
        Me.DGV_Zip99999Flags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Zip99999Flags.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_Zip99999Flags.Location = New System.Drawing.Point(3, 3)
        Me.DGV_Zip99999Flags.Name = "DGV_Zip99999Flags"
        Me.DGV_Zip99999Flags.Size = New System.Drawing.Size(864, 405)
        Me.DGV_Zip99999Flags.TabIndex = 0
        '
        'TabMultiZip
        '
        Me.TabMultiZip.Controls.Add(Me.DGV_MultiZip)
        Me.TabMultiZip.Location = New System.Drawing.Point(4, 22)
        Me.TabMultiZip.Name = "TabMultiZip"
        Me.TabMultiZip.Padding = New System.Windows.Forms.Padding(3)
        Me.TabMultiZip.Size = New System.Drawing.Size(870, 411)
        Me.TabMultiZip.TabIndex = 2
        Me.TabMultiZip.Text = "Multi-Zip"
        Me.TabMultiZip.UseVisualStyleBackColor = True
        '
        'DGV_MultiZip
        '
        Me.DGV_MultiZip.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_MultiZip.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_MultiZip.Location = New System.Drawing.Point(3, 3)
        Me.DGV_MultiZip.Name = "DGV_MultiZip"
        Me.DGV_MultiZip.Size = New System.Drawing.Size(864, 405)
        Me.DGV_MultiZip.TabIndex = 0
        '
        'TabDiscreateState
        '
        Me.TabDiscreateState.Controls.Add(Me.DGV_DiscreteState)
        Me.TabDiscreateState.Location = New System.Drawing.Point(4, 22)
        Me.TabDiscreateState.Name = "TabDiscreateState"
        Me.TabDiscreateState.Padding = New System.Windows.Forms.Padding(3)
        Me.TabDiscreateState.Size = New System.Drawing.Size(870, 411)
        Me.TabDiscreateState.TabIndex = 1
        Me.TabDiscreateState.Text = "Discrete State"
        Me.TabDiscreateState.UseVisualStyleBackColor = True
        '
        'DGV_DiscreteState
        '
        Me.DGV_DiscreteState.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_DiscreteState.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_DiscreteState.Location = New System.Drawing.Point(3, 3)
        Me.DGV_DiscreteState.Name = "DGV_DiscreteState"
        Me.DGV_DiscreteState.Size = New System.Drawing.Size(864, 405)
        Me.DGV_DiscreteState.TabIndex = 0
        '
        'TabRIARates
        '
        Me.TabRIARates.Controls.Add(Me.DGV_RIARates)
        Me.TabRIARates.Location = New System.Drawing.Point(4, 22)
        Me.TabRIARates.Name = "TabRIARates"
        Me.TabRIARates.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRIARates.Size = New System.Drawing.Size(870, 382)
        Me.TabRIARates.TabIndex = 0
        Me.TabRIARates.Text = "RIA Rates"
        Me.TabRIARates.UseVisualStyleBackColor = True
        '
        'DGV_RIARates
        '
        Me.DGV_RIARates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_RIARates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_RIARates.Location = New System.Drawing.Point(3, 3)
        Me.DGV_RIARates.Name = "DGV_RIARates"
        Me.DGV_RIARates.Size = New System.Drawing.Size(864, 376)
        Me.DGV_RIARates.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabRIARates)
        Me.TabControl1.Controls.Add(Me.TabDiscreateState)
        Me.TabControl1.Controls.Add(Me.TabMultiZip)
        Me.TabControl1.Controls.Add(Me.TabZip99999Flags)
        Me.TabControl1.Controls.Add(Me.TabZip99999NotAllowed)
        Me.TabControl1.Controls.Add(Me.TabNoZips)
        Me.TabControl1.Location = New System.Drawing.Point(0, 34)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(878, 408)
        Me.TabControl1.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(799, 448)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmViewJurisdictionChecks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(890, 483)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.cboPeriod)
        Me.Controls.Add(Me.lblPeriod)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmViewJurisdictionChecks"
        Me.Text = "View Jurisdiction Checks"
        Me.TabNoZips.ResumeLayout(False)
        CType(Me.DGV_NoZips, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabZip99999NotAllowed.ResumeLayout(False)
        CType(Me.DGV_Zip99999NotAllowed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabZip99999Flags.ResumeLayout(False)
        CType(Me.DGV_Zip99999Flags, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabMultiZip.ResumeLayout(False)
        CType(Me.DGV_MultiZip, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabDiscreateState.ResumeLayout(False)
        CType(Me.DGV_DiscreteState, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabRIARates.ResumeLayout(False)
        CType(Me.DGV_RIARates, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboPeriod As ComboBox
    Friend WithEvents lblPeriod As Label
    Friend WithEvents TabNoZips As TabPage
    Friend WithEvents DGV_NoZips As DataGridView
    Friend WithEvents TabZip99999NotAllowed As TabPage
    Friend WithEvents DGV_Zip99999NotAllowed As DataGridView
    Friend WithEvents TabZip99999Flags As TabPage
    Friend WithEvents DGV_Zip99999Flags As DataGridView
    Friend WithEvents TabMultiZip As TabPage
    Friend WithEvents DGV_MultiZip As DataGridView
    Friend WithEvents TabDiscreateState As TabPage
    Friend WithEvents DGV_DiscreteState As DataGridView
    Friend WithEvents TabRIARates As TabPage
    Friend WithEvents DGV_RIARates As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents btnClose As Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmYearEndMaintenance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnUserMaintenance = New System.Windows.Forms.Button()
        Me.btnSystemMaintenance = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnUserMaintenance
        '
        Me.btnUserMaintenance.Location = New System.Drawing.Point(350, 95)
        Me.btnUserMaintenance.Name = "btnUserMaintenance"
        Me.btnUserMaintenance.Size = New System.Drawing.Size(114, 44)
        Me.btnUserMaintenance.TabIndex = 0
        Me.btnUserMaintenance.Text = "User Maintenance"
        Me.btnUserMaintenance.UseVisualStyleBackColor = True
        '
        'btnSystemMaintenance
        '
        Me.btnSystemMaintenance.Location = New System.Drawing.Point(350, 195)
        Me.btnSystemMaintenance.Name = "btnSystemMaintenance"
        Me.btnSystemMaintenance.Size = New System.Drawing.Size(114, 44)
        Me.btnSystemMaintenance.TabIndex = 1
        Me.btnSystemMaintenance.Text = "System Maintenance"
        Me.btnSystemMaintenance.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(463, 365)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmYearEndMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(550, 400)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSystemMaintenance)
        Me.Controls.Add(Me.btnUserMaintenance)
        Me.Name = "frmYearEndMaintenance"
        Me.Text = "Year End Maintenance"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnUserMaintenance As Button
    Friend WithEvents btnSystemMaintenance As Button
    Friend WithEvents btnClose As Button
End Class

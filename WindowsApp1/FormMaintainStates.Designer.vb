﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMaintainStates
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim StateLabel As System.Windows.Forms.Label
        Dim State_ID_RIALabel As System.Windows.Forms.Label
        Dim State_NameLabel As System.Windows.Forms.Label
        Dim Group4Label As System.Windows.Forms.Label
        Dim TransactionLimitAmountLabel As System.Windows.Forms.Label
        Dim TaxMgrLabel As System.Windows.Forms.Label
        Dim OutStateAsUseLabel As System.Windows.Forms.Label
        Dim OutStateLocationLabel As System.Windows.Forms.Label
        Dim DefaultZipLabel As System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.SalesUse_UniDataSet1 = New WindowsApp1.SalesUse_UniDataSet1()
        Me.StateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StateTableAdapter = New WindowsApp1.SalesUse_UniDataSet1TableAdapters.StateTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.SalesUse_UniDataSet1TableAdapters.TableAdapterManager()
        Me.StateTextBox = New System.Windows.Forms.TextBox()
        Me.State_ID_RIATextBox = New System.Windows.Forms.TextBox()
        Me.State_NameTextBox = New System.Windows.Forms.TextBox()
        Me.Group4TextBox = New System.Windows.Forms.TextBox()
        Me.TransactionLimitAmountTextBox = New System.Windows.Forms.TextBox()
        Me.TaxMgrTextBox = New System.Windows.Forms.TextBox()
        Me.OutStateAsUseCheckBox = New System.Windows.Forms.CheckBox()
        Me.OutStateLocationCheckBox = New System.Windows.Forms.CheckBox()
        Me.DefaultZipTextBox = New System.Windows.Forms.TextBox()
        StateLabel = New System.Windows.Forms.Label()
        State_ID_RIALabel = New System.Windows.Forms.Label()
        State_NameLabel = New System.Windows.Forms.Label()
        Group4Label = New System.Windows.Forms.Label()
        TransactionLimitAmountLabel = New System.Windows.Forms.Label()
        TaxMgrLabel = New System.Windows.Forms.Label()
        OutStateAsUseLabel = New System.Windows.Forms.Label()
        OutStateLocationLabel = New System.Windows.Forms.Label()
        DefaultZipLabel = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.SalesUse_UniDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(779, 314)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.AutoScroll = True
        Me.TabPage1.Controls.Add(StateLabel)
        Me.TabPage1.Controls.Add(Me.StateTextBox)
        Me.TabPage1.Controls.Add(State_ID_RIALabel)
        Me.TabPage1.Controls.Add(Me.State_ID_RIATextBox)
        Me.TabPage1.Controls.Add(State_NameLabel)
        Me.TabPage1.Controls.Add(Me.State_NameTextBox)
        Me.TabPage1.Controls.Add(Group4Label)
        Me.TabPage1.Controls.Add(Me.Group4TextBox)
        Me.TabPage1.Controls.Add(TransactionLimitAmountLabel)
        Me.TabPage1.Controls.Add(Me.TransactionLimitAmountTextBox)
        Me.TabPage1.Controls.Add(TaxMgrLabel)
        Me.TabPage1.Controls.Add(Me.TaxMgrTextBox)
        Me.TabPage1.Controls.Add(OutStateAsUseLabel)
        Me.TabPage1.Controls.Add(Me.OutStateAsUseCheckBox)
        Me.TabPage1.Controls.Add(OutStateLocationLabel)
        Me.TabPage1.Controls.Add(Me.OutStateLocationCheckBox)
        Me.TabPage1.Controls.Add(DefaultZipLabel)
        Me.TabPage1.Controls.Add(Me.DefaultZipTextBox)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(771, 288)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(771, 288)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(771, 288)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'SalesUse_UniDataSet1
        '
        Me.SalesUse_UniDataSet1.DataSetName = "SalesUse_UniDataSet1"
        Me.SalesUse_UniDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'StateBindingSource
        '
        Me.StateBindingSource.DataMember = "State"
        Me.StateBindingSource.DataSource = Me.SalesUse_UniDataSet1
        '
        'StateTableAdapter
        '
        Me.StateTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.FilingEntityStateTableAdapter = Nothing
        Me.TableAdapterManager.FilingEntityTableAdapter = Nothing
        Me.TableAdapterManager.StateTableAdapter = Me.StateTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.SalesUse_UniDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'StateLabel
        '
        StateLabel.AutoSize = True
        StateLabel.Location = New System.Drawing.Point(38, 25)
        StateLabel.Name = "StateLabel"
        StateLabel.Size = New System.Drawing.Size(35, 13)
        StateLabel.TabIndex = 0
        StateLabel.Text = "State:"
        '
        'StateTextBox
        '
        Me.StateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "State", True))
        Me.StateTextBox.Location = New System.Drawing.Point(173, 22)
        Me.StateTextBox.Name = "StateTextBox"
        Me.StateTextBox.Size = New System.Drawing.Size(104, 20)
        Me.StateTextBox.TabIndex = 1
        '
        'State_ID_RIALabel
        '
        State_ID_RIALabel.AutoSize = True
        State_ID_RIALabel.Location = New System.Drawing.Point(38, 51)
        State_ID_RIALabel.Name = "State_ID_RIALabel"
        State_ID_RIALabel.Size = New System.Drawing.Size(70, 13)
        State_ID_RIALabel.TabIndex = 2
        State_ID_RIALabel.Text = "State ID RIA:"
        '
        'State_ID_RIATextBox
        '
        Me.State_ID_RIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "State_ID_RIA", True))
        Me.State_ID_RIATextBox.Location = New System.Drawing.Point(173, 48)
        Me.State_ID_RIATextBox.Name = "State_ID_RIATextBox"
        Me.State_ID_RIATextBox.Size = New System.Drawing.Size(104, 20)
        Me.State_ID_RIATextBox.TabIndex = 3
        '
        'State_NameLabel
        '
        State_NameLabel.AutoSize = True
        State_NameLabel.Location = New System.Drawing.Point(38, 77)
        State_NameLabel.Name = "State_NameLabel"
        State_NameLabel.Size = New System.Drawing.Size(66, 13)
        State_NameLabel.TabIndex = 4
        State_NameLabel.Text = "State Name:"
        '
        'State_NameTextBox
        '
        Me.State_NameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "State_Name", True))
        Me.State_NameTextBox.Location = New System.Drawing.Point(173, 74)
        Me.State_NameTextBox.Name = "State_NameTextBox"
        Me.State_NameTextBox.Size = New System.Drawing.Size(104, 20)
        Me.State_NameTextBox.TabIndex = 5
        '
        'Group4Label
        '
        Group4Label.AutoSize = True
        Group4Label.Location = New System.Drawing.Point(38, 103)
        Group4Label.Name = "Group4Label"
        Group4Label.Size = New System.Drawing.Size(45, 13)
        Group4Label.TabIndex = 6
        Group4Label.Text = "Group4:"
        '
        'Group4TextBox
        '
        Me.Group4TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "Group4", True))
        Me.Group4TextBox.Location = New System.Drawing.Point(173, 100)
        Me.Group4TextBox.Name = "Group4TextBox"
        Me.Group4TextBox.Size = New System.Drawing.Size(104, 20)
        Me.Group4TextBox.TabIndex = 7
        '
        'TransactionLimitAmountLabel
        '
        TransactionLimitAmountLabel.AutoSize = True
        TransactionLimitAmountLabel.Location = New System.Drawing.Point(38, 129)
        TransactionLimitAmountLabel.Name = "TransactionLimitAmountLabel"
        TransactionLimitAmountLabel.Size = New System.Drawing.Size(129, 13)
        TransactionLimitAmountLabel.TabIndex = 8
        TransactionLimitAmountLabel.Text = "Transaction Limit Amount:"
        '
        'TransactionLimitAmountTextBox
        '
        Me.TransactionLimitAmountTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "TransactionLimitAmount", True))
        Me.TransactionLimitAmountTextBox.Location = New System.Drawing.Point(173, 126)
        Me.TransactionLimitAmountTextBox.Name = "TransactionLimitAmountTextBox"
        Me.TransactionLimitAmountTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TransactionLimitAmountTextBox.TabIndex = 9
        '
        'TaxMgrLabel
        '
        TaxMgrLabel.AutoSize = True
        TaxMgrLabel.Location = New System.Drawing.Point(38, 155)
        TaxMgrLabel.Name = "TaxMgrLabel"
        TaxMgrLabel.Size = New System.Drawing.Size(49, 13)
        TaxMgrLabel.TabIndex = 10
        TaxMgrLabel.Text = "Tax Mgr:"
        '
        'TaxMgrTextBox
        '
        Me.TaxMgrTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "TaxMgr", True))
        Me.TaxMgrTextBox.Location = New System.Drawing.Point(173, 152)
        Me.TaxMgrTextBox.Name = "TaxMgrTextBox"
        Me.TaxMgrTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TaxMgrTextBox.TabIndex = 11
        '
        'OutStateAsUseLabel
        '
        OutStateAsUseLabel.AutoSize = True
        OutStateAsUseLabel.Location = New System.Drawing.Point(38, 183)
        OutStateAsUseLabel.Name = "OutStateAsUseLabel"
        OutStateAsUseLabel.Size = New System.Drawing.Size(92, 13)
        OutStateAsUseLabel.TabIndex = 12
        OutStateAsUseLabel.Text = "Out State As Use:"
        '
        'OutStateAsUseCheckBox
        '
        Me.OutStateAsUseCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.StateBindingSource, "OutStateAsUse", True))
        Me.OutStateAsUseCheckBox.Location = New System.Drawing.Point(173, 178)
        Me.OutStateAsUseCheckBox.Name = "OutStateAsUseCheckBox"
        Me.OutStateAsUseCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.OutStateAsUseCheckBox.TabIndex = 13
        Me.OutStateAsUseCheckBox.Text = "CheckBox1"
        Me.OutStateAsUseCheckBox.UseVisualStyleBackColor = True
        '
        'OutStateLocationLabel
        '
        OutStateLocationLabel.AutoSize = True
        OutStateLocationLabel.Location = New System.Drawing.Point(38, 213)
        OutStateLocationLabel.Name = "OutStateLocationLabel"
        OutStateLocationLabel.Size = New System.Drawing.Size(99, 13)
        OutStateLocationLabel.TabIndex = 14
        OutStateLocationLabel.Text = "Out State Location:"
        '
        'OutStateLocationCheckBox
        '
        Me.OutStateLocationCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.StateBindingSource, "OutStateLocation", True))
        Me.OutStateLocationCheckBox.Location = New System.Drawing.Point(173, 208)
        Me.OutStateLocationCheckBox.Name = "OutStateLocationCheckBox"
        Me.OutStateLocationCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.OutStateLocationCheckBox.TabIndex = 15
        Me.OutStateLocationCheckBox.Text = "CheckBox1"
        Me.OutStateLocationCheckBox.UseVisualStyleBackColor = True
        '
        'DefaultZipLabel
        '
        DefaultZipLabel.AutoSize = True
        DefaultZipLabel.Location = New System.Drawing.Point(38, 241)
        DefaultZipLabel.Name = "DefaultZipLabel"
        DefaultZipLabel.Size = New System.Drawing.Size(62, 13)
        DefaultZipLabel.TabIndex = 16
        DefaultZipLabel.Text = "Default Zip:"
        '
        'DefaultZipTextBox
        '
        Me.DefaultZipTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "DefaultZip", True))
        Me.DefaultZipTextBox.Location = New System.Drawing.Point(173, 238)
        Me.DefaultZipTextBox.Name = "DefaultZipTextBox"
        Me.DefaultZipTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DefaultZipTextBox.TabIndex = 17
        '
        'FormMaintainStates
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 443)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "FormMaintainStates"
        Me.Text = "FormMaintainStates"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.SalesUse_UniDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents SalesUse_UniDataSet1 As SalesUse_UniDataSet1
    Friend WithEvents StateBindingSource As BindingSource
    Friend WithEvents StateTableAdapter As SalesUse_UniDataSet1TableAdapters.StateTableAdapter
    Friend WithEvents TableAdapterManager As SalesUse_UniDataSet1TableAdapters.TableAdapterManager
    Friend WithEvents StateTextBox As TextBox
    Friend WithEvents State_ID_RIATextBox As TextBox
    Friend WithEvents State_NameTextBox As TextBox
    Friend WithEvents Group4TextBox As TextBox
    Friend WithEvents TransactionLimitAmountTextBox As TextBox
    Friend WithEvents TaxMgrTextBox As TextBox
    Friend WithEvents OutStateAsUseCheckBox As CheckBox
    Friend WithEvents OutStateLocationCheckBox As CheckBox
    Friend WithEvents DefaultZipTextBox As TextBox
End Class

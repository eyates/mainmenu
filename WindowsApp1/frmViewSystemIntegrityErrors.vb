﻿Imports System.Data.SqlClient

Public Class frmViewSystemIntegrityErrors
    Private Sub FilingEntityStateBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        'Me.FilingEntityStateBindingSource.EndEdit()  ---might be useful to figure out how to save edited data
        Me.TableAdapterManager.UpdateAll(Me.DataSource_ViewSystemIntegirtyErrors)

    End Sub

    Private Sub frmViewSystemIntegrityErrors_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Load_dgvFilingEntityState()
        Load_dgvFilingEntityStateGroup1()
        Load_dgvStateBinder()
        Load_dgvGLDefault()
    End Sub

    Private Sub Load_dgvFilingEntityState()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select FilingEntity.FilingEntityName, FilingEntityState.* from
((SELECT FilingEntityState.State, FilingEntityState.PeriodID, Count(FilingEntityState.FilingEntityKey) AS CountOfFilingEntityKey
FROM FilingEntityState with (nolock)  
WHERE (((FilingEntityState.DefaultforStateFlag)=1))
GROUP BY FilingEntityState.State, FilingEntityState.PeriodID
HAVING (((Count(FilingEntityState.FilingEntityKey))>1))) as Dupes
inner join FilingEntityState on FilingEntityState.State = Dupes.State and FilingEntityState.PeriodID = Dupes.PeriodID)
inner join FilingEntity on FilingEntity.FilingEntityKey = FilingEntityState.FilingEntityKey
"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvFilingEntityState.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvFilingEntityStateGroup1()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select distinct FilingEntity.FilingEntityName, FilingEntityG1State.* from
((SELECT FilingEntityG1State.State, FilingEntityG1State.Group1, FilingEntityG1State.PeriodID, Count(FilingEntityG1State.FilingEntityKey) AS CountOfFilingEntityKey
FROM FilingEntityG1State with (nolock) 
GROUP BY FilingEntityG1State.State, FilingEntityG1State.Group1, FilingEntityG1State.PeriodID
HAVING (((Count(FilingEntityG1State.FilingEntityKey))>1))
) as Dupes
inner join FilingEntityG1State with (nolock)  on FilingEntityG1State.State = Dupes.State AND FilingEntityG1State.PeriodID = Dupes.PeriodID)
inner join FilingEntity with (nolock)  on FilingEntity.FilingEntityKey = FilingEntityG1State.FilingEntityKey
"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvFilingEntityStateGroup1.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvStateBinder()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select FilingEntity.FilingEntityKey, FilingEntity.FilingEntityName, State, PeriodID
from ((select distinct FilingEntityKey, PeriodID, State,
case when RIABinder is null then 'X' when RIABinder = '' then 'X' else RIABinder end as Binder
from FilingEntityStateBinder with (nolock) ) as DupeBinder
inner join FilingEntity with (nolock)   on DupeBinder.FilingEntityKey = FilingEntity.FilingEntityKey)
group by FilingEntity.FilingEntityKey, FilingEntity.FilingEntityName,  State, PeriodID
having count(Binder) > 1
"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvStateBinder.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvGLDefault()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "
SELECT JurisdictionXRef.SourceJurisdictionCode, JurisdictionXRef.SUImportSource, JurisdictionXRef.PeriodID
FROM JurisdictionXRef with (nolock) 
WHERE (((JurisdictionXRef.DefaultJurisdiction)=1))
GROUP BY JurisdictionXRef.SourceJurisdictionCode, JurisdictionXRef.SUImportSource, JurisdictionXRef.PeriodID
HAVING Count(JurisdictionXRef.JurisXrefKey) > 1
"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvGLDefault.DataSource = dtRecord
    End Sub


    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
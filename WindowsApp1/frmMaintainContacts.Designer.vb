﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainContacts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabContacts = New System.Windows.Forms.TabPage()
        Me.SalesUse_UniDataSet2 = New WindowsApp1.SalesUse_UniDataSet2()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.DGV_Contacts = New System.Windows.Forms.DataGridView()
        Me.TabControl.SuspendLayout()
        Me.TabContacts.SuspendLayout()
        CType(Me.SalesUse_UniDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_Contacts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabContacts)
        Me.TabControl.Location = New System.Drawing.Point(12, 12)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(763, 379)
        Me.TabControl.TabIndex = 0
        '
        'TabContacts
        '
        Me.TabContacts.Controls.Add(Me.DGV_Contacts)
        Me.TabContacts.Location = New System.Drawing.Point(4, 22)
        Me.TabContacts.Name = "TabContacts"
        Me.TabContacts.Padding = New System.Windows.Forms.Padding(3)
        Me.TabContacts.Size = New System.Drawing.Size(755, 353)
        Me.TabContacts.TabIndex = 0
        Me.TabContacts.Text = "Contacts"
        Me.TabContacts.UseVisualStyleBackColor = True
        '
        'SalesUse_UniDataSet2
        '
        Me.SalesUse_UniDataSet2.DataSetName = "SalesUse_UniDataSet2"
        Me.SalesUse_UniDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(696, 397)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DGV_Contacts
        '
        Me.DGV_Contacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Contacts.Location = New System.Drawing.Point(0, 0)
        Me.DGV_Contacts.Name = "DGV_Contacts"
        Me.DGV_Contacts.Size = New System.Drawing.Size(755, 347)
        Me.DGV_Contacts.TabIndex = 0
        '
        'frmMaintainContacts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 443)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl)
        Me.Name = "frmMaintainContacts"
        Me.Text = "Maintain Contacts"
        Me.TabControl.ResumeLayout(False)
        Me.TabContacts.ResumeLayout(False)
        CType(Me.SalesUse_UniDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_Contacts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl As TabControl
    Friend WithEvents TabContacts As TabPage
    Friend WithEvents SalesUse_UniDataSet2 As SalesUse_UniDataSet2
    Friend WithEvents btnClose As Button
    Friend WithEvents DGV_Contacts As DataGridView
End Class

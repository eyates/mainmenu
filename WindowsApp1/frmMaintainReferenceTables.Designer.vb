﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainReferenceTables
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabAdjustType = New System.Windows.Forms.TabPage()
        Me.dgvAdjustType = New System.Windows.Forms.DataGridView()
        Me.TabErrorGroup = New System.Windows.Forms.TabPage()
        Me.lblErrorCodes = New System.Windows.Forms.Label()
        Me.lblErrorGroup = New System.Windows.Forms.Label()
        Me.cboErrorGroup = New System.Windows.Forms.ComboBox()
        Me.dgvErrorCodes = New System.Windows.Forms.DataGridView()
        Me.TabErrorStatus = New System.Windows.Forms.TabPage()
        Me.dgvErrorStatus = New System.Windows.Forms.DataGridView()
        Me.TabImportSource = New System.Windows.Forms.TabPage()
        Me.dgvSUImportSource = New System.Windows.Forms.DataGridView()
        Me.TabAccuralReview = New System.Windows.Forms.TabPage()
        Me.dgvVendorsToExclude = New System.Windows.Forms.DataGridView()
        Me.dgvB3I4AccountsToReview = New System.Windows.Forms.DataGridView()
        Me.TabTaxVendor = New System.Windows.Forms.TabPage()
        Me.dgvTaxVendors = New System.Windows.Forms.DataGridView()
        Me.TabTransactionsSource = New System.Windows.Forms.TabPage()
        Me.dgvTransactionSource = New System.Windows.Forms.DataGridView()
        Me.TabHandlingCode = New System.Windows.Forms.TabPage()
        Me.dgvHandlingCode = New System.Windows.Forms.DataGridView()
        Me.TabSystemCode = New System.Windows.Forms.TabPage()
        Me.dgvSystemCodes = New System.Windows.Forms.DataGridView()
        Me.DataSource_MaintainReferenceTables = New WindowsApp1.DataSource_MaintainReferenceTables()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainReferenceTablesTableAdapters.TableAdapterManager()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabAdjustType.SuspendLayout()
        CType(Me.dgvAdjustType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabErrorGroup.SuspendLayout()
        CType(Me.dgvErrorCodes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabErrorStatus.SuspendLayout()
        CType(Me.dgvErrorStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabImportSource.SuspendLayout()
        CType(Me.dgvSUImportSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabAccuralReview.SuspendLayout()
        CType(Me.dgvVendorsToExclude, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvB3I4AccountsToReview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabTaxVendor.SuspendLayout()
        CType(Me.dgvTaxVendors, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabTransactionsSource.SuspendLayout()
        CType(Me.dgvTransactionSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabHandlingCode.SuspendLayout()
        CType(Me.dgvHandlingCode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSystemCode.SuspendLayout()
        CType(Me.dgvSystemCodes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSource_MaintainReferenceTables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabAdjustType)
        Me.TabControl1.Controls.Add(Me.TabErrorGroup)
        Me.TabControl1.Controls.Add(Me.TabErrorStatus)
        Me.TabControl1.Controls.Add(Me.TabImportSource)
        Me.TabControl1.Controls.Add(Me.TabAccuralReview)
        Me.TabControl1.Controls.Add(Me.TabTaxVendor)
        Me.TabControl1.Controls.Add(Me.TabTransactionsSource)
        Me.TabControl1.Controls.Add(Me.TabHandlingCode)
        Me.TabControl1.Controls.Add(Me.TabSystemCode)
        Me.TabControl1.Location = New System.Drawing.Point(0, 37)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(677, 378)
        Me.TabControl1.TabIndex = 0
        '
        'TabAdjustType
        '
        Me.TabAdjustType.Controls.Add(Me.dgvAdjustType)
        Me.TabAdjustType.Location = New System.Drawing.Point(4, 22)
        Me.TabAdjustType.Name = "TabAdjustType"
        Me.TabAdjustType.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAdjustType.Size = New System.Drawing.Size(669, 352)
        Me.TabAdjustType.TabIndex = 0
        Me.TabAdjustType.Text = "Adjust. Type"
        Me.TabAdjustType.UseVisualStyleBackColor = True
        '
        'dgvAdjustType
        '
        Me.dgvAdjustType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAdjustType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAdjustType.Location = New System.Drawing.Point(3, 3)
        Me.dgvAdjustType.Name = "dgvAdjustType"
        Me.dgvAdjustType.Size = New System.Drawing.Size(663, 346)
        Me.dgvAdjustType.TabIndex = 0
        '
        'TabErrorGroup
        '
        Me.TabErrorGroup.Controls.Add(Me.lblErrorCodes)
        Me.TabErrorGroup.Controls.Add(Me.lblErrorGroup)
        Me.TabErrorGroup.Controls.Add(Me.cboErrorGroup)
        Me.TabErrorGroup.Controls.Add(Me.dgvErrorCodes)
        Me.TabErrorGroup.Location = New System.Drawing.Point(4, 22)
        Me.TabErrorGroup.Name = "TabErrorGroup"
        Me.TabErrorGroup.Padding = New System.Windows.Forms.Padding(3)
        Me.TabErrorGroup.Size = New System.Drawing.Size(669, 352)
        Me.TabErrorGroup.TabIndex = 1
        Me.TabErrorGroup.Text = "Error Group"
        Me.TabErrorGroup.UseVisualStyleBackColor = True
        '
        'lblErrorCodes
        '
        Me.lblErrorCodes.AutoSize = True
        Me.lblErrorCodes.Location = New System.Drawing.Point(43, 41)
        Me.lblErrorCodes.Name = "lblErrorCodes"
        Me.lblErrorCodes.Size = New System.Drawing.Size(62, 13)
        Me.lblErrorCodes.TabIndex = 3
        Me.lblErrorCodes.Text = "Error Codes"
        '
        'lblErrorGroup
        '
        Me.lblErrorGroup.AutoSize = True
        Me.lblErrorGroup.Location = New System.Drawing.Point(43, 13)
        Me.lblErrorGroup.Name = "lblErrorGroup"
        Me.lblErrorGroup.Size = New System.Drawing.Size(61, 13)
        Me.lblErrorGroup.TabIndex = 2
        Me.lblErrorGroup.Text = "Error Group"
        '
        'cboErrorGroup
        '
        Me.cboErrorGroup.FormattingEnabled = True
        Me.cboErrorGroup.Location = New System.Drawing.Point(109, 10)
        Me.cboErrorGroup.Name = "cboErrorGroup"
        Me.cboErrorGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboErrorGroup.TabIndex = 1
        '
        'dgvErrorCodes
        '
        Me.dgvErrorCodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvErrorCodes.Location = New System.Drawing.Point(8, 57)
        Me.dgvErrorCodes.Name = "dgvErrorCodes"
        Me.dgvErrorCodes.Size = New System.Drawing.Size(655, 312)
        Me.dgvErrorCodes.TabIndex = 0
        '
        'TabErrorStatus
        '
        Me.TabErrorStatus.Controls.Add(Me.dgvErrorStatus)
        Me.TabErrorStatus.Location = New System.Drawing.Point(4, 22)
        Me.TabErrorStatus.Name = "TabErrorStatus"
        Me.TabErrorStatus.Padding = New System.Windows.Forms.Padding(3)
        Me.TabErrorStatus.Size = New System.Drawing.Size(669, 352)
        Me.TabErrorStatus.TabIndex = 2
        Me.TabErrorStatus.Text = "Error Status"
        Me.TabErrorStatus.UseVisualStyleBackColor = True
        '
        'dgvErrorStatus
        '
        Me.dgvErrorStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvErrorStatus.Location = New System.Drawing.Point(8, 6)
        Me.dgvErrorStatus.Name = "dgvErrorStatus"
        Me.dgvErrorStatus.Size = New System.Drawing.Size(655, 363)
        Me.dgvErrorStatus.TabIndex = 0
        '
        'TabImportSource
        '
        Me.TabImportSource.AutoScroll = True
        Me.TabImportSource.Controls.Add(Me.dgvSUImportSource)
        Me.TabImportSource.Location = New System.Drawing.Point(4, 22)
        Me.TabImportSource.Name = "TabImportSource"
        Me.TabImportSource.Padding = New System.Windows.Forms.Padding(3)
        Me.TabImportSource.Size = New System.Drawing.Size(669, 352)
        Me.TabImportSource.TabIndex = 3
        Me.TabImportSource.Text = "Import Source"
        Me.TabImportSource.UseVisualStyleBackColor = True
        '
        'dgvSUImportSource
        '
        Me.dgvSUImportSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSUImportSource.Location = New System.Drawing.Point(8, 6)
        Me.dgvSUImportSource.Name = "dgvSUImportSource"
        Me.dgvSUImportSource.Size = New System.Drawing.Size(655, 363)
        Me.dgvSUImportSource.TabIndex = 0
        '
        'TabAccuralReview
        '
        Me.TabAccuralReview.AutoScroll = True
        Me.TabAccuralReview.Controls.Add(Me.dgvVendorsToExclude)
        Me.TabAccuralReview.Controls.Add(Me.dgvB3I4AccountsToReview)
        Me.TabAccuralReview.Location = New System.Drawing.Point(4, 22)
        Me.TabAccuralReview.Name = "TabAccuralReview"
        Me.TabAccuralReview.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAccuralReview.Size = New System.Drawing.Size(669, 352)
        Me.TabAccuralReview.TabIndex = 4
        Me.TabAccuralReview.Text = "Accural Review"
        Me.TabAccuralReview.UseVisualStyleBackColor = True
        '
        'dgvVendorsToExclude
        '
        Me.dgvVendorsToExclude.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVendorsToExclude.Location = New System.Drawing.Point(351, 6)
        Me.dgvVendorsToExclude.Name = "dgvVendorsToExclude"
        Me.dgvVendorsToExclude.Size = New System.Drawing.Size(312, 363)
        Me.dgvVendorsToExclude.TabIndex = 1
        '
        'dgvB3I4AccountsToReview
        '
        Me.dgvB3I4AccountsToReview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvB3I4AccountsToReview.Location = New System.Drawing.Point(3, 6)
        Me.dgvB3I4AccountsToReview.Name = "dgvB3I4AccountsToReview"
        Me.dgvB3I4AccountsToReview.Size = New System.Drawing.Size(342, 366)
        Me.dgvB3I4AccountsToReview.TabIndex = 0
        '
        'TabTaxVendor
        '
        Me.TabTaxVendor.Controls.Add(Me.dgvTaxVendors)
        Me.TabTaxVendor.Location = New System.Drawing.Point(4, 22)
        Me.TabTaxVendor.Name = "TabTaxVendor"
        Me.TabTaxVendor.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTaxVendor.Size = New System.Drawing.Size(669, 352)
        Me.TabTaxVendor.TabIndex = 5
        Me.TabTaxVendor.Text = "Tax Vendor"
        Me.TabTaxVendor.UseVisualStyleBackColor = True
        '
        'dgvTaxVendors
        '
        Me.dgvTaxVendors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTaxVendors.Location = New System.Drawing.Point(8, 6)
        Me.dgvTaxVendors.Name = "dgvTaxVendors"
        Me.dgvTaxVendors.Size = New System.Drawing.Size(655, 363)
        Me.dgvTaxVendors.TabIndex = 0
        '
        'TabTransactionsSource
        '
        Me.TabTransactionsSource.Controls.Add(Me.dgvTransactionSource)
        Me.TabTransactionsSource.Location = New System.Drawing.Point(4, 22)
        Me.TabTransactionsSource.Name = "TabTransactionsSource"
        Me.TabTransactionsSource.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTransactionsSource.Size = New System.Drawing.Size(669, 352)
        Me.TabTransactionsSource.TabIndex = 6
        Me.TabTransactionsSource.Text = "Transaction Source"
        Me.TabTransactionsSource.UseVisualStyleBackColor = True
        '
        'dgvTransactionSource
        '
        Me.dgvTransactionSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTransactionSource.Location = New System.Drawing.Point(6, 6)
        Me.dgvTransactionSource.Name = "dgvTransactionSource"
        Me.dgvTransactionSource.Size = New System.Drawing.Size(453, 319)
        Me.dgvTransactionSource.TabIndex = 0
        '
        'TabHandlingCode
        '
        Me.TabHandlingCode.Controls.Add(Me.dgvHandlingCode)
        Me.TabHandlingCode.Location = New System.Drawing.Point(4, 22)
        Me.TabHandlingCode.Name = "TabHandlingCode"
        Me.TabHandlingCode.Padding = New System.Windows.Forms.Padding(3)
        Me.TabHandlingCode.Size = New System.Drawing.Size(669, 352)
        Me.TabHandlingCode.TabIndex = 7
        Me.TabHandlingCode.Text = "Handling Code"
        Me.TabHandlingCode.UseVisualStyleBackColor = True
        '
        'dgvHandlingCode
        '
        Me.dgvHandlingCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHandlingCode.Location = New System.Drawing.Point(6, 6)
        Me.dgvHandlingCode.Name = "dgvHandlingCode"
        Me.dgvHandlingCode.Size = New System.Drawing.Size(660, 363)
        Me.dgvHandlingCode.TabIndex = 0
        '
        'TabSystemCode
        '
        Me.TabSystemCode.AutoScroll = True
        Me.TabSystemCode.Controls.Add(Me.dgvSystemCodes)
        Me.TabSystemCode.Location = New System.Drawing.Point(4, 22)
        Me.TabSystemCode.Name = "TabSystemCode"
        Me.TabSystemCode.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSystemCode.Size = New System.Drawing.Size(669, 352)
        Me.TabSystemCode.TabIndex = 8
        Me.TabSystemCode.Text = "System Code"
        Me.TabSystemCode.UseVisualStyleBackColor = True
        '
        'dgvSystemCodes
        '
        Me.dgvSystemCodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSystemCodes.Location = New System.Drawing.Point(6, 6)
        Me.dgvSystemCodes.Name = "dgvSystemCodes"
        Me.dgvSystemCodes.Size = New System.Drawing.Size(657, 316)
        Me.dgvSystemCodes.TabIndex = 0
        '
        'DataSource_MaintainReferenceTables
        '
        Me.DataSource_MaintainReferenceTables.DataSetName = "DataSource_MaintainReferenceTables"
        Me.DataSource_MaintainReferenceTables.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AdjustmentTypesTableAdapter = Nothing
        Me.TableAdapterManager.B3I4AccountsToReviewTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.ErrorCodesTableAdapter = Nothing
        Me.TableAdapterManager.ErrorStatusTableAdapter = Nothing
        Me.TableAdapterManager.HandlingCodeTableAdapter = Nothing
        Me.TableAdapterManager.SUImportSourceTableAdapter = Nothing
        Me.TableAdapterManager.SystemCodesTableAdapter = Nothing
        Me.TableAdapterManager.TaxVendorsTableAdapter = Nothing
        Me.TableAdapterManager.TransactionSourceTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainReferenceTablesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VendorsToExcludeTableAdapter = Nothing
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(598, 417)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainReferenceTables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 450)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainReferenceTables"
        Me.Text = "Maintain Reference Tables"
        Me.TabControl1.ResumeLayout(False)
        Me.TabAdjustType.ResumeLayout(False)
        CType(Me.dgvAdjustType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabErrorGroup.ResumeLayout(False)
        Me.TabErrorGroup.PerformLayout()
        CType(Me.dgvErrorCodes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabErrorStatus.ResumeLayout(False)
        CType(Me.dgvErrorStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabImportSource.ResumeLayout(False)
        CType(Me.dgvSUImportSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabAccuralReview.ResumeLayout(False)
        CType(Me.dgvVendorsToExclude, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvB3I4AccountsToReview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabTaxVendor.ResumeLayout(False)
        CType(Me.dgvTaxVendors, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabTransactionsSource.ResumeLayout(False)
        CType(Me.dgvTransactionSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabHandlingCode.ResumeLayout(False)
        CType(Me.dgvHandlingCode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSystemCode.ResumeLayout(False)
        CType(Me.dgvSystemCodes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSource_MaintainReferenceTables, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabAdjustType As TabPage
    Friend WithEvents TabErrorGroup As TabPage
    Friend WithEvents TabErrorStatus As TabPage
    Friend WithEvents TabImportSource As TabPage
    Friend WithEvents TabAccuralReview As TabPage
    Friend WithEvents TabTaxVendor As TabPage
    Friend WithEvents TabTransactionsSource As TabPage
    Friend WithEvents TabHandlingCode As TabPage
    Friend WithEvents TabSystemCode As TabPage
    Friend WithEvents DataSource_MaintainReferenceTables As DataSource_MaintainReferenceTables
    Friend WithEvents TableAdapterManager As DataSource_MaintainReferenceTablesTableAdapters.TableAdapterManager
    Friend WithEvents dgvSystemCodes As DataGridView
    Friend WithEvents dgvHandlingCode As DataGridView
    Friend WithEvents dgvTransactionSource As DataGridView
    Friend WithEvents dgvTaxVendors As DataGridView
    Friend WithEvents dgvB3I4AccountsToReview As DataGridView
    Friend WithEvents dgvVendorsToExclude As DataGridView
    Friend WithEvents dgvSUImportSource As DataGridView
    Friend WithEvents dgvErrorStatus As DataGridView
    Friend WithEvents dgvErrorCodes As DataGridView
    Friend WithEvents lblErrorCodes As Label
    Friend WithEvents lblErrorGroup As Label
    Friend WithEvents cboErrorGroup As ComboBox
    Friend WithEvents btnClose As Button
    Friend WithEvents dgvAdjustType As DataGridView
End Class

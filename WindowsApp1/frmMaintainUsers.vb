﻿Public Class frmMaintainUsers
    Private Sub UsersBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles UsersBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.UsersBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainUsers)

    End Sub

    Private Sub frmMaintainUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainUsers.Users' table. You can move, or remove it, as needed.
        Me.UsersTableAdapter.Fill(Me.DataSource_MaintainUsers.Users)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
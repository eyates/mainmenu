﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMaintainFilingEntities
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabFilingEntity = New System.Windows.Forms.TabPage()
        Me.DGV_FilingEntity = New System.Windows.Forms.DataGridView()
        Me.TabStates = New System.Windows.Forms.TabPage()
        Me.DGV_States = New System.Windows.Forms.DataGridView()
        Me.TabGroup1 = New System.Windows.Forms.TabPage()
        Me.TabGroup2 = New System.Windows.Forms.TabPage()
        Me.TabAccount = New System.Windows.Forms.TabPage()
        Me.TabRecLoc = New System.Windows.Forms.TabPage()
        Me.TabBinder = New System.Windows.Forms.TabPage()
        Me.FilingEntityG1StateBindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityG1G2ExceptionsBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityStateRecLocBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityStateBinderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityG1StateBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityG1G2ExceptionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityStateTableAdapter = New WindowsApp1.DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityStateTableAdapter()
        Me.FilingEntityG1StateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityG1StateTableAdapter = New WindowsApp1.DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityG1StateTableAdapter()
        Me.FilingEntityG1StateBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.FilingEntityG1G2ExceptionsTableAdapter = New WindowsApp1.DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityG1G2ExceptionsTableAdapter()
        Me.FilingEntityStateRecLocTableAdapter = New WindowsApp1.DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityStateRecLocTableAdapter()
        Me.FilingEntityStateBinderTableAdapter = New WindowsApp1.DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityStateBinderTableAdapter()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.DGV_Group1 = New System.Windows.Forms.DataGridView()
        Me.DGV_Group2 = New System.Windows.Forms.DataGridView()
        Me.DGV_Account = New System.Windows.Forms.DataGridView()
        Me.DGV_RecLoc = New System.Windows.Forms.DataGridView()
        Me.DGV_Binder = New System.Windows.Forms.DataGridView()
        Me.TabControl1.SuspendLayout()
        Me.TabFilingEntity.SuspendLayout()
        CType(Me.DGV_FilingEntity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabStates.SuspendLayout()
        CType(Me.DGV_States, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabGroup1.SuspendLayout()
        Me.TabGroup2.SuspendLayout()
        Me.TabAccount.SuspendLayout()
        Me.TabRecLoc.SuspendLayout()
        Me.TabBinder.SuspendLayout()
        CType(Me.FilingEntityG1StateBindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityG1G2ExceptionsBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityStateRecLocBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityStateBinderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityG1StateBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityG1G2ExceptionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityG1StateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityG1StateBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_Group1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_Group2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_RecLoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_Binder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabFilingEntity)
        Me.TabControl1.Controls.Add(Me.TabStates)
        Me.TabControl1.Controls.Add(Me.TabGroup1)
        Me.TabControl1.Controls.Add(Me.TabGroup2)
        Me.TabControl1.Controls.Add(Me.TabAccount)
        Me.TabControl1.Controls.Add(Me.TabRecLoc)
        Me.TabControl1.Controls.Add(Me.TabBinder)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(727, 458)
        Me.TabControl1.TabIndex = 0
        '
        'TabFilingEntity
        '
        Me.TabFilingEntity.Controls.Add(Me.DGV_FilingEntity)
        Me.TabFilingEntity.Location = New System.Drawing.Point(4, 22)
        Me.TabFilingEntity.Name = "TabFilingEntity"
        Me.TabFilingEntity.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFilingEntity.Size = New System.Drawing.Size(719, 432)
        Me.TabFilingEntity.TabIndex = 0
        Me.TabFilingEntity.Text = "Filing Entity"
        Me.TabFilingEntity.UseVisualStyleBackColor = True
        '
        'DGV_FilingEntity
        '
        Me.DGV_FilingEntity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_FilingEntity.Location = New System.Drawing.Point(3, 3)
        Me.DGV_FilingEntity.Name = "DGV_FilingEntity"
        Me.DGV_FilingEntity.Size = New System.Drawing.Size(716, 432)
        Me.DGV_FilingEntity.TabIndex = 0
        '
        'TabStates
        '
        Me.TabStates.Controls.Add(Me.DGV_States)
        Me.TabStates.Location = New System.Drawing.Point(4, 22)
        Me.TabStates.Name = "TabStates"
        Me.TabStates.Padding = New System.Windows.Forms.Padding(3)
        Me.TabStates.Size = New System.Drawing.Size(719, 432)
        Me.TabStates.TabIndex = 1
        Me.TabStates.Text = "States"
        Me.TabStates.UseVisualStyleBackColor = True
        '
        'DGV_States
        '
        Me.DGV_States.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_States.Location = New System.Drawing.Point(0, 0)
        Me.DGV_States.Name = "DGV_States"
        Me.DGV_States.Size = New System.Drawing.Size(716, 421)
        Me.DGV_States.TabIndex = 0
        '
        'TabGroup1
        '
        Me.TabGroup1.Controls.Add(Me.DGV_Group1)
        Me.TabGroup1.Location = New System.Drawing.Point(4, 22)
        Me.TabGroup1.Name = "TabGroup1"
        Me.TabGroup1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabGroup1.Size = New System.Drawing.Size(719, 432)
        Me.TabGroup1.TabIndex = 2
        Me.TabGroup1.Text = "Group1"
        Me.TabGroup1.UseVisualStyleBackColor = True
        '
        'TabGroup2
        '
        Me.TabGroup2.Controls.Add(Me.DGV_Group2)
        Me.TabGroup2.Location = New System.Drawing.Point(4, 22)
        Me.TabGroup2.Name = "TabGroup2"
        Me.TabGroup2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabGroup2.Size = New System.Drawing.Size(719, 432)
        Me.TabGroup2.TabIndex = 3
        Me.TabGroup2.Text = "Group2"
        Me.TabGroup2.UseVisualStyleBackColor = True
        '
        'TabAccount
        '
        Me.TabAccount.Controls.Add(Me.DGV_Account)
        Me.TabAccount.Location = New System.Drawing.Point(4, 22)
        Me.TabAccount.Name = "TabAccount"
        Me.TabAccount.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAccount.Size = New System.Drawing.Size(719, 432)
        Me.TabAccount.TabIndex = 4
        Me.TabAccount.Text = "Account"
        Me.TabAccount.UseVisualStyleBackColor = True
        '
        'TabRecLoc
        '
        Me.TabRecLoc.Controls.Add(Me.DGV_RecLoc)
        Me.TabRecLoc.Location = New System.Drawing.Point(4, 22)
        Me.TabRecLoc.Name = "TabRecLoc"
        Me.TabRecLoc.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRecLoc.Size = New System.Drawing.Size(719, 432)
        Me.TabRecLoc.TabIndex = 5
        Me.TabRecLoc.Text = "RecLoc"
        Me.TabRecLoc.UseVisualStyleBackColor = True
        '
        'TabBinder
        '
        Me.TabBinder.Controls.Add(Me.DGV_Binder)
        Me.TabBinder.Location = New System.Drawing.Point(4, 22)
        Me.TabBinder.Name = "TabBinder"
        Me.TabBinder.Padding = New System.Windows.Forms.Padding(3)
        Me.TabBinder.Size = New System.Drawing.Size(719, 432)
        Me.TabBinder.TabIndex = 6
        Me.TabBinder.Text = "Binder"
        Me.TabBinder.UseVisualStyleBackColor = True
        '
        'FilingEntityStateTableAdapter
        '
        Me.FilingEntityStateTableAdapter.ClearBeforeFill = True
        '
        'FilingEntityG1StateTableAdapter
        '
        Me.FilingEntityG1StateTableAdapter.ClearBeforeFill = True
        '
        'FilingEntityG1G2ExceptionsTableAdapter
        '
        Me.FilingEntityG1G2ExceptionsTableAdapter.ClearBeforeFill = True
        '
        'FilingEntityStateRecLocTableAdapter
        '
        Me.FilingEntityStateRecLocTableAdapter.ClearBeforeFill = True
        '
        'FilingEntityStateBinderTableAdapter
        '
        Me.FilingEntityStateBinderTableAdapter.ClearBeforeFill = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(640, 460)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DGV_Group1
        '
        Me.DGV_Group1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Group1.Location = New System.Drawing.Point(0, 0)
        Me.DGV_Group1.Name = "DGV_Group1"
        Me.DGV_Group1.Size = New System.Drawing.Size(719, 374)
        Me.DGV_Group1.TabIndex = 0
        '
        'DGV_Group2
        '
        Me.DGV_Group2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Group2.Location = New System.Drawing.Point(0, 0)
        Me.DGV_Group2.Name = "DGV_Group2"
        Me.DGV_Group2.Size = New System.Drawing.Size(719, 397)
        Me.DGV_Group2.TabIndex = 0
        '
        'DGV_Account
        '
        Me.DGV_Account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Account.Location = New System.Drawing.Point(8, 3)
        Me.DGV_Account.Name = "DGV_Account"
        Me.DGV_Account.Size = New System.Drawing.Size(703, 382)
        Me.DGV_Account.TabIndex = 0
        '
        'DGV_RecLoc
        '
        Me.DGV_RecLoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_RecLoc.Location = New System.Drawing.Point(0, 0)
        Me.DGV_RecLoc.Name = "DGV_RecLoc"
        Me.DGV_RecLoc.Size = New System.Drawing.Size(711, 408)
        Me.DGV_RecLoc.TabIndex = 0
        '
        'DGV_Binder
        '
        Me.DGV_Binder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_Binder.Location = New System.Drawing.Point(0, 0)
        Me.DGV_Binder.Name = "DGV_Binder"
        Me.DGV_Binder.Size = New System.Drawing.Size(711, 394)
        Me.DGV_Binder.TabIndex = 0
        '
        'frmMaintainFilingEntities
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(727, 491)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainFilingEntities"
        Me.Text = "Maintain Filing Entities"
        Me.TabControl1.ResumeLayout(False)
        Me.TabFilingEntity.ResumeLayout(False)
        CType(Me.DGV_FilingEntity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabStates.ResumeLayout(False)
        CType(Me.DGV_States, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabGroup1.ResumeLayout(False)
        Me.TabGroup2.ResumeLayout(False)
        Me.TabAccount.ResumeLayout(False)
        Me.TabRecLoc.ResumeLayout(False)
        Me.TabBinder.ResumeLayout(False)
        CType(Me.FilingEntityG1StateBindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityG1G2ExceptionsBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityStateRecLocBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityStateBinderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityG1StateBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityG1G2ExceptionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityG1StateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityG1StateBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_Group1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_Group2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_RecLoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_Binder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabFilingEntity As TabPage
    Friend WithEvents TabStates As TabPage
    Friend WithEvents TabGroup1 As TabPage
    Friend WithEvents TabGroup2 As TabPage
    Friend WithEvents TabAccount As TabPage
    Friend WithEvents TabRecLoc As TabPage
    Friend WithEvents FilingEntityStateTableAdapter As DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityStateTableAdapter
    Friend WithEvents FilingEntityG1StateBindingSource As BindingSource
    Friend WithEvents FilingEntityG1StateTableAdapter As DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityG1StateTableAdapter
    Friend WithEvents FilingEntityG1StateBindingSource1 As BindingSource
    Friend WithEvents FilingEntityG1StateBindingSource2 As BindingSource
    Friend WithEvents FilingEntityG1G2ExceptionsBindingSource As BindingSource
    Friend WithEvents FilingEntityG1G2ExceptionsTableAdapter As DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityG1G2ExceptionsTableAdapter
    Friend WithEvents FilingEntityG1StateBindingSource3 As BindingSource
    Friend WithEvents FilingEntityG1G2ExceptionsBindingSource1 As BindingSource
    Friend WithEvents TabBinder As TabPage
    Friend WithEvents FilingEntityStateRecLocBindingSource As BindingSource
    Friend WithEvents FilingEntityStateRecLocTableAdapter As DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityStateRecLocTableAdapter
    Friend WithEvents FilingEntityStateBinderBindingSource As BindingSource
    Friend WithEvents FilingEntityStateBinderTableAdapter As DataSource_MaintainFilingEntitiesTableAdapters.FilingEntityStateBinderTableAdapter
    Friend WithEvents DGV_FilingEntity As DataGridView
    Friend WithEvents DGV_States As DataGridView
    Friend WithEvents btnClose As Button
    Friend WithEvents DGV_Group1 As DataGridView
    Friend WithEvents DGV_Group2 As DataGridView
    Friend WithEvents DGV_Account As DataGridView
    Friend WithEvents DGV_RecLoc As DataGridView
    Friend WithEvents DGV_Binder As DataGridView
End Class

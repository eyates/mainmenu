﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMaintainingGroup1ReportingGroups
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataSource_maintainingGroup1ReportingGroups = New WindowsApp1.DataSource_maintainingGroup1ReportingGroups()
        Me.Group1ReportingGroupsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Group1ReportingGroupsTableAdapter = New WindowsApp1.DataSource_maintainingGroup1ReportingGroupsTableAdapters.Group1ReportingGroupsTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_maintainingGroup1ReportingGroupsTableAdapters.TableAdapterManager()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.lblGroupName = New System.Windows.Forms.Label()
        Me.lblGroup1s = New System.Windows.Forms.Label()
        Me.cboGroupName = New System.Windows.Forms.ComboBox()
        Me.dgvGroup1s = New System.Windows.Forms.DataGridView()
        CType(Me.DataSource_maintainingGroup1ReportingGroups, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Group1ReportingGroupsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGroup1s, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSource_maintainingGroup1ReportingGroups
        '
        Me.DataSource_maintainingGroup1ReportingGroups.DataSetName = "DataSource_maintainingGroup1ReportingGroups"
        Me.DataSource_maintainingGroup1ReportingGroups.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Group1ReportingGroupsBindingSource
        '
        Me.Group1ReportingGroupsBindingSource.DataMember = "Group1ReportingGroups"
        Me.Group1ReportingGroupsBindingSource.DataSource = Me.DataSource_maintainingGroup1ReportingGroups
        '
        'Group1ReportingGroupsTableAdapter
        '
        Me.Group1ReportingGroupsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Group1ReportingGroupsTableAdapter = Me.Group1ReportingGroupsTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_maintainingGroup1ReportingGroupsTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(650, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblGroupName
        '
        Me.lblGroupName.AutoSize = True
        Me.lblGroupName.Location = New System.Drawing.Point(12, 5)
        Me.lblGroupName.Name = "lblGroupName"
        Me.lblGroupName.Size = New System.Drawing.Size(67, 13)
        Me.lblGroupName.TabIndex = 4
        Me.lblGroupName.Text = "Group Name"
        '
        'lblGroup1s
        '
        Me.lblGroup1s.AutoSize = True
        Me.lblGroup1s.Location = New System.Drawing.Point(12, 34)
        Me.lblGroup1s.Name = "lblGroup1s"
        Me.lblGroup1s.Size = New System.Drawing.Size(47, 13)
        Me.lblGroup1s.TabIndex = 5
        Me.lblGroup1s.Text = "Group1s"
        '
        'cboGroupName
        '
        Me.cboGroupName.FormattingEnabled = True
        Me.cboGroupName.Location = New System.Drawing.Point(85, 2)
        Me.cboGroupName.Name = "cboGroupName"
        Me.cboGroupName.Size = New System.Drawing.Size(121, 21)
        Me.cboGroupName.TabIndex = 6
        '
        'dgvGroup1s
        '
        Me.dgvGroup1s.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGroup1s.Location = New System.Drawing.Point(85, 45)
        Me.dgvGroup1s.Name = "dgvGroup1s"
        Me.dgvGroup1s.Size = New System.Drawing.Size(568, 326)
        Me.dgvGroup1s.TabIndex = 7
        '
        'frmMaintainingGroup1ReportingGroups
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(899, 495)
        Me.Controls.Add(Me.dgvGroup1s)
        Me.Controls.Add(Me.cboGroupName)
        Me.Controls.Add(Me.lblGroup1s)
        Me.Controls.Add(Me.lblGroupName)
        Me.Controls.Add(Me.btnClose)
        Me.Name = "frmMaintainingGroup1ReportingGroups"
        Me.Text = "Maintaining Group1 Reporting Groups"
        CType(Me.DataSource_maintainingGroup1ReportingGroups, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Group1ReportingGroupsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGroup1s, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataSource_maintainingGroup1ReportingGroups As DataSource_maintainingGroup1ReportingGroups
    Friend WithEvents Group1ReportingGroupsBindingSource As BindingSource
    Friend WithEvents Group1ReportingGroupsTableAdapter As DataSource_maintainingGroup1ReportingGroupsTableAdapters.Group1ReportingGroupsTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_maintainingGroup1ReportingGroupsTableAdapters.TableAdapterManager
    Friend WithEvents btnClose As Button
    Friend WithEvents lblGroupName As Label
    Friend WithEvents lblGroup1s As Label
    Friend WithEvents cboGroupName As ComboBox
    Friend WithEvents dgvGroup1s As DataGridView
End Class

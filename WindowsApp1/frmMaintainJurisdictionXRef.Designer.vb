﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainJurisdictionXRef
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim JurisdictionKeyLabel1 As System.Windows.Forms.Label
        Dim PeriodIDLabel1 As System.Windows.Forms.Label
        Dim StateLabel As System.Windows.Forms.Label
        Dim GPJurisDescriptionLabel As System.Windows.Forms.Label
        Dim ZipCountyLabel As System.Windows.Forms.Label
        Dim ZipLocalLabel As System.Windows.Forms.Label
        Dim ZipTransitLabel As System.Windows.Forms.Label
        Dim CountyNameLabel As System.Windows.Forms.Label
        Dim LocationNameLabel As System.Windows.Forms.Label
        Dim DefaultZipForSalesLabel As System.Windows.Forms.Label
        Dim RIARecordOverrideLabel As System.Windows.Forms.Label
        Dim RIANoState_CalculationLabel As System.Windows.Forms.Label
        Dim RIAMultipleZipsLabel As System.Windows.Forms.Label
        Dim DiscreteStateLabel As System.Windows.Forms.Label
        Dim DiscreteLocalsLabel As System.Windows.Forms.Label
        Dim HandlingCodeLabel As System.Windows.Forms.Label
        Dim VtGeoCodeLabel As System.Windows.Forms.Label
        Dim DefaultForZipLabel As System.Windows.Forms.Label
        Dim DefaultforGEOLabel As System.Windows.Forms.Label
        Dim ChangeByLabel1 As System.Windows.Forms.Label
        Dim ChangeDateLabel1 As System.Windows.Forms.Label
        Dim ReportGrossSalesLabel As System.Windows.Forms.Label
        Dim ZipCodeLabel As System.Windows.Forms.Label
        Dim ZipExtensionLabel As System.Windows.Forms.Label
        Dim ExcludeStateLabel As System.Windows.Forms.Label
        Dim ExcludeCountyLabel As System.Windows.Forms.Label
        Dim ExcludeCityLabel As System.Windows.Forms.Label
        Dim ExcludeDistrictLabel As System.Windows.Forms.Label
        Dim JurisXrefKeyLabel As System.Windows.Forms.Label
        Dim PeriodIDLabel As System.Windows.Forms.Label
        Dim SUImportSourceLabel As System.Windows.Forms.Label
        Dim SourceStateLabel As System.Windows.Forms.Label
        Dim SourceJurisdictionCodeLabel As System.Windows.Forms.Label
        Dim SourceJurisdictionNameLabel As System.Windows.Forms.Label
        Dim JurisdictionKeyLabel As System.Windows.Forms.Label
        Dim DefaultJurisdictionLabel As System.Windows.Forms.Label
        Dim SalesUseLabel As System.Windows.Forms.Label
        Dim LocationCodeLabel As System.Windows.Forms.Label
        Dim RIABinderLabel As System.Windows.Forms.Label
        Dim ChangeByLabel As System.Windows.Forms.Label
        Dim ChangeDateLabel As System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabSourceJurisdictionListing = New System.Windows.Forms.TabPage()
        Me.DGV_SourceJurisdictionListing = New System.Windows.Forms.DataGridView()
        Me.TabSourceJurisdictionDetail = New System.Windows.Forms.TabPage()
        Me.TabReturnJurisdiction = New System.Windows.Forms.TabPage()
        Me.JurisdictionKeyTextBox1 = New System.Windows.Forms.TextBox()
        Me.JurisdictionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSource_MaintainJurisdictionXRef = New WindowsApp1.DataSource_MaintainJurisdictionXRef()
        Me.PeriodIDTextBox1 = New System.Windows.Forms.TextBox()
        Me.StateTextBox = New System.Windows.Forms.TextBox()
        Me.GPJurisDescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.ZipCountyTextBox = New System.Windows.Forms.TextBox()
        Me.ZipLocalTextBox = New System.Windows.Forms.TextBox()
        Me.ZipTransitTextBox = New System.Windows.Forms.TextBox()
        Me.CountyNameTextBox = New System.Windows.Forms.TextBox()
        Me.LocationNameTextBox = New System.Windows.Forms.TextBox()
        Me.DefaultZipForSalesCheckBox = New System.Windows.Forms.CheckBox()
        Me.RIARecordOverrideTextBox = New System.Windows.Forms.TextBox()
        Me.RIANoState_CalculationTextBox = New System.Windows.Forms.TextBox()
        Me.RIAMultipleZipsTextBox = New System.Windows.Forms.TextBox()
        Me.DiscreteStateCheckBox = New System.Windows.Forms.CheckBox()
        Me.DiscreteLocalsCheckBox = New System.Windows.Forms.CheckBox()
        Me.HandlingCodeTextBox = New System.Windows.Forms.TextBox()
        Me.VtGeoCodeTextBox = New System.Windows.Forms.TextBox()
        Me.DefaultForZipCheckBox = New System.Windows.Forms.CheckBox()
        Me.DefaultforGEOCheckBox = New System.Windows.Forms.CheckBox()
        Me.ChangeByTextBox1 = New System.Windows.Forms.TextBox()
        Me.ChangeDateDateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.ReportGrossSalesCheckBox = New System.Windows.Forms.CheckBox()
        Me.ZipCodeTextBox = New System.Windows.Forms.TextBox()
        Me.ZipExtensionTextBox = New System.Windows.Forms.TextBox()
        Me.ExcludeStateTextBox = New System.Windows.Forms.TextBox()
        Me.ExcludeCountyTextBox = New System.Windows.Forms.TextBox()
        Me.ExcludeCityTextBox = New System.Windows.Forms.TextBox()
        Me.ExcludeDistrictTextBox = New System.Windows.Forms.TextBox()
        Me.TabSourceJurisdictionOverides = New System.Windows.Forms.TabPage()
        Me.JurisdictionXRefOverrideBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainJurisdictionXRefTableAdapters.TableAdapterManager()
        Me.JurisdictionTableAdapter = New WindowsApp1.DataSource_MaintainJurisdictionXRefTableAdapters.JurisdictionTableAdapter()
        Me.JurisdictionXRefOverrideTableAdapter = New WindowsApp1.DataSource_MaintainJurisdictionXRefTableAdapters.JurisdictionXRefOverrideTableAdapter()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.cboPeriod = New System.Windows.Forms.ComboBox()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.JurisdictionXRefBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JurisdictionXRefTableAdapter = New WindowsApp1.DataSource_MaintainJurisdictionXRefTableAdapters.JurisdictionXRefTableAdapter()
        Me.JurisXrefKeyTextBox = New System.Windows.Forms.TextBox()
        Me.PeriodIDTextBox = New System.Windows.Forms.TextBox()
        Me.SUImportSourceTextBox = New System.Windows.Forms.TextBox()
        Me.SourceStateTextBox = New System.Windows.Forms.TextBox()
        Me.SourceJurisdictionCodeTextBox = New System.Windows.Forms.TextBox()
        Me.SourceJurisdictionNameTextBox = New System.Windows.Forms.TextBox()
        Me.JurisdictionKeyTextBox = New System.Windows.Forms.TextBox()
        Me.DefaultJurisdictionCheckBox = New System.Windows.Forms.CheckBox()
        Me.SalesUseTextBox = New System.Windows.Forms.TextBox()
        Me.LocationCodeTextBox = New System.Windows.Forms.TextBox()
        Me.RIABinderTextBox = New System.Windows.Forms.TextBox()
        Me.ChangeByTextBox = New System.Windows.Forms.TextBox()
        Me.ChangeDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.DGV_SourceJurisdictionOverides = New System.Windows.Forms.DataGridView()
        JurisdictionKeyLabel1 = New System.Windows.Forms.Label()
        PeriodIDLabel1 = New System.Windows.Forms.Label()
        StateLabel = New System.Windows.Forms.Label()
        GPJurisDescriptionLabel = New System.Windows.Forms.Label()
        ZipCountyLabel = New System.Windows.Forms.Label()
        ZipLocalLabel = New System.Windows.Forms.Label()
        ZipTransitLabel = New System.Windows.Forms.Label()
        CountyNameLabel = New System.Windows.Forms.Label()
        LocationNameLabel = New System.Windows.Forms.Label()
        DefaultZipForSalesLabel = New System.Windows.Forms.Label()
        RIARecordOverrideLabel = New System.Windows.Forms.Label()
        RIANoState_CalculationLabel = New System.Windows.Forms.Label()
        RIAMultipleZipsLabel = New System.Windows.Forms.Label()
        DiscreteStateLabel = New System.Windows.Forms.Label()
        DiscreteLocalsLabel = New System.Windows.Forms.Label()
        HandlingCodeLabel = New System.Windows.Forms.Label()
        VtGeoCodeLabel = New System.Windows.Forms.Label()
        DefaultForZipLabel = New System.Windows.Forms.Label()
        DefaultforGEOLabel = New System.Windows.Forms.Label()
        ChangeByLabel1 = New System.Windows.Forms.Label()
        ChangeDateLabel1 = New System.Windows.Forms.Label()
        ReportGrossSalesLabel = New System.Windows.Forms.Label()
        ZipCodeLabel = New System.Windows.Forms.Label()
        ZipExtensionLabel = New System.Windows.Forms.Label()
        ExcludeStateLabel = New System.Windows.Forms.Label()
        ExcludeCountyLabel = New System.Windows.Forms.Label()
        ExcludeCityLabel = New System.Windows.Forms.Label()
        ExcludeDistrictLabel = New System.Windows.Forms.Label()
        JurisXrefKeyLabel = New System.Windows.Forms.Label()
        PeriodIDLabel = New System.Windows.Forms.Label()
        SUImportSourceLabel = New System.Windows.Forms.Label()
        SourceStateLabel = New System.Windows.Forms.Label()
        SourceJurisdictionCodeLabel = New System.Windows.Forms.Label()
        SourceJurisdictionNameLabel = New System.Windows.Forms.Label()
        JurisdictionKeyLabel = New System.Windows.Forms.Label()
        DefaultJurisdictionLabel = New System.Windows.Forms.Label()
        SalesUseLabel = New System.Windows.Forms.Label()
        LocationCodeLabel = New System.Windows.Forms.Label()
        RIABinderLabel = New System.Windows.Forms.Label()
        ChangeByLabel = New System.Windows.Forms.Label()
        ChangeDateLabel = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabSourceJurisdictionListing.SuspendLayout()
        CType(Me.DGV_SourceJurisdictionListing, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSourceJurisdictionDetail.SuspendLayout()
        Me.TabReturnJurisdiction.SuspendLayout()
        CType(Me.JurisdictionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSource_MaintainJurisdictionXRef, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSourceJurisdictionOverides.SuspendLayout()
        CType(Me.JurisdictionXRefOverrideBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JurisdictionXRefBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_SourceJurisdictionOverides, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'JurisdictionKeyLabel1
        '
        JurisdictionKeyLabel1.AutoSize = True
        JurisdictionKeyLabel1.Location = New System.Drawing.Point(26, 9)
        JurisdictionKeyLabel1.Name = "JurisdictionKeyLabel1"
        JurisdictionKeyLabel1.Size = New System.Drawing.Size(83, 13)
        JurisdictionKeyLabel1.TabIndex = 0
        JurisdictionKeyLabel1.Text = "Jurisdiction Key:"
        '
        'PeriodIDLabel1
        '
        PeriodIDLabel1.AutoSize = True
        PeriodIDLabel1.Location = New System.Drawing.Point(26, 35)
        PeriodIDLabel1.Name = "PeriodIDLabel1"
        PeriodIDLabel1.Size = New System.Drawing.Size(54, 13)
        PeriodIDLabel1.TabIndex = 2
        PeriodIDLabel1.Text = "Period ID:"
        '
        'StateLabel
        '
        StateLabel.AutoSize = True
        StateLabel.Location = New System.Drawing.Point(26, 61)
        StateLabel.Name = "StateLabel"
        StateLabel.Size = New System.Drawing.Size(35, 13)
        StateLabel.TabIndex = 4
        StateLabel.Text = "State:"
        '
        'GPJurisDescriptionLabel
        '
        GPJurisDescriptionLabel.AutoSize = True
        GPJurisDescriptionLabel.Location = New System.Drawing.Point(26, 87)
        GPJurisDescriptionLabel.Name = "GPJurisDescriptionLabel"
        GPJurisDescriptionLabel.Size = New System.Drawing.Size(102, 13)
        GPJurisDescriptionLabel.TabIndex = 6
        GPJurisDescriptionLabel.Text = "GPJuris Description:"
        '
        'ZipCountyLabel
        '
        ZipCountyLabel.AutoSize = True
        ZipCountyLabel.Location = New System.Drawing.Point(26, 113)
        ZipCountyLabel.Name = "ZipCountyLabel"
        ZipCountyLabel.Size = New System.Drawing.Size(61, 13)
        ZipCountyLabel.TabIndex = 8
        ZipCountyLabel.Text = "Zip County:"
        '
        'ZipLocalLabel
        '
        ZipLocalLabel.AutoSize = True
        ZipLocalLabel.Location = New System.Drawing.Point(26, 139)
        ZipLocalLabel.Name = "ZipLocalLabel"
        ZipLocalLabel.Size = New System.Drawing.Size(54, 13)
        ZipLocalLabel.TabIndex = 10
        ZipLocalLabel.Text = "Zip Local:"
        '
        'ZipTransitLabel
        '
        ZipTransitLabel.AutoSize = True
        ZipTransitLabel.Location = New System.Drawing.Point(26, 165)
        ZipTransitLabel.Name = "ZipTransitLabel"
        ZipTransitLabel.Size = New System.Drawing.Size(60, 13)
        ZipTransitLabel.TabIndex = 12
        ZipTransitLabel.Text = "Zip Transit:"
        '
        'CountyNameLabel
        '
        CountyNameLabel.AutoSize = True
        CountyNameLabel.Location = New System.Drawing.Point(26, 191)
        CountyNameLabel.Name = "CountyNameLabel"
        CountyNameLabel.Size = New System.Drawing.Size(74, 13)
        CountyNameLabel.TabIndex = 14
        CountyNameLabel.Text = "County Name:"
        '
        'LocationNameLabel
        '
        LocationNameLabel.AutoSize = True
        LocationNameLabel.Location = New System.Drawing.Point(26, 217)
        LocationNameLabel.Name = "LocationNameLabel"
        LocationNameLabel.Size = New System.Drawing.Size(82, 13)
        LocationNameLabel.TabIndex = 16
        LocationNameLabel.Text = "Location Name:"
        '
        'DefaultZipForSalesLabel
        '
        DefaultZipForSalesLabel.AutoSize = True
        DefaultZipForSalesLabel.Location = New System.Drawing.Point(26, 245)
        DefaultZipForSalesLabel.Name = "DefaultZipForSalesLabel"
        DefaultZipForSalesLabel.Size = New System.Drawing.Size(109, 13)
        DefaultZipForSalesLabel.TabIndex = 18
        DefaultZipForSalesLabel.Text = "Default Zip For Sales:"
        '
        'RIARecordOverrideLabel
        '
        RIARecordOverrideLabel.AutoSize = True
        RIARecordOverrideLabel.Location = New System.Drawing.Point(26, 273)
        RIARecordOverrideLabel.Name = "RIARecordOverrideLabel"
        RIARecordOverrideLabel.Size = New System.Drawing.Size(106, 13)
        RIARecordOverrideLabel.TabIndex = 20
        RIARecordOverrideLabel.Text = "RIARecord Override:"
        '
        'RIANoState_CalculationLabel
        '
        RIANoState_CalculationLabel.AutoSize = True
        RIANoState_CalculationLabel.Location = New System.Drawing.Point(26, 299)
        RIANoState_CalculationLabel.Name = "RIANoState_CalculationLabel"
        RIANoState_CalculationLabel.Size = New System.Drawing.Size(125, 13)
        RIANoState_CalculationLabel.TabIndex = 22
        RIANoState_CalculationLabel.Text = "RIANo State Calculation:"
        '
        'RIAMultipleZipsLabel
        '
        RIAMultipleZipsLabel.AutoSize = True
        RIAMultipleZipsLabel.Location = New System.Drawing.Point(26, 325)
        RIAMultipleZipsLabel.Name = "RIAMultipleZipsLabel"
        RIAMultipleZipsLabel.Size = New System.Drawing.Size(87, 13)
        RIAMultipleZipsLabel.TabIndex = 24
        RIAMultipleZipsLabel.Text = "RIAMultiple Zips:"
        '
        'DiscreteStateLabel
        '
        DiscreteStateLabel.AutoSize = True
        DiscreteStateLabel.Location = New System.Drawing.Point(26, 353)
        DiscreteStateLabel.Name = "DiscreteStateLabel"
        DiscreteStateLabel.Size = New System.Drawing.Size(77, 13)
        DiscreteStateLabel.TabIndex = 26
        DiscreteStateLabel.Text = "Discrete State:"
        '
        'DiscreteLocalsLabel
        '
        DiscreteLocalsLabel.AutoSize = True
        DiscreteLocalsLabel.Location = New System.Drawing.Point(26, 383)
        DiscreteLocalsLabel.Name = "DiscreteLocalsLabel"
        DiscreteLocalsLabel.Size = New System.Drawing.Size(83, 13)
        DiscreteLocalsLabel.TabIndex = 28
        DiscreteLocalsLabel.Text = "Discrete Locals:"
        '
        'HandlingCodeLabel
        '
        HandlingCodeLabel.AutoSize = True
        HandlingCodeLabel.Location = New System.Drawing.Point(26, 411)
        HandlingCodeLabel.Name = "HandlingCodeLabel"
        HandlingCodeLabel.Size = New System.Drawing.Size(80, 13)
        HandlingCodeLabel.TabIndex = 30
        HandlingCodeLabel.Text = "Handling Code:"
        '
        'VtGeoCodeLabel
        '
        VtGeoCodeLabel.AutoSize = True
        VtGeoCodeLabel.Location = New System.Drawing.Point(26, 437)
        VtGeoCodeLabel.Name = "VtGeoCodeLabel"
        VtGeoCodeLabel.Size = New System.Drawing.Size(70, 13)
        VtGeoCodeLabel.TabIndex = 32
        VtGeoCodeLabel.Text = "vt Geo Code:"
        '
        'DefaultForZipLabel
        '
        DefaultForZipLabel.AutoSize = True
        DefaultForZipLabel.Location = New System.Drawing.Point(26, 465)
        DefaultForZipLabel.Name = "DefaultForZipLabel"
        DefaultForZipLabel.Size = New System.Drawing.Size(80, 13)
        DefaultForZipLabel.TabIndex = 34
        DefaultForZipLabel.Text = "Default For Zip:"
        '
        'DefaultforGEOLabel
        '
        DefaultforGEOLabel.AutoSize = True
        DefaultforGEOLabel.Location = New System.Drawing.Point(26, 495)
        DefaultforGEOLabel.Name = "DefaultforGEOLabel"
        DefaultforGEOLabel.Size = New System.Drawing.Size(82, 13)
        DefaultforGEOLabel.TabIndex = 36
        DefaultforGEOLabel.Text = "Defaultfor GEO:"
        '
        'ChangeByLabel1
        '
        ChangeByLabel1.AutoSize = True
        ChangeByLabel1.Location = New System.Drawing.Point(26, 523)
        ChangeByLabel1.Name = "ChangeByLabel1"
        ChangeByLabel1.Size = New System.Drawing.Size(62, 13)
        ChangeByLabel1.TabIndex = 38
        ChangeByLabel1.Text = "Change By:"
        '
        'ChangeDateLabel1
        '
        ChangeDateLabel1.AutoSize = True
        ChangeDateLabel1.Location = New System.Drawing.Point(26, 550)
        ChangeDateLabel1.Name = "ChangeDateLabel1"
        ChangeDateLabel1.Size = New System.Drawing.Size(73, 13)
        ChangeDateLabel1.TabIndex = 40
        ChangeDateLabel1.Text = "Change Date:"
        '
        'ReportGrossSalesLabel
        '
        ReportGrossSalesLabel.AutoSize = True
        ReportGrossSalesLabel.Location = New System.Drawing.Point(26, 577)
        ReportGrossSalesLabel.Name = "ReportGrossSalesLabel"
        ReportGrossSalesLabel.Size = New System.Drawing.Size(101, 13)
        ReportGrossSalesLabel.TabIndex = 42
        ReportGrossSalesLabel.Text = "Report Gross Sales:"
        '
        'ZipCodeLabel
        '
        ZipCodeLabel.AutoSize = True
        ZipCodeLabel.Location = New System.Drawing.Point(26, 605)
        ZipCodeLabel.Name = "ZipCodeLabel"
        ZipCodeLabel.Size = New System.Drawing.Size(53, 13)
        ZipCodeLabel.TabIndex = 44
        ZipCodeLabel.Text = "Zip Code:"
        '
        'ZipExtensionLabel
        '
        ZipExtensionLabel.AutoSize = True
        ZipExtensionLabel.Location = New System.Drawing.Point(26, 631)
        ZipExtensionLabel.Name = "ZipExtensionLabel"
        ZipExtensionLabel.Size = New System.Drawing.Size(74, 13)
        ZipExtensionLabel.TabIndex = 46
        ZipExtensionLabel.Text = "Zip Extension:"
        '
        'ExcludeStateLabel
        '
        ExcludeStateLabel.AutoSize = True
        ExcludeStateLabel.Location = New System.Drawing.Point(26, 657)
        ExcludeStateLabel.Name = "ExcludeStateLabel"
        ExcludeStateLabel.Size = New System.Drawing.Size(76, 13)
        ExcludeStateLabel.TabIndex = 48
        ExcludeStateLabel.Text = "Exclude State:"
        '
        'ExcludeCountyLabel
        '
        ExcludeCountyLabel.AutoSize = True
        ExcludeCountyLabel.Location = New System.Drawing.Point(26, 683)
        ExcludeCountyLabel.Name = "ExcludeCountyLabel"
        ExcludeCountyLabel.Size = New System.Drawing.Size(84, 13)
        ExcludeCountyLabel.TabIndex = 50
        ExcludeCountyLabel.Text = "Exclude County:"
        '
        'ExcludeCityLabel
        '
        ExcludeCityLabel.AutoSize = True
        ExcludeCityLabel.Location = New System.Drawing.Point(26, 709)
        ExcludeCityLabel.Name = "ExcludeCityLabel"
        ExcludeCityLabel.Size = New System.Drawing.Size(68, 13)
        ExcludeCityLabel.TabIndex = 52
        ExcludeCityLabel.Text = "Exclude City:"
        '
        'ExcludeDistrictLabel
        '
        ExcludeDistrictLabel.AutoSize = True
        ExcludeDistrictLabel.Location = New System.Drawing.Point(26, 735)
        ExcludeDistrictLabel.Name = "ExcludeDistrictLabel"
        ExcludeDistrictLabel.Size = New System.Drawing.Size(83, 13)
        ExcludeDistrictLabel.TabIndex = 54
        ExcludeDistrictLabel.Text = "Exclude District:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabSourceJurisdictionListing)
        Me.TabControl1.Controls.Add(Me.TabSourceJurisdictionDetail)
        Me.TabControl1.Controls.Add(Me.TabReturnJurisdiction)
        Me.TabControl1.Controls.Add(Me.TabSourceJurisdictionOverides)
        Me.TabControl1.Location = New System.Drawing.Point(12, 62)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(859, 382)
        Me.TabControl1.TabIndex = 0
        '
        'TabSourceJurisdictionListing
        '
        Me.TabSourceJurisdictionListing.Controls.Add(Me.DGV_SourceJurisdictionListing)
        Me.TabSourceJurisdictionListing.Location = New System.Drawing.Point(4, 22)
        Me.TabSourceJurisdictionListing.Name = "TabSourceJurisdictionListing"
        Me.TabSourceJurisdictionListing.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSourceJurisdictionListing.Size = New System.Drawing.Size(851, 356)
        Me.TabSourceJurisdictionListing.TabIndex = 0
        Me.TabSourceJurisdictionListing.Text = "Soure Jurisdiction Listing"
        Me.TabSourceJurisdictionListing.UseVisualStyleBackColor = True
        '
        'DGV_SourceJurisdictionListing
        '
        Me.DGV_SourceJurisdictionListing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_SourceJurisdictionListing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_SourceJurisdictionListing.Location = New System.Drawing.Point(3, 3)
        Me.DGV_SourceJurisdictionListing.Name = "DGV_SourceJurisdictionListing"
        Me.DGV_SourceJurisdictionListing.Size = New System.Drawing.Size(845, 350)
        Me.DGV_SourceJurisdictionListing.TabIndex = 0
        '
        'TabSourceJurisdictionDetail
        '
        Me.TabSourceJurisdictionDetail.AutoScroll = True
        Me.TabSourceJurisdictionDetail.Controls.Add(JurisXrefKeyLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.JurisXrefKeyTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(PeriodIDLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.PeriodIDTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(SUImportSourceLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.SUImportSourceTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(SourceStateLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.SourceStateTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(SourceJurisdictionCodeLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.SourceJurisdictionCodeTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(SourceJurisdictionNameLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.SourceJurisdictionNameTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(JurisdictionKeyLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.JurisdictionKeyTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(DefaultJurisdictionLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.DefaultJurisdictionCheckBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(SalesUseLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.SalesUseTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(LocationCodeLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.LocationCodeTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(RIABinderLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.RIABinderTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(ChangeByLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.ChangeByTextBox)
        Me.TabSourceJurisdictionDetail.Controls.Add(ChangeDateLabel)
        Me.TabSourceJurisdictionDetail.Controls.Add(Me.ChangeDateDateTimePicker)
        Me.TabSourceJurisdictionDetail.Location = New System.Drawing.Point(4, 22)
        Me.TabSourceJurisdictionDetail.Name = "TabSourceJurisdictionDetail"
        Me.TabSourceJurisdictionDetail.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSourceJurisdictionDetail.Size = New System.Drawing.Size(851, 356)
        Me.TabSourceJurisdictionDetail.TabIndex = 1
        Me.TabSourceJurisdictionDetail.Text = "Source Jurisdiction Detail"
        Me.TabSourceJurisdictionDetail.UseVisualStyleBackColor = True
        '
        'TabReturnJurisdiction
        '
        Me.TabReturnJurisdiction.AutoScroll = True
        Me.TabReturnJurisdiction.Controls.Add(JurisdictionKeyLabel1)
        Me.TabReturnJurisdiction.Controls.Add(Me.JurisdictionKeyTextBox1)
        Me.TabReturnJurisdiction.Controls.Add(PeriodIDLabel1)
        Me.TabReturnJurisdiction.Controls.Add(Me.PeriodIDTextBox1)
        Me.TabReturnJurisdiction.Controls.Add(StateLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.StateTextBox)
        Me.TabReturnJurisdiction.Controls.Add(GPJurisDescriptionLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.GPJurisDescriptionTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ZipCountyLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ZipCountyTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ZipLocalLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ZipLocalTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ZipTransitLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ZipTransitTextBox)
        Me.TabReturnJurisdiction.Controls.Add(CountyNameLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.CountyNameTextBox)
        Me.TabReturnJurisdiction.Controls.Add(LocationNameLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.LocationNameTextBox)
        Me.TabReturnJurisdiction.Controls.Add(DefaultZipForSalesLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.DefaultZipForSalesCheckBox)
        Me.TabReturnJurisdiction.Controls.Add(RIARecordOverrideLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.RIARecordOverrideTextBox)
        Me.TabReturnJurisdiction.Controls.Add(RIANoState_CalculationLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.RIANoState_CalculationTextBox)
        Me.TabReturnJurisdiction.Controls.Add(RIAMultipleZipsLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.RIAMultipleZipsTextBox)
        Me.TabReturnJurisdiction.Controls.Add(DiscreteStateLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.DiscreteStateCheckBox)
        Me.TabReturnJurisdiction.Controls.Add(DiscreteLocalsLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.DiscreteLocalsCheckBox)
        Me.TabReturnJurisdiction.Controls.Add(HandlingCodeLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.HandlingCodeTextBox)
        Me.TabReturnJurisdiction.Controls.Add(VtGeoCodeLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.VtGeoCodeTextBox)
        Me.TabReturnJurisdiction.Controls.Add(DefaultForZipLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.DefaultForZipCheckBox)
        Me.TabReturnJurisdiction.Controls.Add(DefaultforGEOLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.DefaultforGEOCheckBox)
        Me.TabReturnJurisdiction.Controls.Add(ChangeByLabel1)
        Me.TabReturnJurisdiction.Controls.Add(Me.ChangeByTextBox1)
        Me.TabReturnJurisdiction.Controls.Add(ChangeDateLabel1)
        Me.TabReturnJurisdiction.Controls.Add(Me.ChangeDateDateTimePicker1)
        Me.TabReturnJurisdiction.Controls.Add(ReportGrossSalesLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ReportGrossSalesCheckBox)
        Me.TabReturnJurisdiction.Controls.Add(ZipCodeLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ZipCodeTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ZipExtensionLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ZipExtensionTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ExcludeStateLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ExcludeStateTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ExcludeCountyLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ExcludeCountyTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ExcludeCityLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ExcludeCityTextBox)
        Me.TabReturnJurisdiction.Controls.Add(ExcludeDistrictLabel)
        Me.TabReturnJurisdiction.Controls.Add(Me.ExcludeDistrictTextBox)
        Me.TabReturnJurisdiction.Location = New System.Drawing.Point(4, 22)
        Me.TabReturnJurisdiction.Name = "TabReturnJurisdiction"
        Me.TabReturnJurisdiction.Padding = New System.Windows.Forms.Padding(3)
        Me.TabReturnJurisdiction.Size = New System.Drawing.Size(851, 356)
        Me.TabReturnJurisdiction.TabIndex = 2
        Me.TabReturnJurisdiction.Text = "Return Jurisdiction"
        Me.TabReturnJurisdiction.UseVisualStyleBackColor = True
        '
        'JurisdictionKeyTextBox1
        '
        Me.JurisdictionKeyTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "JurisdictionKey", True))
        Me.JurisdictionKeyTextBox1.Location = New System.Drawing.Point(157, 6)
        Me.JurisdictionKeyTextBox1.Name = "JurisdictionKeyTextBox1"
        Me.JurisdictionKeyTextBox1.Size = New System.Drawing.Size(200, 20)
        Me.JurisdictionKeyTextBox1.TabIndex = 1
        '
        'JurisdictionBindingSource
        '
        Me.JurisdictionBindingSource.DataMember = "Jurisdiction"
        Me.JurisdictionBindingSource.DataSource = Me.DataSource_MaintainJurisdictionXRef
        '
        'DataSource_MaintainJurisdictionXRef
        '
        Me.DataSource_MaintainJurisdictionXRef.DataSetName = "DataSource_MaintainJurisdictionXRef"
        Me.DataSource_MaintainJurisdictionXRef.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PeriodIDTextBox1
        '
        Me.PeriodIDTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "PeriodID", True))
        Me.PeriodIDTextBox1.Location = New System.Drawing.Point(157, 32)
        Me.PeriodIDTextBox1.Name = "PeriodIDTextBox1"
        Me.PeriodIDTextBox1.Size = New System.Drawing.Size(200, 20)
        Me.PeriodIDTextBox1.TabIndex = 3
        '
        'StateTextBox
        '
        Me.StateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "State", True))
        Me.StateTextBox.Location = New System.Drawing.Point(157, 58)
        Me.StateTextBox.Name = "StateTextBox"
        Me.StateTextBox.Size = New System.Drawing.Size(200, 20)
        Me.StateTextBox.TabIndex = 5
        '
        'GPJurisDescriptionTextBox
        '
        Me.GPJurisDescriptionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "GPJurisDescription", True))
        Me.GPJurisDescriptionTextBox.Location = New System.Drawing.Point(157, 84)
        Me.GPJurisDescriptionTextBox.Name = "GPJurisDescriptionTextBox"
        Me.GPJurisDescriptionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.GPJurisDescriptionTextBox.TabIndex = 7
        '
        'ZipCountyTextBox
        '
        Me.ZipCountyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipCounty", True))
        Me.ZipCountyTextBox.Location = New System.Drawing.Point(157, 110)
        Me.ZipCountyTextBox.Name = "ZipCountyTextBox"
        Me.ZipCountyTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ZipCountyTextBox.TabIndex = 9
        '
        'ZipLocalTextBox
        '
        Me.ZipLocalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipLocal", True))
        Me.ZipLocalTextBox.Location = New System.Drawing.Point(157, 136)
        Me.ZipLocalTextBox.Name = "ZipLocalTextBox"
        Me.ZipLocalTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ZipLocalTextBox.TabIndex = 11
        '
        'ZipTransitTextBox
        '
        Me.ZipTransitTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipTransit", True))
        Me.ZipTransitTextBox.Location = New System.Drawing.Point(157, 162)
        Me.ZipTransitTextBox.Name = "ZipTransitTextBox"
        Me.ZipTransitTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ZipTransitTextBox.TabIndex = 13
        '
        'CountyNameTextBox
        '
        Me.CountyNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "CountyName", True))
        Me.CountyNameTextBox.Location = New System.Drawing.Point(157, 188)
        Me.CountyNameTextBox.Name = "CountyNameTextBox"
        Me.CountyNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CountyNameTextBox.TabIndex = 15
        '
        'LocationNameTextBox
        '
        Me.LocationNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "LocationName", True))
        Me.LocationNameTextBox.Location = New System.Drawing.Point(157, 214)
        Me.LocationNameTextBox.Name = "LocationNameTextBox"
        Me.LocationNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.LocationNameTextBox.TabIndex = 17
        '
        'DefaultZipForSalesCheckBox
        '
        Me.DefaultZipForSalesCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DefaultZipForSales", True))
        Me.DefaultZipForSalesCheckBox.Location = New System.Drawing.Point(157, 240)
        Me.DefaultZipForSalesCheckBox.Name = "DefaultZipForSalesCheckBox"
        Me.DefaultZipForSalesCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DefaultZipForSalesCheckBox.TabIndex = 19
        Me.DefaultZipForSalesCheckBox.Text = "CheckBox1"
        Me.DefaultZipForSalesCheckBox.UseVisualStyleBackColor = True
        '
        'RIARecordOverrideTextBox
        '
        Me.RIARecordOverrideTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "RIARecordOverride", True))
        Me.RIARecordOverrideTextBox.Location = New System.Drawing.Point(157, 270)
        Me.RIARecordOverrideTextBox.Name = "RIARecordOverrideTextBox"
        Me.RIARecordOverrideTextBox.Size = New System.Drawing.Size(200, 20)
        Me.RIARecordOverrideTextBox.TabIndex = 21
        '
        'RIANoState_CalculationTextBox
        '
        Me.RIANoState_CalculationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "RIANoState_Calculation", True))
        Me.RIANoState_CalculationTextBox.Location = New System.Drawing.Point(157, 296)
        Me.RIANoState_CalculationTextBox.Name = "RIANoState_CalculationTextBox"
        Me.RIANoState_CalculationTextBox.Size = New System.Drawing.Size(200, 20)
        Me.RIANoState_CalculationTextBox.TabIndex = 23
        '
        'RIAMultipleZipsTextBox
        '
        Me.RIAMultipleZipsTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "RIAMultipleZips", True))
        Me.RIAMultipleZipsTextBox.Location = New System.Drawing.Point(157, 322)
        Me.RIAMultipleZipsTextBox.Name = "RIAMultipleZipsTextBox"
        Me.RIAMultipleZipsTextBox.Size = New System.Drawing.Size(200, 20)
        Me.RIAMultipleZipsTextBox.TabIndex = 25
        '
        'DiscreteStateCheckBox
        '
        Me.DiscreteStateCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DiscreteState", True))
        Me.DiscreteStateCheckBox.Location = New System.Drawing.Point(157, 348)
        Me.DiscreteStateCheckBox.Name = "DiscreteStateCheckBox"
        Me.DiscreteStateCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DiscreteStateCheckBox.TabIndex = 27
        Me.DiscreteStateCheckBox.Text = "CheckBox1"
        Me.DiscreteStateCheckBox.UseVisualStyleBackColor = True
        '
        'DiscreteLocalsCheckBox
        '
        Me.DiscreteLocalsCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DiscreteLocals", True))
        Me.DiscreteLocalsCheckBox.Location = New System.Drawing.Point(157, 378)
        Me.DiscreteLocalsCheckBox.Name = "DiscreteLocalsCheckBox"
        Me.DiscreteLocalsCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DiscreteLocalsCheckBox.TabIndex = 29
        Me.DiscreteLocalsCheckBox.Text = "CheckBox1"
        Me.DiscreteLocalsCheckBox.UseVisualStyleBackColor = True
        '
        'HandlingCodeTextBox
        '
        Me.HandlingCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "HandlingCode", True))
        Me.HandlingCodeTextBox.Location = New System.Drawing.Point(157, 408)
        Me.HandlingCodeTextBox.Name = "HandlingCodeTextBox"
        Me.HandlingCodeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.HandlingCodeTextBox.TabIndex = 31
        '
        'VtGeoCodeTextBox
        '
        Me.VtGeoCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "vtGeoCode", True))
        Me.VtGeoCodeTextBox.Location = New System.Drawing.Point(157, 434)
        Me.VtGeoCodeTextBox.Name = "VtGeoCodeTextBox"
        Me.VtGeoCodeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.VtGeoCodeTextBox.TabIndex = 33
        '
        'DefaultForZipCheckBox
        '
        Me.DefaultForZipCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DefaultForZip", True))
        Me.DefaultForZipCheckBox.Location = New System.Drawing.Point(157, 460)
        Me.DefaultForZipCheckBox.Name = "DefaultForZipCheckBox"
        Me.DefaultForZipCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DefaultForZipCheckBox.TabIndex = 35
        Me.DefaultForZipCheckBox.Text = "CheckBox1"
        Me.DefaultForZipCheckBox.UseVisualStyleBackColor = True
        '
        'DefaultforGEOCheckBox
        '
        Me.DefaultforGEOCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "DefaultforGEO", True))
        Me.DefaultforGEOCheckBox.Location = New System.Drawing.Point(157, 490)
        Me.DefaultforGEOCheckBox.Name = "DefaultforGEOCheckBox"
        Me.DefaultforGEOCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DefaultforGEOCheckBox.TabIndex = 37
        Me.DefaultforGEOCheckBox.Text = "CheckBox1"
        Me.DefaultforGEOCheckBox.UseVisualStyleBackColor = True
        '
        'ChangeByTextBox1
        '
        Me.ChangeByTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ChangeBy", True))
        Me.ChangeByTextBox1.Location = New System.Drawing.Point(157, 520)
        Me.ChangeByTextBox1.Name = "ChangeByTextBox1"
        Me.ChangeByTextBox1.Size = New System.Drawing.Size(200, 20)
        Me.ChangeByTextBox1.TabIndex = 39
        '
        'ChangeDateDateTimePicker1
        '
        Me.ChangeDateDateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JurisdictionBindingSource, "ChangeDate", True))
        Me.ChangeDateDateTimePicker1.Location = New System.Drawing.Point(157, 546)
        Me.ChangeDateDateTimePicker1.Name = "ChangeDateDateTimePicker1"
        Me.ChangeDateDateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.ChangeDateDateTimePicker1.TabIndex = 41
        '
        'ReportGrossSalesCheckBox
        '
        Me.ReportGrossSalesCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionBindingSource, "ReportGrossSales", True))
        Me.ReportGrossSalesCheckBox.Location = New System.Drawing.Point(157, 572)
        Me.ReportGrossSalesCheckBox.Name = "ReportGrossSalesCheckBox"
        Me.ReportGrossSalesCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.ReportGrossSalesCheckBox.TabIndex = 43
        Me.ReportGrossSalesCheckBox.Text = "CheckBox1"
        Me.ReportGrossSalesCheckBox.UseVisualStyleBackColor = True
        '
        'ZipCodeTextBox
        '
        Me.ZipCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipCode", True))
        Me.ZipCodeTextBox.Location = New System.Drawing.Point(157, 602)
        Me.ZipCodeTextBox.Name = "ZipCodeTextBox"
        Me.ZipCodeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ZipCodeTextBox.TabIndex = 45
        '
        'ZipExtensionTextBox
        '
        Me.ZipExtensionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ZipExtension", True))
        Me.ZipExtensionTextBox.Location = New System.Drawing.Point(157, 628)
        Me.ZipExtensionTextBox.Name = "ZipExtensionTextBox"
        Me.ZipExtensionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ZipExtensionTextBox.TabIndex = 47
        '
        'ExcludeStateTextBox
        '
        Me.ExcludeStateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeState", True))
        Me.ExcludeStateTextBox.Location = New System.Drawing.Point(157, 654)
        Me.ExcludeStateTextBox.Name = "ExcludeStateTextBox"
        Me.ExcludeStateTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ExcludeStateTextBox.TabIndex = 49
        '
        'ExcludeCountyTextBox
        '
        Me.ExcludeCountyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeCounty", True))
        Me.ExcludeCountyTextBox.Location = New System.Drawing.Point(157, 680)
        Me.ExcludeCountyTextBox.Name = "ExcludeCountyTextBox"
        Me.ExcludeCountyTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ExcludeCountyTextBox.TabIndex = 51
        '
        'ExcludeCityTextBox
        '
        Me.ExcludeCityTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeCity", True))
        Me.ExcludeCityTextBox.Location = New System.Drawing.Point(157, 706)
        Me.ExcludeCityTextBox.Name = "ExcludeCityTextBox"
        Me.ExcludeCityTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ExcludeCityTextBox.TabIndex = 53
        '
        'ExcludeDistrictTextBox
        '
        Me.ExcludeDistrictTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionBindingSource, "ExcludeDistrict", True))
        Me.ExcludeDistrictTextBox.Location = New System.Drawing.Point(157, 732)
        Me.ExcludeDistrictTextBox.Name = "ExcludeDistrictTextBox"
        Me.ExcludeDistrictTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ExcludeDistrictTextBox.TabIndex = 55
        '
        'TabSourceJurisdictionOverides
        '
        Me.TabSourceJurisdictionOverides.Controls.Add(Me.DGV_SourceJurisdictionOverides)
        Me.TabSourceJurisdictionOverides.Location = New System.Drawing.Point(4, 22)
        Me.TabSourceJurisdictionOverides.Name = "TabSourceJurisdictionOverides"
        Me.TabSourceJurisdictionOverides.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSourceJurisdictionOverides.Size = New System.Drawing.Size(851, 356)
        Me.TabSourceJurisdictionOverides.TabIndex = 3
        Me.TabSourceJurisdictionOverides.Text = "Source Jurisdiction Overides"
        Me.TabSourceJurisdictionOverides.UseVisualStyleBackColor = True
        '
        'JurisdictionXRefOverrideBindingSource
        '
        Me.JurisdictionXRefOverrideBindingSource.DataMember = "JurisdictionXRefOverride"
        Me.JurisdictionXRefOverrideBindingSource.DataSource = Me.DataSource_MaintainJurisdictionXRef
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.JurisdictionTableAdapter = Me.JurisdictionTableAdapter
        Me.TableAdapterManager.JurisdictionXRefOverrideTableAdapter = Nothing
        Me.TableAdapterManager.JurisdictionXRefTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainJurisdictionXRefTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'JurisdictionTableAdapter
        '
        Me.JurisdictionTableAdapter.ClearBeforeFill = True
        '
        'JurisdictionXRefOverrideTableAdapter
        '
        Me.JurisdictionXRefOverrideTableAdapter.ClearBeforeFill = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(792, 454)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboPeriod
        '
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(85, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 3
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(40, 43)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(37, 13)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Period"
        '
        'JurisdictionXRefBindingSource
        '
        Me.JurisdictionXRefBindingSource.DataMember = "JurisdictionXRef"
        Me.JurisdictionXRefBindingSource.DataSource = Me.DataSource_MaintainJurisdictionXRef
        '
        'JurisdictionXRefTableAdapter
        '
        Me.JurisdictionXRefTableAdapter.ClearBeforeFill = True
        '
        'JurisXrefKeyLabel
        '
        JurisXrefKeyLabel.AutoSize = True
        JurisXrefKeyLabel.Location = New System.Drawing.Point(6, 13)
        JurisXrefKeyLabel.Name = "JurisXrefKeyLabel"
        JurisXrefKeyLabel.Size = New System.Drawing.Size(74, 13)
        JurisXrefKeyLabel.TabIndex = 0
        JurisXrefKeyLabel.Text = "Juris Xref Key:"
        '
        'JurisXrefKeyTextBox
        '
        Me.JurisXrefKeyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "JurisXrefKey", True))
        Me.JurisXrefKeyTextBox.Location = New System.Drawing.Point(142, 10)
        Me.JurisXrefKeyTextBox.Name = "JurisXrefKeyTextBox"
        Me.JurisXrefKeyTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JurisXrefKeyTextBox.TabIndex = 1
        '
        'PeriodIDLabel
        '
        PeriodIDLabel.AutoSize = True
        PeriodIDLabel.Location = New System.Drawing.Point(6, 39)
        PeriodIDLabel.Name = "PeriodIDLabel"
        PeriodIDLabel.Size = New System.Drawing.Size(54, 13)
        PeriodIDLabel.TabIndex = 2
        PeriodIDLabel.Text = "Period ID:"
        '
        'PeriodIDTextBox
        '
        Me.PeriodIDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "PeriodID", True))
        Me.PeriodIDTextBox.Location = New System.Drawing.Point(142, 36)
        Me.PeriodIDTextBox.Name = "PeriodIDTextBox"
        Me.PeriodIDTextBox.Size = New System.Drawing.Size(200, 20)
        Me.PeriodIDTextBox.TabIndex = 3
        '
        'SUImportSourceLabel
        '
        SUImportSourceLabel.AutoSize = True
        SUImportSourceLabel.Location = New System.Drawing.Point(6, 65)
        SUImportSourceLabel.Name = "SUImportSourceLabel"
        SUImportSourceLabel.Size = New System.Drawing.Size(91, 13)
        SUImportSourceLabel.TabIndex = 4
        SUImportSourceLabel.Text = "SUImport Source:"
        '
        'SUImportSourceTextBox
        '
        Me.SUImportSourceTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "SUImportSource", True))
        Me.SUImportSourceTextBox.Location = New System.Drawing.Point(142, 62)
        Me.SUImportSourceTextBox.Name = "SUImportSourceTextBox"
        Me.SUImportSourceTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SUImportSourceTextBox.TabIndex = 5
        '
        'SourceStateLabel
        '
        SourceStateLabel.AutoSize = True
        SourceStateLabel.Location = New System.Drawing.Point(6, 91)
        SourceStateLabel.Name = "SourceStateLabel"
        SourceStateLabel.Size = New System.Drawing.Size(72, 13)
        SourceStateLabel.TabIndex = 6
        SourceStateLabel.Text = "Source State:"
        '
        'SourceStateTextBox
        '
        Me.SourceStateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "SourceState", True))
        Me.SourceStateTextBox.Location = New System.Drawing.Point(142, 88)
        Me.SourceStateTextBox.Name = "SourceStateTextBox"
        Me.SourceStateTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SourceStateTextBox.TabIndex = 7
        '
        'SourceJurisdictionCodeLabel
        '
        SourceJurisdictionCodeLabel.AutoSize = True
        SourceJurisdictionCodeLabel.Location = New System.Drawing.Point(6, 117)
        SourceJurisdictionCodeLabel.Name = "SourceJurisdictionCodeLabel"
        SourceJurisdictionCodeLabel.Size = New System.Drawing.Size(127, 13)
        SourceJurisdictionCodeLabel.TabIndex = 8
        SourceJurisdictionCodeLabel.Text = "Source Jurisdiction Code:"
        '
        'SourceJurisdictionCodeTextBox
        '
        Me.SourceJurisdictionCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "SourceJurisdictionCode", True))
        Me.SourceJurisdictionCodeTextBox.Location = New System.Drawing.Point(142, 114)
        Me.SourceJurisdictionCodeTextBox.Name = "SourceJurisdictionCodeTextBox"
        Me.SourceJurisdictionCodeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SourceJurisdictionCodeTextBox.TabIndex = 9
        '
        'SourceJurisdictionNameLabel
        '
        SourceJurisdictionNameLabel.AutoSize = True
        SourceJurisdictionNameLabel.Location = New System.Drawing.Point(6, 143)
        SourceJurisdictionNameLabel.Name = "SourceJurisdictionNameLabel"
        SourceJurisdictionNameLabel.Size = New System.Drawing.Size(130, 13)
        SourceJurisdictionNameLabel.TabIndex = 10
        SourceJurisdictionNameLabel.Text = "Source Jurisdiction Name:"
        '
        'SourceJurisdictionNameTextBox
        '
        Me.SourceJurisdictionNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "SourceJurisdictionName", True))
        Me.SourceJurisdictionNameTextBox.Location = New System.Drawing.Point(142, 140)
        Me.SourceJurisdictionNameTextBox.Name = "SourceJurisdictionNameTextBox"
        Me.SourceJurisdictionNameTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SourceJurisdictionNameTextBox.TabIndex = 11
        '
        'JurisdictionKeyLabel
        '
        JurisdictionKeyLabel.AutoSize = True
        JurisdictionKeyLabel.Location = New System.Drawing.Point(6, 169)
        JurisdictionKeyLabel.Name = "JurisdictionKeyLabel"
        JurisdictionKeyLabel.Size = New System.Drawing.Size(83, 13)
        JurisdictionKeyLabel.TabIndex = 12
        JurisdictionKeyLabel.Text = "Jurisdiction Key:"
        '
        'JurisdictionKeyTextBox
        '
        Me.JurisdictionKeyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "JurisdictionKey", True))
        Me.JurisdictionKeyTextBox.Location = New System.Drawing.Point(142, 166)
        Me.JurisdictionKeyTextBox.Name = "JurisdictionKeyTextBox"
        Me.JurisdictionKeyTextBox.Size = New System.Drawing.Size(200, 20)
        Me.JurisdictionKeyTextBox.TabIndex = 13
        '
        'DefaultJurisdictionLabel
        '
        DefaultJurisdictionLabel.AutoSize = True
        DefaultJurisdictionLabel.Location = New System.Drawing.Point(6, 197)
        DefaultJurisdictionLabel.Name = "DefaultJurisdictionLabel"
        DefaultJurisdictionLabel.Size = New System.Drawing.Size(99, 13)
        DefaultJurisdictionLabel.TabIndex = 14
        DefaultJurisdictionLabel.Text = "Default Jurisdiction:"
        '
        'DefaultJurisdictionCheckBox
        '
        Me.DefaultJurisdictionCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.JurisdictionXRefBindingSource, "DefaultJurisdiction", True))
        Me.DefaultJurisdictionCheckBox.Location = New System.Drawing.Point(142, 192)
        Me.DefaultJurisdictionCheckBox.Name = "DefaultJurisdictionCheckBox"
        Me.DefaultJurisdictionCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.DefaultJurisdictionCheckBox.TabIndex = 15
        Me.DefaultJurisdictionCheckBox.Text = "CheckBox1"
        Me.DefaultJurisdictionCheckBox.UseVisualStyleBackColor = True
        '
        'SalesUseLabel
        '
        SalesUseLabel.AutoSize = True
        SalesUseLabel.Location = New System.Drawing.Point(6, 225)
        SalesUseLabel.Name = "SalesUseLabel"
        SalesUseLabel.Size = New System.Drawing.Size(58, 13)
        SalesUseLabel.TabIndex = 16
        SalesUseLabel.Text = "Sales Use:"
        '
        'SalesUseTextBox
        '
        Me.SalesUseTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "SalesUse", True))
        Me.SalesUseTextBox.Location = New System.Drawing.Point(142, 222)
        Me.SalesUseTextBox.Name = "SalesUseTextBox"
        Me.SalesUseTextBox.Size = New System.Drawing.Size(200, 20)
        Me.SalesUseTextBox.TabIndex = 17
        '
        'LocationCodeLabel
        '
        LocationCodeLabel.AutoSize = True
        LocationCodeLabel.Location = New System.Drawing.Point(6, 251)
        LocationCodeLabel.Name = "LocationCodeLabel"
        LocationCodeLabel.Size = New System.Drawing.Size(79, 13)
        LocationCodeLabel.TabIndex = 18
        LocationCodeLabel.Text = "Location Code:"
        '
        'LocationCodeTextBox
        '
        Me.LocationCodeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "LocationCode", True))
        Me.LocationCodeTextBox.Location = New System.Drawing.Point(142, 248)
        Me.LocationCodeTextBox.Name = "LocationCodeTextBox"
        Me.LocationCodeTextBox.Size = New System.Drawing.Size(200, 20)
        Me.LocationCodeTextBox.TabIndex = 19
        '
        'RIABinderLabel
        '
        RIABinderLabel.AutoSize = True
        RIABinderLabel.Location = New System.Drawing.Point(6, 277)
        RIABinderLabel.Name = "RIABinderLabel"
        RIABinderLabel.Size = New System.Drawing.Size(58, 13)
        RIABinderLabel.TabIndex = 20
        RIABinderLabel.Text = "RIABinder:"
        '
        'RIABinderTextBox
        '
        Me.RIABinderTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "RIABinder", True))
        Me.RIABinderTextBox.Location = New System.Drawing.Point(142, 274)
        Me.RIABinderTextBox.Name = "RIABinderTextBox"
        Me.RIABinderTextBox.Size = New System.Drawing.Size(200, 20)
        Me.RIABinderTextBox.TabIndex = 21
        '
        'ChangeByLabel
        '
        ChangeByLabel.AutoSize = True
        ChangeByLabel.Location = New System.Drawing.Point(6, 303)
        ChangeByLabel.Name = "ChangeByLabel"
        ChangeByLabel.Size = New System.Drawing.Size(62, 13)
        ChangeByLabel.TabIndex = 22
        ChangeByLabel.Text = "Change By:"
        '
        'ChangeByTextBox
        '
        Me.ChangeByTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JurisdictionXRefBindingSource, "ChangeBy", True))
        Me.ChangeByTextBox.Location = New System.Drawing.Point(142, 300)
        Me.ChangeByTextBox.Name = "ChangeByTextBox"
        Me.ChangeByTextBox.Size = New System.Drawing.Size(200, 20)
        Me.ChangeByTextBox.TabIndex = 23
        '
        'ChangeDateLabel
        '
        ChangeDateLabel.AutoSize = True
        ChangeDateLabel.Location = New System.Drawing.Point(6, 330)
        ChangeDateLabel.Name = "ChangeDateLabel"
        ChangeDateLabel.Size = New System.Drawing.Size(73, 13)
        ChangeDateLabel.TabIndex = 24
        ChangeDateLabel.Text = "Change Date:"
        '
        'ChangeDateDateTimePicker
        '
        Me.ChangeDateDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.JurisdictionXRefBindingSource, "ChangeDate", True))
        Me.ChangeDateDateTimePicker.Location = New System.Drawing.Point(142, 326)
        Me.ChangeDateDateTimePicker.Name = "ChangeDateDateTimePicker"
        Me.ChangeDateDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.ChangeDateDateTimePicker.TabIndex = 25
        '
        'DGV_SourceJurisdictionOverides
        '
        Me.DGV_SourceJurisdictionOverides.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_SourceJurisdictionOverides.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_SourceJurisdictionOverides.Location = New System.Drawing.Point(3, 3)
        Me.DGV_SourceJurisdictionOverides.Name = "DGV_SourceJurisdictionOverides"
        Me.DGV_SourceJurisdictionOverides.Size = New System.Drawing.Size(845, 350)
        Me.DGV_SourceJurisdictionOverides.TabIndex = 0
        '
        'frmMaintainJurisdictionXRef
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(883, 505)
        Me.Controls.Add(Me.lblPeriod)
        Me.Controls.Add(Me.cboPeriod)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainJurisdictionXRef"
        Me.Text = "Maintain Jurisdiction XRef"
        Me.TabControl1.ResumeLayout(False)
        Me.TabSourceJurisdictionListing.ResumeLayout(False)
        CType(Me.DGV_SourceJurisdictionListing, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSourceJurisdictionDetail.ResumeLayout(False)
        Me.TabSourceJurisdictionDetail.PerformLayout()
        Me.TabReturnJurisdiction.ResumeLayout(False)
        Me.TabReturnJurisdiction.PerformLayout()
        CType(Me.JurisdictionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSource_MaintainJurisdictionXRef, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSourceJurisdictionOverides.ResumeLayout(False)
        CType(Me.JurisdictionXRefOverrideBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JurisdictionXRefBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_SourceJurisdictionOverides, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabSourceJurisdictionListing As TabPage
    Friend WithEvents TabSourceJurisdictionDetail As TabPage
    Friend WithEvents TabReturnJurisdiction As TabPage
    Friend WithEvents TabSourceJurisdictionOverides As TabPage
    Friend WithEvents DataSource_MaintainJurisdictionXRef As DataSource_MaintainJurisdictionXRef
    Friend WithEvents TableAdapterManager As DataSource_MaintainJurisdictionXRefTableAdapters.TableAdapterManager
    Friend WithEvents JurisdictionTableAdapter As DataSource_MaintainJurisdictionXRefTableAdapters.JurisdictionTableAdapter
    Friend WithEvents JurisdictionBindingSource As BindingSource
    Friend WithEvents JurisdictionKeyTextBox1 As TextBox
    Friend WithEvents PeriodIDTextBox1 As TextBox
    Friend WithEvents StateTextBox As TextBox
    Friend WithEvents GPJurisDescriptionTextBox As TextBox
    Friend WithEvents ZipCountyTextBox As TextBox
    Friend WithEvents ZipLocalTextBox As TextBox
    Friend WithEvents ZipTransitTextBox As TextBox
    Friend WithEvents CountyNameTextBox As TextBox
    Friend WithEvents LocationNameTextBox As TextBox
    Friend WithEvents DefaultZipForSalesCheckBox As CheckBox
    Friend WithEvents RIARecordOverrideTextBox As TextBox
    Friend WithEvents RIANoState_CalculationTextBox As TextBox
    Friend WithEvents RIAMultipleZipsTextBox As TextBox
    Friend WithEvents DiscreteStateCheckBox As CheckBox
    Friend WithEvents DiscreteLocalsCheckBox As CheckBox
    Friend WithEvents HandlingCodeTextBox As TextBox
    Friend WithEvents VtGeoCodeTextBox As TextBox
    Friend WithEvents DefaultForZipCheckBox As CheckBox
    Friend WithEvents DefaultforGEOCheckBox As CheckBox
    Friend WithEvents ChangeByTextBox1 As TextBox
    Friend WithEvents ChangeDateDateTimePicker1 As DateTimePicker
    Friend WithEvents ReportGrossSalesCheckBox As CheckBox
    Friend WithEvents ZipCodeTextBox As TextBox
    Friend WithEvents ZipExtensionTextBox As TextBox
    Friend WithEvents ExcludeStateTextBox As TextBox
    Friend WithEvents ExcludeCountyTextBox As TextBox
    Friend WithEvents ExcludeCityTextBox As TextBox
    Friend WithEvents ExcludeDistrictTextBox As TextBox
    Friend WithEvents JurisdictionXRefOverrideBindingSource As BindingSource
    Friend WithEvents JurisdictionXRefOverrideTableAdapter As DataSource_MaintainJurisdictionXRefTableAdapters.JurisdictionXRefOverrideTableAdapter
    Friend WithEvents btnClose As Button
    Friend WithEvents DGV_SourceJurisdictionListing As DataGridView
    Friend WithEvents cboPeriod As ComboBox
    Friend WithEvents lblPeriod As Label
    Friend WithEvents JurisdictionXRefBindingSource As BindingSource
    Friend WithEvents JurisdictionXRefTableAdapter As DataSource_MaintainJurisdictionXRefTableAdapters.JurisdictionXRefTableAdapter
    Friend WithEvents JurisXrefKeyTextBox As TextBox
    Friend WithEvents PeriodIDTextBox As TextBox
    Friend WithEvents SUImportSourceTextBox As TextBox
    Friend WithEvents SourceStateTextBox As TextBox
    Friend WithEvents SourceJurisdictionCodeTextBox As TextBox
    Friend WithEvents SourceJurisdictionNameTextBox As TextBox
    Friend WithEvents JurisdictionKeyTextBox As TextBox
    Friend WithEvents DefaultJurisdictionCheckBox As CheckBox
    Friend WithEvents SalesUseTextBox As TextBox
    Friend WithEvents LocationCodeTextBox As TextBox
    Friend WithEvents RIABinderTextBox As TextBox
    Friend WithEvents ChangeByTextBox As TextBox
    Friend WithEvents ChangeDateDateTimePicker As DateTimePicker
    Friend WithEvents DGV_SourceJurisdictionOverides As DataGridView
End Class

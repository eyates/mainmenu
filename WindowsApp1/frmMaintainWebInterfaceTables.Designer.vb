﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainWebInterfaceTables
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainWebInterfaceTables))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabFacility = New System.Windows.Forms.TabPage()
        Me.FacilityDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FacilityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSource_MaintainWebInterfaceTables = New WindowsApp1.DataSource_MaintainWebInterfaceTables()
        Me.TabJNumber = New System.Windows.Forms.TabPage()
        Me.JNumberDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JNumberBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabWebUser = New System.Windows.Forms.TabPage()
        Me.WebUserDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WebUserBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FacilityTableAdapter = New WindowsApp1.DataSource_MaintainWebInterfaceTablesTableAdapters.FacilityTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainWebInterfaceTablesTableAdapters.TableAdapterManager()
        Me.JNumberTableAdapter = New WindowsApp1.DataSource_MaintainWebInterfaceTablesTableAdapters.JNumberTableAdapter()
        Me.WebUserTableAdapter = New WindowsApp1.DataSource_MaintainWebInterfaceTablesTableAdapters.WebUserTableAdapter()
        Me.FacilityBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.FacilityBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabFacility.SuspendLayout()
        CType(Me.FacilityDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FacilityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSource_MaintainWebInterfaceTables, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabJNumber.SuspendLayout()
        CType(Me.JNumberDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JNumberBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabWebUser.SuspendLayout()
        CType(Me.WebUserDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WebUserBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FacilityBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FacilityBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabFacility)
        Me.TabControl1.Controls.Add(Me.TabJNumber)
        Me.TabControl1.Controls.Add(Me.TabWebUser)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(799, 340)
        Me.TabControl1.TabIndex = 0
        '
        'TabFacility
        '
        Me.TabFacility.Controls.Add(Me.FacilityDataGridView)
        Me.TabFacility.Location = New System.Drawing.Point(4, 22)
        Me.TabFacility.Name = "TabFacility"
        Me.TabFacility.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFacility.Size = New System.Drawing.Size(791, 314)
        Me.TabFacility.TabIndex = 0
        Me.TabFacility.Text = "Facility"
        Me.TabFacility.UseVisualStyleBackColor = True
        '
        'FacilityDataGridView
        '
        Me.FacilityDataGridView.AutoGenerateColumns = False
        Me.FacilityDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.FacilityDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.FacilityDataGridView.DataSource = Me.FacilityBindingSource
        Me.FacilityDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.FacilityDataGridView.Name = "FacilityDataGridView"
        Me.FacilityDataGridView.Size = New System.Drawing.Size(791, 315)
        Me.FacilityDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "dbFacilityCode"
        Me.DataGridViewTextBoxColumn1.HeaderText = "dbFacilityCode"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Group2"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Group2"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'FacilityBindingSource
        '
        Me.FacilityBindingSource.DataMember = "Facility"
        Me.FacilityBindingSource.DataSource = Me.DataSource_MaintainWebInterfaceTables
        '
        'DataSource_MaintainWebInterfaceTables
        '
        Me.DataSource_MaintainWebInterfaceTables.DataSetName = "DataSource_MaintainWebInterfaceTables"
        Me.DataSource_MaintainWebInterfaceTables.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabJNumber
        '
        Me.TabJNumber.Controls.Add(Me.JNumberDataGridView)
        Me.TabJNumber.Location = New System.Drawing.Point(4, 22)
        Me.TabJNumber.Name = "TabJNumber"
        Me.TabJNumber.Padding = New System.Windows.Forms.Padding(3)
        Me.TabJNumber.Size = New System.Drawing.Size(791, 362)
        Me.TabJNumber.TabIndex = 1
        Me.TabJNumber.Text = "JNumber"
        Me.TabJNumber.UseVisualStyleBackColor = True
        '
        'JNumberDataGridView
        '
        Me.JNumberDataGridView.AutoGenerateColumns = False
        Me.JNumberDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.JNumberDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3})
        Me.JNumberDataGridView.DataSource = Me.JNumberBindingSource
        Me.JNumberDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.JNumberDataGridView.Name = "JNumberDataGridView"
        Me.JNumberDataGridView.Size = New System.Drawing.Size(197, 323)
        Me.JNumberDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "JNumber"
        Me.DataGridViewTextBoxColumn3.HeaderText = "JNumber"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'JNumberBindingSource
        '
        Me.JNumberBindingSource.DataMember = "JNumber"
        Me.JNumberBindingSource.DataSource = Me.DataSource_MaintainWebInterfaceTables
        '
        'TabWebUser
        '
        Me.TabWebUser.Controls.Add(Me.WebUserDataGridView)
        Me.TabWebUser.Location = New System.Drawing.Point(4, 22)
        Me.TabWebUser.Name = "TabWebUser"
        Me.TabWebUser.Padding = New System.Windows.Forms.Padding(3)
        Me.TabWebUser.Size = New System.Drawing.Size(791, 362)
        Me.TabWebUser.TabIndex = 2
        Me.TabWebUser.Text = "Web User"
        Me.TabWebUser.UseVisualStyleBackColor = True
        '
        'WebUserDataGridView
        '
        Me.WebUserDataGridView.AutoGenerateColumns = False
        Me.WebUserDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.WebUserDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.WebUserDataGridView.DataSource = Me.WebUserBindingSource
        Me.WebUserDataGridView.Location = New System.Drawing.Point(3, 3)
        Me.WebUserDataGridView.Name = "WebUserDataGridView"
        Me.WebUserDataGridView.Size = New System.Drawing.Size(785, 310)
        Me.WebUserDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "dbUserCnt"
        Me.DataGridViewTextBoxColumn4.HeaderText = "dbUserCnt"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "dbUserID"
        Me.DataGridViewTextBoxColumn5.HeaderText = "dbUserID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "dbUserProfile"
        Me.DataGridViewTextBoxColumn6.HeaderText = "dbUserProfile"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "dbFirst"
        Me.DataGridViewTextBoxColumn7.HeaderText = "dbFirst"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "dbLast"
        Me.DataGridViewTextBoxColumn8.HeaderText = "dbLast"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'WebUserBindingSource
        '
        Me.WebUserBindingSource.DataMember = "WebUser"
        Me.WebUserBindingSource.DataSource = Me.DataSource_MaintainWebInterfaceTables
        '
        'FacilityTableAdapter
        '
        Me.FacilityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.FacilityTableAdapter = Me.FacilityTableAdapter
        Me.TableAdapterManager.JNumberTableAdapter = Me.JNumberTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainWebInterfaceTablesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WebUserTableAdapter = Me.WebUserTableAdapter
        '
        'JNumberTableAdapter
        '
        Me.JNumberTableAdapter.ClearBeforeFill = True
        '
        'WebUserTableAdapter
        '
        Me.WebUserTableAdapter.ClearBeforeFill = True
        '
        'FacilityBindingNavigator
        '
        Me.FacilityBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.FacilityBindingNavigator.BindingSource = Me.FacilityBindingSource
        Me.FacilityBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.FacilityBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.FacilityBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FacilityBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.FacilityBindingNavigatorSaveItem})
        Me.FacilityBindingNavigator.Location = New System.Drawing.Point(0, 418)
        Me.FacilityBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.FacilityBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.FacilityBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.FacilityBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.FacilityBindingNavigator.Name = "FacilityBindingNavigator"
        Me.FacilityBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.FacilityBindingNavigator.Size = New System.Drawing.Size(1017, 25)
        Me.FacilityBindingNavigator.TabIndex = 1
        Me.FacilityBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'FacilityBindingNavigatorSaveItem
        '
        Me.FacilityBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.FacilityBindingNavigatorSaveItem.Image = CType(resources.GetObject("FacilityBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.FacilityBindingNavigatorSaveItem.Name = "FacilityBindingNavigatorSaveItem"
        Me.FacilityBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.FacilityBindingNavigatorSaveItem.Text = "Save Data"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(736, 358)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainWebInterfaceTables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 443)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.FacilityBindingNavigator)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainWebInterfaceTables"
        Me.Text = "Maintain Web Interface Tables"
        Me.TabControl1.ResumeLayout(False)
        Me.TabFacility.ResumeLayout(False)
        CType(Me.FacilityDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FacilityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSource_MaintainWebInterfaceTables, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabJNumber.ResumeLayout(False)
        CType(Me.JNumberDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JNumberBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabWebUser.ResumeLayout(False)
        CType(Me.WebUserDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WebUserBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FacilityBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FacilityBindingNavigator.ResumeLayout(False)
        Me.FacilityBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabFacility As TabPage
    Friend WithEvents TabJNumber As TabPage
    Friend WithEvents TabWebUser As TabPage
    Friend WithEvents DataSource_MaintainWebInterfaceTables As DataSource_MaintainWebInterfaceTables
    Friend WithEvents FacilityBindingSource As BindingSource
    Friend WithEvents FacilityTableAdapter As DataSource_MaintainWebInterfaceTablesTableAdapters.FacilityTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_MaintainWebInterfaceTablesTableAdapters.TableAdapterManager
    Friend WithEvents FacilityBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents FacilityBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents FacilityDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents JNumberTableAdapter As DataSource_MaintainWebInterfaceTablesTableAdapters.JNumberTableAdapter
    Friend WithEvents JNumberBindingSource As BindingSource
    Friend WithEvents JNumberDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents WebUserTableAdapter As DataSource_MaintainWebInterfaceTablesTableAdapters.WebUserTableAdapter
    Friend WithEvents WebUserBindingSource As BindingSource
    Friend WithEvents WebUserDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents btnClose As Button
End Class

﻿Imports System.Data.SqlClient

Public Class frmMaintainStates
    Private Sub StateBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.StateBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.SalesUse_UniDataSet1)
    End Sub

    Private Sub FormMaintainStates_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SalesUse_UniDataSet1.State' table. You can move, or remove it, as needed.
        LoadStateCombo()
        LoadDGV_HandlingCodes()
        Me.StateTableAdapter.Fill(Me.SalesUse_UniDataSet1.State, cboState.Text)
        Me.FilingEntityStateTableAdapter.Fill(Me.SalesUse_UniDataSet1.FilingEntityState, cboState.Text)

    End Sub
    Private Sub LoadStateCombo()
        Dim SQL As String = "SELECT * FROM State with(nolock) ORDER BY State"
        Using connection As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(SQL, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                cboState.ValueMember = "State"
                cboState.DisplayMember = "State"
                cboState.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub LoadDGV_HandlingCodes()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT RIABinderStateHandlingCode.HandlingCodeKey, 
RIABinderStateHandlingCode.RIABinder, 
RIABinderStateHandlingCode.State,
RIABinderStateHandlingCode.HandlingCode, 
RIABinderStateHandlingCode.PeriodID, 
RIABinderStateHandlingCode.ManufacturingRate, 
RIABinderStateHandlingCode.SalesUse, 
HandlingCode.HandlingDescription, 
RIABinderStateHandlingCode.StateRateFrom, 
RIABinderStateHandlingCode.StateRateTo, 
RIABinderStateHandlingCode.CreditPercentage, 
RIABinderStateHandlingCode.OmitFromASCII, 
RIABinderStateHandlingCode.TaxCode 
FROM HandlingCode 
INNER JOIN RIABinderStateHandlingCode ON HandlingCode.HandlingCode=RIABinderStateHandlingCode.HandlingCode; "
        'Commented out filter that isn't working at the moment
        'If cboState.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND RIABinderStateHandlingCode.State = '" & Trim(cboState.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_HandlingCodes.DataSource = dtRecord
    End Sub

    Private Sub cboState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboState.SelectedIndexChanged
        Me.StateTableAdapter.Fill(Me.SalesUse_UniDataSet1.State, cboState.SelectedValue)
        Me.FilingEntityStateTableAdapter.Fill(Me.SalesUse_UniDataSet1.FilingEntityState, cboState.SelectedValue)
        LoadDGV_HandlingCodes()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
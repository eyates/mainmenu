﻿Public Class FormStartUp
    Private Sub MaintainGPJurisdictions_Click(sender As Object, e As EventArgs) Handles btnMaintainGPJurisdictions.Click
        frmMaintainJurisdiction.Show()
    End Sub

    Private Sub ButtonMaintainStates_Click(sender As Object, e As EventArgs) Handles btnMaintainStates.Click
        frmMaintainStates.Show()
    End Sub

    Private Sub ButtonMaintainFilingEntities_Click(sender As Object, e As EventArgs) Handles btnMaintainFilingEntities.Click
        frmMaintainFilingEntities.Show()
    End Sub

    Private Sub btnMaintainContacts_Click(sender As Object, e As EventArgs) Handles btnMaintainContacts.Click
        frmMaintainContacts.Show()
    End Sub

    Private Sub btnMaintainDivision_Click(sender As Object, e As EventArgs) Handles btnMaintainDivision.Click
        frmMaintainDivision.Show()
    End Sub

    Private Sub btnMaintainUsers_Click(sender As Object, e As EventArgs) Handles btnMaintainUsers.Click
        frmMaintainUsers.Show()
    End Sub

    Private Sub btnMaintainuserGroups_Click(sender As Object, e As EventArgs) Handles btnMaintainuserGroups.Click
        frmMaintainuserGroups.Show()
    End Sub

    Private Sub btnMaintainWebInterfaceTables_Click(sender As Object, e As EventArgs) Handles btnMaintainWebInterfaceTables.Click
        frmMaintainWebInterfaceTables.Show()
    End Sub

    Private Sub btnMaintainVertexMappingTables_Click(sender As Object, e As EventArgs) Handles btnMaintainVertexMappingTables.Click
        frmMaintainVertexMappingTables.Show()
    End Sub

    Private Sub btnMaintainDeterminationMapping_Click(sender As Object, e As EventArgs) Handles btnMaintainDeterminationMapping.Click
        frmMaintainDeterminationMapping.Show()
    End Sub

    Private Sub btnMaintainAR027Mapping_Click(sender As Object, e As EventArgs) Handles btnMaintainAR027Mapping.Click
        frmMaintainAR027Mapping.Show()
    End Sub

    Private Sub btnMaintainJurisdictionXRef_Click(sender As Object, e As EventArgs) Handles btnMaintainJurisdictionXRef.Click
        frmMaintainJurisdictionXRef.Show()
    End Sub

    Private Sub btnMaintainRIABinders_Click(sender As Object, e As EventArgs) Handles btnMaintainRIABinders.Click
        frmMaintainRIABinders.Show()
    End Sub

    Private Sub btnMaintainRIABinderStateReturn_Click(sender As Object, e As EventArgs) Handles btnMaintainRIABinderStateReturn.Click
        frmMaintainRIABinderStateReturn.Show()
    End Sub

    Private Sub btnMaintainAccountsToExlude_Click(sender As Object, e As EventArgs) Handles btnMaintainAccountsToExlude.Click
        frmMaintainAccountsToExclude.Show()
    End Sub

    Private Sub btnMaintainLocationCodeOveride_Click(sender As Object, e As EventArgs) Handles btnMaintainLocationCodeOveride.Click
        frmMaintainLocationCodeOveride.Show()
    End Sub

    Private Sub btnMaintainGroup1ReportingGroups_Click(sender As Object, e As EventArgs) Handles btnMaintainGroup1ReportingGroups.Click
        frmMaintainingGroup1ReportingGroups.Show()
    End Sub

    Private Sub btnViewSystemIntegrityErrors_Click(sender As Object, e As EventArgs) Handles btnViewSystemIntegrityErrors.Click
        frmViewSystemIntegrityErrors.Show()
    End Sub

    Private Sub btnViewJurisdictionChecks_Click(sender As Object, e As EventArgs) Handles btnViewJurisdictionChecks.Click
        frmViewJurisdictionChecks.Show()
    End Sub

    Private Sub btnYearEndMaintenance_Click(sender As Object, e As EventArgs) Handles btnYearEndMaintenance.Click
        frmYearEndMaintenance.Show()
    End Sub

    Private Sub btnMaintainReferenceTables_Click(sender As Object, e As EventArgs) Handles btnMaintainReferenceTables.Click
        frmMaintainReferenceTables.Show()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
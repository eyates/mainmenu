﻿Imports System.Data.SqlClient

'Load Form Event. It loads all the Data Grid Views (DGV)
Public Class frmViewJurisdictionChecks
    Private Sub frmViewJurisdictionChecks_Load(sender As Object, e As EventArgs) Handles Me.Load
        LoadComboBox()
        LoadDGV_NoZips()
        LoadDGV_DiscreteState()
        LoadDGV_MultiZip()
        LoadDGV_Zip99999Flags()
        LoadDGV_Zip99999NotAllowed()
    End Sub

    'Load Main Combo Box with Period IDs
    Private Sub LoadComboBox()
        Dim sqlquery As String
        sqlquery = "SELECT Period.Period FROM Period ORDER BY Period.Period DESC;"
        Using connection As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(sqlquery, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                ' as an example set the ValueMember and DisplayMember'
                ' to two columns of the returned table'
                cboPeriod.ValueMember = "Period"
                cboPeriod.DisplayMember = "Period"
                cboPeriod.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub LoadDGV_RIARates()
        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT Jurisdiction.PeriodID, Jurisdiction.JurisdictionKey, Jurisdiction.GPJurisDescription, Jurisdiction.State, Jurisdiction.ZipCounty, Jurisdiction.ZipLocal, Jurisdiction.ZipTransit, Jurisdiction.CountyName, Jurisdiction.LocationName
FROM (Jurisdiction with (nolock)  INNER JOIN State WITH (NOLOCK) ON Jurisdiction.State = State.State)
LEFT JOIN RIA_Rates WITH (NOLOCK) ON (Jurisdiction.LocationName = RIA_Rates.LocationName)
AND (RIA_Rates.CountyJurName = Jurisdiction.CountyName) 
AND (RIA_Rates.Zip = case when Jurisdiction.ZipCounty is null then case when Jurisdiction.ZipLocal is null then Jurisdiction.ZipTransit else Jurisdiction.ZipLocal end else Jurisdiction.ZipCounty end) 
AND (Jurisdiction.PeriodID = RIA_Rates.PeriodID) 
and (State.State_ID_RIA = RIA_Rates.State_ID)
WHERE RIA_Rates.KeyCounter is null and Jurisdiction.ZipCounty <> '99999'"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_RIARates.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_NoZips()
        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "Select Jurisdiction.JurisdictionKey, Jurisdiction.GPJurisDescription, PeriodID, ZipCounty, ZipLocal, ZipTransit from Jurisdiction With (nolock) where ZipCounty Is null And ZipLocal Is null And ZipTransit Is null"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_NoZips.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_DiscreteState()
        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select Jurisdiction.JurisdictionKey, Jurisdiction.GPJurisDescription, PeriodID, Jurisdiction.DiscreteLocals, Jurisdiction.DiscreteState
from Jurisdiction with (nolock) 
where Jurisdiction.DiscreteLocals = 1 and Jurisdiction.DiscreteState = 0"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_DiscreteState.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_MultiZip()
        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select Jurisdiction.JurisdictionKey, Jurisdiction.GPJurisDescription, PeriodID, ZipCounty, ZipLocal, ZipTransit, RIAMultipleZips
from Jurisdiction with (nolock)  
where Jurisdiction.RIAMultipleZips = 0
and not((ZipLocal is null) and (ZipTransit is null)) 
and Jurisdiction.PeriodID = 99999"
        'Jurisdiction.PeriodID should not be in there, but keep getting error without it
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_MultiZip.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_Zip99999Flags()
        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select Jurisdiction.JurisdictionKey, Jurisdiction.GPJurisDescription, PeriodID, ZipCounty, RIANoState_Calculation, DiscreteState, DiscreteLocals
from Jurisdiction with (nolock) 
where ZipCounty = '99999' and (RIANoState_Calculation = 1 or  DiscreteState = 1 or  DiscreteLocals = 1)"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Zip99999Flags.DataSource = dtRecord
    End Sub

    Private Sub LoadDGV_Zip99999NotAllowed()
        Dim con As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "select Jurisdiction.JurisdictionKey, Jurisdiction.GPJurisDescription, PeriodID, ZipCounty, RIANoState_Calculation, DiscreteState, DiscreteLocals
from Jurisdiction with (nolock) 
where (ZipCounty = '99999' or DiscreteState = 1) and State in ('AZ','UT','NY','NC','OH','TX','CA','TX','GA','NM','TN')"
        If cboPeriod.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Zip99999NotAllowed.DataSource = dtRecord
    End Sub

    'Changes selected index of all DGVs
    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriod.SelectedIndexChanged
        cboPeriod.Text = cboPeriod.SelectedValue
        LoadDGV_NoZips()
        LoadDGV_RIARates()
        LoadDGV_DiscreteState()
        LoadDGV_MultiZip()
        LoadDGV_Zip99999Flags()
        LoadDGV_Zip99999NotAllowed()
    End Sub

End Class

﻿Imports System.Data.SqlClient

Public Class frmMaintainContacts
    Private Sub DivisionBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        'Me.DivisionBindingSource.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.SalesUse_UniDataSet2)

    End Sub

    Private Sub frmMaintainContacts_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadDGV_Contacts()
    End Sub

    Private Sub LoadDGV_Contacts()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM Contacts"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        DGV_Contacts.DataSource = dtRecord
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
﻿Public Class frmMaintainLocationCodeOveride
    Private Sub LocationCodeOverrideBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles LocationCodeOverrideBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.LocationCodeOverrideBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainLocationCodeOveride)

    End Sub

    Private Sub frmMaintainLocationCodeOveride_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainLocationCodeOveride.LocationCodeOverride' table. You can move, or remove it, as needed.
        Me.LocationCodeOverrideTableAdapter.Fill(Me.DataSource_MaintainLocationCodeOveride.LocationCodeOverride)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainRIABinderStateReturn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainRIABinderStateReturn))
        Me.DataSource_MaintainRIABinderStateReturn = New WindowsApp1.DataSource_MaintainRIABinderStateReturn()
        Me.RIABinderStateReturnBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RIABinderStateReturnTableAdapter = New WindowsApp1.DataSource_MaintainRIABinderStateReturnTableAdapters.RIABinderStateReturnTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainRIABinderStateReturnTableAdapters.TableAdapterManager()
        Me.RIABinderStateReturnBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.RIABinderStateReturnBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.RIABinderStateReturnDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnPrintByBinder = New System.Windows.Forms.Button()
        Me.btnPrintByState = New System.Windows.Forms.Button()
        Me.btnPrintByPreparer = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        CType(Me.DataSource_MaintainRIABinderStateReturn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RIABinderStateReturnBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RIABinderStateReturnBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RIABinderStateReturnBindingNavigator.SuspendLayout()
        CType(Me.RIABinderStateReturnDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataSource_MaintainRIABinderStateReturn
        '
        Me.DataSource_MaintainRIABinderStateReturn.DataSetName = "DataSource_MaintainRIABinderStateReturn"
        Me.DataSource_MaintainRIABinderStateReturn.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RIABinderStateReturnBindingSource
        '
        Me.RIABinderStateReturnBindingSource.DataMember = "RIABinderStateReturn"
        Me.RIABinderStateReturnBindingSource.DataSource = Me.DataSource_MaintainRIABinderStateReturn
        '
        'RIABinderStateReturnTableAdapter
        '
        Me.RIABinderStateReturnTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.RIABinderStateReturnTableAdapter = Me.RIABinderStateReturnTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainRIABinderStateReturnTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'RIABinderStateReturnBindingNavigator
        '
        Me.RIABinderStateReturnBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.RIABinderStateReturnBindingNavigator.BindingSource = Me.RIABinderStateReturnBindingSource
        Me.RIABinderStateReturnBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.RIABinderStateReturnBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.RIABinderStateReturnBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.RIABinderStateReturnBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.RIABinderStateReturnBindingNavigatorSaveItem})
        Me.RIABinderStateReturnBindingNavigator.Location = New System.Drawing.Point(0, 478)
        Me.RIABinderStateReturnBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.RIABinderStateReturnBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.RIABinderStateReturnBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.RIABinderStateReturnBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.RIABinderStateReturnBindingNavigator.Name = "RIABinderStateReturnBindingNavigator"
        Me.RIABinderStateReturnBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.RIABinderStateReturnBindingNavigator.Size = New System.Drawing.Size(903, 25)
        Me.RIABinderStateReturnBindingNavigator.TabIndex = 0
        Me.RIABinderStateReturnBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'RIABinderStateReturnBindingNavigatorSaveItem
        '
        Me.RIABinderStateReturnBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RIABinderStateReturnBindingNavigatorSaveItem.Image = CType(resources.GetObject("RIABinderStateReturnBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.RIABinderStateReturnBindingNavigatorSaveItem.Name = "RIABinderStateReturnBindingNavigatorSaveItem"
        Me.RIABinderStateReturnBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.RIABinderStateReturnBindingNavigatorSaveItem.Text = "Save Data"
        '
        'RIABinderStateReturnDataGridView
        '
        Me.RIABinderStateReturnDataGridView.AutoGenerateColumns = False
        Me.RIABinderStateReturnDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RIABinderStateReturnDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn17, Me.DataGridViewCheckBoxColumn2, Me.DataGridViewCheckBoxColumn3, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22})
        Me.RIABinderStateReturnDataGridView.DataSource = Me.RIABinderStateReturnBindingSource
        Me.RIABinderStateReturnDataGridView.Location = New System.Drawing.Point(12, 40)
        Me.RIABinderStateReturnDataGridView.Name = "RIABinderStateReturnDataGridView"
        Me.RIABinderStateReturnDataGridView.Size = New System.Drawing.Size(879, 463)
        Me.RIABinderStateReturnDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "RIABinder"
        Me.DataGridViewTextBoxColumn1.HeaderText = "RIABinder"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "State"
        Me.DataGridViewTextBoxColumn2.HeaderText = "State"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "ReturnName"
        Me.DataGridViewTextBoxColumn3.HeaderText = "ReturnName"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "State_ID"
        Me.DataGridViewTextBoxColumn4.HeaderText = "State_ID"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "State_Name"
        Me.DataGridViewTextBoxColumn5.HeaderText = "State_Name"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "DiscountAccount"
        Me.DataGridViewTextBoxColumn6.HeaderText = "DiscountAccount"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "ClearingAccount"
        Me.DataGridViewTextBoxColumn7.HeaderText = "ClearingAccount"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "System_ID"
        Me.DataGridViewTextBoxColumn8.HeaderText = "System_ID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "VendorNumber"
        Me.DataGridViewTextBoxColumn9.HeaderText = "VendorNumber"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "ACH_Template"
        Me.DataGridViewTextBoxColumn10.HeaderText = "ACH_Template"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "PreparerID"
        Me.DataGridViewTextBoxColumn11.HeaderText = "PreparerID"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "PreparerCycle"
        Me.DataGridViewTextBoxColumn12.HeaderText = "PreparerCycle"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Vouchering1"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Vouchering1"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Vouchering2"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Vouchering2"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "VoucherMessage"
        Me.DataGridViewTextBoxColumn15.HeaderText = "VoucherMessage"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "VoucherComment"
        Me.DataGridViewTextBoxColumn16.HeaderText = "VoucherComment"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "LocalReturn"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "LocalReturn"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "ShortName"
        Me.DataGridViewTextBoxColumn17.HeaderText = "ShortName"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "InActive"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "InActive"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        '
        'DataGridViewCheckBoxColumn3
        '
        Me.DataGridViewCheckBoxColumn3.DataPropertyName = "InComplianceSystem"
        Me.DataGridViewCheckBoxColumn3.HeaderText = "InComplianceSystem"
        Me.DataGridViewCheckBoxColumn3.Name = "DataGridViewCheckBoxColumn3"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "Notes"
        Me.DataGridViewTextBoxColumn18.HeaderText = "Notes"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "ReturnCycle"
        Me.DataGridViewTextBoxColumn19.HeaderText = "ReturnCycle"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "DueDay"
        Me.DataGridViewTextBoxColumn20.HeaderText = "DueDay"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "DueMonth"
        Me.DataGridViewTextBoxColumn21.HeaderText = "DueMonth"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "PeriodIDOffset"
        Me.DataGridViewTextBoxColumn22.HeaderText = "PeriodIDOffset"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'btnPrintByBinder
        '
        Me.btnPrintByBinder.Location = New System.Drawing.Point(12, 3)
        Me.btnPrintByBinder.Name = "btnPrintByBinder"
        Me.btnPrintByBinder.Size = New System.Drawing.Size(125, 31)
        Me.btnPrintByBinder.TabIndex = 2
        Me.btnPrintByBinder.Text = "Print by Binder"
        Me.btnPrintByBinder.UseVisualStyleBackColor = True
        '
        'btnPrintByState
        '
        Me.btnPrintByState.Location = New System.Drawing.Point(143, 3)
        Me.btnPrintByState.Name = "btnPrintByState"
        Me.btnPrintByState.Size = New System.Drawing.Size(125, 31)
        Me.btnPrintByState.TabIndex = 3
        Me.btnPrintByState.Text = "Print by State"
        Me.btnPrintByState.UseVisualStyleBackColor = True
        '
        'btnPrintByPreparer
        '
        Me.btnPrintByPreparer.Location = New System.Drawing.Point(274, 4)
        Me.btnPrintByPreparer.Name = "btnPrintByPreparer"
        Me.btnPrintByPreparer.Size = New System.Drawing.Size(125, 31)
        Me.btnPrintByPreparer.TabIndex = 4
        Me.btnPrintByPreparer.Text = "Print by Preparer"
        Me.btnPrintByPreparer.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(677, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(125, 31)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainRIABinderStateReturn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(903, 503)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPrintByPreparer)
        Me.Controls.Add(Me.btnPrintByState)
        Me.Controls.Add(Me.btnPrintByBinder)
        Me.Controls.Add(Me.RIABinderStateReturnDataGridView)
        Me.Controls.Add(Me.RIABinderStateReturnBindingNavigator)
        Me.Name = "frmMaintainRIABinderStateReturn"
        Me.Text = "Maintain RIA Binder State Return"
        CType(Me.DataSource_MaintainRIABinderStateReturn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RIABinderStateReturnBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RIABinderStateReturnBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RIABinderStateReturnBindingNavigator.ResumeLayout(False)
        Me.RIABinderStateReturnBindingNavigator.PerformLayout()
        CType(Me.RIABinderStateReturnDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataSource_MaintainRIABinderStateReturn As DataSource_MaintainRIABinderStateReturn
    Friend WithEvents RIABinderStateReturnBindingSource As BindingSource
    Friend WithEvents RIABinderStateReturnTableAdapter As DataSource_MaintainRIABinderStateReturnTableAdapters.RIABinderStateReturnTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_MaintainRIABinderStateReturnTableAdapters.TableAdapterManager
    Friend WithEvents RIABinderStateReturnBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents RIABinderStateReturnBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents RIABinderStateReturnDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents btnPrintByBinder As Button
    Friend WithEvents btnPrintByState As Button
    Friend WithEvents btnPrintByPreparer As Button
    Friend WithEvents btnClose As Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainRIABinders
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMaintainRIABinders))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabRIABinder = New System.Windows.Forms.TabPage()
        Me.RIABinderDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RIABinderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSource_MaintainRIABinders = New WindowsApp1.DataSource_MaintainRIABinders()
        Me.TabRIABinder_State_Returns = New System.Windows.Forms.TabPage()
        Me.RIABinderStateReturnDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RIABinderStateReturnBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RIABinderTableAdapter = New WindowsApp1.DataSource_MaintainRIABindersTableAdapters.RIABinderTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.DataSource_MaintainRIABindersTableAdapters.TableAdapterManager()
        Me.RIABinderStateReturnTableAdapter = New WindowsApp1.DataSource_MaintainRIABindersTableAdapters.RIABinderStateReturnTableAdapter()
        Me.RIABinderBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.RIABinderBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabRIABinder.SuspendLayout()
        CType(Me.RIABinderDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RIABinderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSource_MaintainRIABinders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabRIABinder_State_Returns.SuspendLayout()
        CType(Me.RIABinderStateReturnDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RIABinderStateReturnBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RIABinderBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RIABinderBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabRIABinder)
        Me.TabControl1.Controls.Add(Me.TabRIABinder_State_Returns)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(868, 454)
        Me.TabControl1.TabIndex = 0
        '
        'TabRIABinder
        '
        Me.TabRIABinder.Controls.Add(Me.RIABinderDataGridView)
        Me.TabRIABinder.Location = New System.Drawing.Point(4, 22)
        Me.TabRIABinder.Name = "TabRIABinder"
        Me.TabRIABinder.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRIABinder.Size = New System.Drawing.Size(860, 428)
        Me.TabRIABinder.TabIndex = 0
        Me.TabRIABinder.Text = "RIABinder"
        Me.TabRIABinder.UseVisualStyleBackColor = True
        '
        'RIABinderDataGridView
        '
        Me.RIABinderDataGridView.AutoGenerateColumns = False
        Me.RIABinderDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RIABinderDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.RIABinderDataGridView.DataSource = Me.RIABinderBindingSource
        Me.RIABinderDataGridView.Location = New System.Drawing.Point(6, 6)
        Me.RIABinderDataGridView.Name = "RIABinderDataGridView"
        Me.RIABinderDataGridView.Size = New System.Drawing.Size(848, 409)
        Me.RIABinderDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "RIABinder"
        Me.DataGridViewTextBoxColumn1.HeaderText = "RIABinder"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "BinderCycle"
        Me.DataGridViewTextBoxColumn2.HeaderText = "BinderCycle"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "OutletTable"
        Me.DataGridViewTextBoxColumn3.HeaderText = "OutletTable"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "NullTable"
        Me.DataGridViewTextBoxColumn4.HeaderText = "NullTable"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'RIABinderBindingSource
        '
        Me.RIABinderBindingSource.DataMember = "RIABinder"
        Me.RIABinderBindingSource.DataSource = Me.DataSource_MaintainRIABinders
        '
        'DataSource_MaintainRIABinders
        '
        Me.DataSource_MaintainRIABinders.DataSetName = "DataSource_MaintainRIABinders"
        Me.DataSource_MaintainRIABinders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabRIABinder_State_Returns
        '
        Me.TabRIABinder_State_Returns.Controls.Add(Me.RIABinderStateReturnDataGridView)
        Me.TabRIABinder_State_Returns.Location = New System.Drawing.Point(4, 22)
        Me.TabRIABinder_State_Returns.Name = "TabRIABinder_State_Returns"
        Me.TabRIABinder_State_Returns.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRIABinder_State_Returns.Size = New System.Drawing.Size(860, 449)
        Me.TabRIABinder_State_Returns.TabIndex = 1
        Me.TabRIABinder_State_Returns.Text = "RIABinder/State/Returns"
        Me.TabRIABinder_State_Returns.UseVisualStyleBackColor = True
        '
        'RIABinderStateReturnDataGridView
        '
        Me.RIABinderStateReturnDataGridView.AutoGenerateColumns = False
        Me.RIABinderStateReturnDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RIABinderStateReturnDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn21, Me.DataGridViewCheckBoxColumn2, Me.DataGridViewCheckBoxColumn3, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26})
        Me.RIABinderStateReturnDataGridView.DataSource = Me.RIABinderStateReturnBindingSource
        Me.RIABinderStateReturnDataGridView.Location = New System.Drawing.Point(6, 6)
        Me.RIABinderStateReturnDataGridView.Name = "RIABinderStateReturnDataGridView"
        Me.RIABinderStateReturnDataGridView.Size = New System.Drawing.Size(848, 437)
        Me.RIABinderStateReturnDataGridView.TabIndex = 0
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "RIABinder"
        Me.DataGridViewTextBoxColumn5.HeaderText = "RIABinder"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "State"
        Me.DataGridViewTextBoxColumn6.HeaderText = "State"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "ReturnName"
        Me.DataGridViewTextBoxColumn7.HeaderText = "ReturnName"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "State_ID"
        Me.DataGridViewTextBoxColumn8.HeaderText = "State_ID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "State_Name"
        Me.DataGridViewTextBoxColumn9.HeaderText = "State_Name"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "DiscountAccount"
        Me.DataGridViewTextBoxColumn10.HeaderText = "DiscountAccount"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "ClearingAccount"
        Me.DataGridViewTextBoxColumn11.HeaderText = "ClearingAccount"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "System_ID"
        Me.DataGridViewTextBoxColumn12.HeaderText = "System_ID"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "VendorNumber"
        Me.DataGridViewTextBoxColumn13.HeaderText = "VendorNumber"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "ACH_Template"
        Me.DataGridViewTextBoxColumn14.HeaderText = "ACH_Template"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "PreparerID"
        Me.DataGridViewTextBoxColumn15.HeaderText = "PreparerID"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "PreparerCycle"
        Me.DataGridViewTextBoxColumn16.HeaderText = "PreparerCycle"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Vouchering1"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Vouchering1"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "Vouchering2"
        Me.DataGridViewTextBoxColumn18.HeaderText = "Vouchering2"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "VoucherMessage"
        Me.DataGridViewTextBoxColumn19.HeaderText = "VoucherMessage"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "VoucherComment"
        Me.DataGridViewTextBoxColumn20.HeaderText = "VoucherComment"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "LocalReturn"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "LocalReturn"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "ShortName"
        Me.DataGridViewTextBoxColumn21.HeaderText = "ShortName"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "InActive"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "InActive"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        '
        'DataGridViewCheckBoxColumn3
        '
        Me.DataGridViewCheckBoxColumn3.DataPropertyName = "InComplianceSystem"
        Me.DataGridViewCheckBoxColumn3.HeaderText = "InComplianceSystem"
        Me.DataGridViewCheckBoxColumn3.Name = "DataGridViewCheckBoxColumn3"
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "Notes"
        Me.DataGridViewTextBoxColumn22.HeaderText = "Notes"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "ReturnCycle"
        Me.DataGridViewTextBoxColumn23.HeaderText = "ReturnCycle"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "DueDay"
        Me.DataGridViewTextBoxColumn24.HeaderText = "DueDay"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "DueMonth"
        Me.DataGridViewTextBoxColumn25.HeaderText = "DueMonth"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "PeriodIDOffset"
        Me.DataGridViewTextBoxColumn26.HeaderText = "PeriodIDOffset"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'RIABinderStateReturnBindingSource
        '
        Me.RIABinderStateReturnBindingSource.DataMember = "RIABinderStateReturn"
        Me.RIABinderStateReturnBindingSource.DataSource = Me.DataSource_MaintainRIABinders
        '
        'RIABinderTableAdapter
        '
        Me.RIABinderTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.RIABinderStateReturnTableAdapter = Me.RIABinderStateReturnTableAdapter
        Me.TableAdapterManager.RIABinderTableAdapter = Me.RIABinderTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_MaintainRIABindersTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'RIABinderStateReturnTableAdapter
        '
        Me.RIABinderStateReturnTableAdapter.ClearBeforeFill = True
        '
        'RIABinderBindingNavigator
        '
        Me.RIABinderBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.RIABinderBindingNavigator.BindingSource = Me.RIABinderBindingSource
        Me.RIABinderBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.RIABinderBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.RIABinderBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.RIABinderBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.RIABinderBindingNavigatorSaveItem})
        Me.RIABinderBindingNavigator.Location = New System.Drawing.Point(0, 487)
        Me.RIABinderBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.RIABinderBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.RIABinderBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.RIABinderBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.RIABinderBindingNavigator.Name = "RIABinderBindingNavigator"
        Me.RIABinderBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.RIABinderBindingNavigator.Size = New System.Drawing.Size(892, 25)
        Me.RIABinderBindingNavigator.TabIndex = 1
        Me.RIABinderBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'RIABinderBindingNavigatorSaveItem
        '
        Me.RIABinderBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.RIABinderBindingNavigatorSaveItem.Image = CType(resources.GetObject("RIABinderBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.RIABinderBindingNavigatorSaveItem.Name = "RIABinderBindingNavigatorSaveItem"
        Me.RIABinderBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.RIABinderBindingNavigatorSaveItem.Text = "Save Data"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(801, 477)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainRIABinders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(892, 512)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.RIABinderBindingNavigator)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainRIABinders"
        Me.Text = "Maintain RIA Binders"
        Me.TabControl1.ResumeLayout(False)
        Me.TabRIABinder.ResumeLayout(False)
        CType(Me.RIABinderDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RIABinderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSource_MaintainRIABinders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabRIABinder_State_Returns.ResumeLayout(False)
        CType(Me.RIABinderStateReturnDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RIABinderStateReturnBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RIABinderBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RIABinderBindingNavigator.ResumeLayout(False)
        Me.RIABinderBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabRIABinder As TabPage
    Friend WithEvents TabRIABinder_State_Returns As TabPage
    Friend WithEvents DataSource_MaintainRIABinders As DataSource_MaintainRIABinders
    Friend WithEvents RIABinderBindingSource As BindingSource
    Friend WithEvents RIABinderTableAdapter As DataSource_MaintainRIABindersTableAdapters.RIABinderTableAdapter
    Friend WithEvents TableAdapterManager As DataSource_MaintainRIABindersTableAdapters.TableAdapterManager
    Friend WithEvents RIABinderBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents RIABinderBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents RIABinderDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents RIABinderStateReturnTableAdapter As DataSource_MaintainRIABindersTableAdapters.RIABinderStateReturnTableAdapter
    Friend WithEvents RIABinderStateReturnBindingSource As BindingSource
    Friend WithEvents RIABinderStateReturnDataGridView As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As DataGridViewTextBoxColumn
    Friend WithEvents btnClose As Button
End Class

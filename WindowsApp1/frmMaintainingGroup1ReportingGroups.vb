﻿Imports System.Data.SqlClient

Public Class frmMaintainingGroup1ReportingGroups
    Private Sub Group1ReportingGroupsBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.Group1ReportingGroupsBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_maintainingGroup1ReportingGroups)
    End Sub

    Private Sub frmMaintainingGroup1ReportingGroups_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_maintainingGroup1ReportingGroups.Group1ReportingGroups' table. You can move, or remove it, as needed.
        Me.Group1ReportingGroupsTableAdapter.Fill(Me.DataSource_maintainingGroup1ReportingGroups.Group1ReportingGroups)
        Load_cboGroupName()
        Load_dgvGroup1s()
    End Sub

    Private Sub Load_cboGroupName()
        Dim sqlquery As String
        sqlquery = "SELECT DISTINCT Group1ReportingGroups.GroupName FROM Group1ReportingGroups"
        Using connection As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(sqlquery, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                ' as an example set the ValueMember and DisplayMember'
                ' to two columns of the returned table'
                cboGroupName.ValueMember = "GroupName"
                cboGroupName.DisplayMember = "GroupName"
                cboGroupName.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub Load_dgvGroup1s()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT DISTINCT Division.Division, Division.DivName FROM Division ORDER BY Division.Division"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvGroup1s.DataSource = dtRecord
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
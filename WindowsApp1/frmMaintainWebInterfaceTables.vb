﻿Public Class frmMaintainWebInterfaceTables
    Private Sub FacilityBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles FacilityBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.FacilityBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainWebInterfaceTables)

    End Sub

    Private Sub frmMaintainWebInterfaceTables_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainWebInterfaceTables.WebUser' table. You can move, or remove it, as needed.
        Me.WebUserTableAdapter.Fill(Me.DataSource_MaintainWebInterfaceTables.WebUser)
        'TODO: This line of code loads data into the 'DataSource_MaintainWebInterfaceTables.JNumber' table. You can move, or remove it, as needed.
        Me.JNumberTableAdapter.Fill(Me.DataSource_MaintainWebInterfaceTables.JNumber)
        'TODO: This line of code loads data into the 'DataSource_MaintainWebInterfaceTables.Facility' table. You can move, or remove it, as needed.
        Me.FacilityTableAdapter.Fill(Me.DataSource_MaintainWebInterfaceTables.Facility)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
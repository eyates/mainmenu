﻿Imports System.Data.SqlClient

Public Class frmMaintainReferenceTables
    Private Sub SystemCodesBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainReferenceTables)
    End Sub

    Private Sub frmMaintainReferenceTables_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Load_dgvAdjustType()
        Load_cboErrorGroup()
        Load_dgvErrorGroup()
        Load_dgvErrorStatus()
        Load_dgvSUImportSource()
        Load_dgvB3I4AccountsToReview()
        Load_dgvVendorsToExclude()
        Load_dgvTaxVendors()
        Load_dgvTransactionSource()
        Load_dgvHandlingCode()
        Load_dgvSystemCodes()
    End Sub

    Private Sub Load_dgvAdjustType()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM AdjustmentTypes"
        'If cboPeriod.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " AND Jurisdiction.PeriodID = '" & Trim(cboPeriod.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvAdjustType.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvErrorGroup()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM ErrorCodes"
        If cboErrorGroup.Text <> "" Then
            sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvErrorCodes.DataSource = dtRecord
    End Sub

    Private Sub Load_cboErrorGroup()
        Dim sqlquery As String
        sqlquery = "SELECT DISTINCT ErrorGroupName FROM ErrorCodes ORDER BY ErrorGroupName"
        Using connection As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(sqlquery, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                ' as an example set the ValueMember and DisplayMember'
                ' to two columns of the returned table'
                cboErrorGroup.ValueMember = "ErrorGroupName"
                cboErrorGroup.DisplayMember = "ErrorGroupName"
                cboErrorGroup.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub Load_dgvErrorStatus()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM ErrorStatus"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvErrorStatus.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvSUImportSource()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM SUImportSource"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvSUImportSource.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvB3I4AccountsToReview()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM B3I4AccountsToReview"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvB3I4AccountsToReview.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvVendorsToExclude()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM VendorsToExclude"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvVendorsToExclude.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvTaxVendors()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM TaxVendors"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvTaxVendors.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvTransactionSource()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM TransactionSource"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvTransactionSource.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvHandlingCode()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM HandlingCode"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvHandlingCode.DataSource = dtRecord
    End Sub

    Private Sub Load_dgvSystemCodes()
        Dim con As SqlConnection = New SqlConnection("Data Source=DESKTOP-43KCLG5;Initial Catalog=SalesUse_Uni;Integrated Security=True")
        Dim sqlCmd As SqlCommand = New SqlCommand()
        sqlCmd.Connection = con
        sqlCmd.CommandType = CommandType.Text
        sqlCmd.CommandText = "SELECT * FROM SystemCodes"
        'If cboErrorGroup.Text <> "" Then
        'sqlCmd.CommandText = sqlCmd.CommandText & " WHERE ErrorGroupName = '" & Trim(cboErrorGroup.Text) & "'"
        'End If
        Dim sqlDataAdap As SqlDataAdapter = New SqlDataAdapter(sqlCmd)
        Dim dtRecord As DataTable = New DataTable
        'DataTable dtRecord = New DataTable();
        sqlDataAdap.Fill(dtRecord)
        dgvSystemCodes.DataSource = dtRecord
    End Sub

    Private Sub cboErrorGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboErrorGroup.SelectedIndexChanged
        cboErrorGroup.Text = cboErrorGroup.SelectedValue
        Load_dgvErrorGroup()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
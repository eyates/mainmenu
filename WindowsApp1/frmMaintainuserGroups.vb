﻿Public Class frmMaintainuserGroups
    Private Sub UserRightsBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles UserRightsBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.UserRightsBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DataSource_MaintainUserGroups)

    End Sub

    Private Sub frmMaintainuserGroups_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSource_MaintainUserGroups.UserGroup' table. You can move, or remove it, as needed.
        Me.UserGroupTableAdapter.Fill(Me.DataSource_MaintainUserGroups.UserGroup)
        'TODO: This line of code loads data into the 'DataSource_MaintainUserGroups.UserGroupRights' table. You can move, or remove it, as needed.
        Me.UserGroupRightsTableAdapter.Fill(Me.DataSource_MaintainUserGroups.UserGroupRights)
        'TODO: This line of code loads data into the 'DataSource_MaintainUserGroups.UserRights' table. You can move, or remove it, as needed.
        Me.UserRightsTableAdapter.Fill(Me.DataSource_MaintainUserGroups.UserRights)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMaintainStates
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim StateLabel As System.Windows.Forms.Label
        Dim State_ID_RIALabel As System.Windows.Forms.Label
        Dim State_NameLabel As System.Windows.Forms.Label
        Dim Group4Label As System.Windows.Forms.Label
        Dim TransactionLimitAmountLabel As System.Windows.Forms.Label
        Dim TaxMgrLabel As System.Windows.Forms.Label
        Dim OutStateAsUseLabel As System.Windows.Forms.Label
        Dim OutStateLocationLabel As System.Windows.Forms.Label
        Dim DefaultZipLabel As System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.StateTextBox = New System.Windows.Forms.TextBox()
        Me.StateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SalesUse_UniDataSet1 = New WindowsApp1.SalesUse_UniDataSet1()
        Me.State_ID_RIATextBox = New System.Windows.Forms.TextBox()
        Me.State_NameTextBox = New System.Windows.Forms.TextBox()
        Me.Group4TextBox = New System.Windows.Forms.TextBox()
        Me.TransactionLimitAmountTextBox = New System.Windows.Forms.TextBox()
        Me.TaxMgrTextBox = New System.Windows.Forms.TextBox()
        Me.OutStateAsUseCheckBox = New System.Windows.Forms.CheckBox()
        Me.OutStateLocationCheckBox = New System.Windows.Forms.CheckBox()
        Me.DefaultZipTextBox = New System.Windows.Forms.TextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DGV_FilingEntityState = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.FilingEntityStateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.StateTableAdapter = New WindowsApp1.SalesUse_UniDataSet1TableAdapters.StateTableAdapter()
        Me.TableAdapterManager = New WindowsApp1.SalesUse_UniDataSet1TableAdapters.TableAdapterManager()
        Me.FilingEntityStateTableAdapter = New WindowsApp1.SalesUse_UniDataSet1TableAdapters.FilingEntityStateTableAdapter()
        Me.cboState = New System.Windows.Forms.ComboBox()
        Me.DGV_HandlingCodes = New System.Windows.Forms.DataGridView()
        Me.btnClose = New System.Windows.Forms.Button()
        StateLabel = New System.Windows.Forms.Label()
        State_ID_RIALabel = New System.Windows.Forms.Label()
        State_NameLabel = New System.Windows.Forms.Label()
        Group4Label = New System.Windows.Forms.Label()
        TransactionLimitAmountLabel = New System.Windows.Forms.Label()
        TaxMgrLabel = New System.Windows.Forms.Label()
        OutStateAsUseLabel = New System.Windows.Forms.Label()
        OutStateLocationLabel = New System.Windows.Forms.Label()
        DefaultZipLabel = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.StateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SalesUse_UniDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.DGV_FilingEntityState, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FilingEntityStateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.DGV_HandlingCodes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StateLabel
        '
        StateLabel.AutoSize = True
        StateLabel.Location = New System.Drawing.Point(25, 17)
        StateLabel.Name = "StateLabel"
        StateLabel.Size = New System.Drawing.Size(35, 13)
        StateLabel.TabIndex = 0
        StateLabel.Text = "State:"
        '
        'State_ID_RIALabel
        '
        State_ID_RIALabel.AutoSize = True
        State_ID_RIALabel.Location = New System.Drawing.Point(25, 43)
        State_ID_RIALabel.Name = "State_ID_RIALabel"
        State_ID_RIALabel.Size = New System.Drawing.Size(70, 13)
        State_ID_RIALabel.TabIndex = 2
        State_ID_RIALabel.Text = "State ID RIA:"
        '
        'State_NameLabel
        '
        State_NameLabel.AutoSize = True
        State_NameLabel.Location = New System.Drawing.Point(25, 69)
        State_NameLabel.Name = "State_NameLabel"
        State_NameLabel.Size = New System.Drawing.Size(66, 13)
        State_NameLabel.TabIndex = 4
        State_NameLabel.Text = "State Name:"
        '
        'Group4Label
        '
        Group4Label.AutoSize = True
        Group4Label.Location = New System.Drawing.Point(25, 95)
        Group4Label.Name = "Group4Label"
        Group4Label.Size = New System.Drawing.Size(45, 13)
        Group4Label.TabIndex = 6
        Group4Label.Text = "Group4:"
        '
        'TransactionLimitAmountLabel
        '
        TransactionLimitAmountLabel.AutoSize = True
        TransactionLimitAmountLabel.Location = New System.Drawing.Point(25, 121)
        TransactionLimitAmountLabel.Name = "TransactionLimitAmountLabel"
        TransactionLimitAmountLabel.Size = New System.Drawing.Size(129, 13)
        TransactionLimitAmountLabel.TabIndex = 8
        TransactionLimitAmountLabel.Text = "Transaction Limit Amount:"
        '
        'TaxMgrLabel
        '
        TaxMgrLabel.AutoSize = True
        TaxMgrLabel.Location = New System.Drawing.Point(25, 147)
        TaxMgrLabel.Name = "TaxMgrLabel"
        TaxMgrLabel.Size = New System.Drawing.Size(49, 13)
        TaxMgrLabel.TabIndex = 10
        TaxMgrLabel.Text = "Tax Mgr:"
        '
        'OutStateAsUseLabel
        '
        OutStateAsUseLabel.AutoSize = True
        OutStateAsUseLabel.Location = New System.Drawing.Point(25, 175)
        OutStateAsUseLabel.Name = "OutStateAsUseLabel"
        OutStateAsUseLabel.Size = New System.Drawing.Size(92, 13)
        OutStateAsUseLabel.TabIndex = 12
        OutStateAsUseLabel.Text = "Out State As Use:"
        '
        'OutStateLocationLabel
        '
        OutStateLocationLabel.AutoSize = True
        OutStateLocationLabel.Location = New System.Drawing.Point(25, 205)
        OutStateLocationLabel.Name = "OutStateLocationLabel"
        OutStateLocationLabel.Size = New System.Drawing.Size(99, 13)
        OutStateLocationLabel.TabIndex = 14
        OutStateLocationLabel.Text = "Out State Location:"
        '
        'DefaultZipLabel
        '
        DefaultZipLabel.AutoSize = True
        DefaultZipLabel.Location = New System.Drawing.Point(25, 233)
        DefaultZipLabel.Name = "DefaultZipLabel"
        DefaultZipLabel.Size = New System.Drawing.Size(62, 13)
        DefaultZipLabel.TabIndex = 16
        DefaultZipLabel.Text = "Default Zip:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 28)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(779, 314)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.AutoScroll = True
        Me.TabPage1.Controls.Add(StateLabel)
        Me.TabPage1.Controls.Add(Me.StateTextBox)
        Me.TabPage1.Controls.Add(State_ID_RIALabel)
        Me.TabPage1.Controls.Add(Me.State_ID_RIATextBox)
        Me.TabPage1.Controls.Add(State_NameLabel)
        Me.TabPage1.Controls.Add(Me.State_NameTextBox)
        Me.TabPage1.Controls.Add(Group4Label)
        Me.TabPage1.Controls.Add(Me.Group4TextBox)
        Me.TabPage1.Controls.Add(TransactionLimitAmountLabel)
        Me.TabPage1.Controls.Add(Me.TransactionLimitAmountTextBox)
        Me.TabPage1.Controls.Add(TaxMgrLabel)
        Me.TabPage1.Controls.Add(Me.TaxMgrTextBox)
        Me.TabPage1.Controls.Add(OutStateAsUseLabel)
        Me.TabPage1.Controls.Add(Me.OutStateAsUseCheckBox)
        Me.TabPage1.Controls.Add(OutStateLocationLabel)
        Me.TabPage1.Controls.Add(Me.OutStateLocationCheckBox)
        Me.TabPage1.Controls.Add(DefaultZipLabel)
        Me.TabPage1.Controls.Add(Me.DefaultZipTextBox)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(771, 288)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'StateTextBox
        '
        Me.StateTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "State", True))
        Me.StateTextBox.Location = New System.Drawing.Point(160, 14)
        Me.StateTextBox.Name = "StateTextBox"
        Me.StateTextBox.Size = New System.Drawing.Size(104, 20)
        Me.StateTextBox.TabIndex = 1
        '
        'StateBindingSource
        '
        Me.StateBindingSource.DataMember = "State"
        Me.StateBindingSource.DataSource = Me.SalesUse_UniDataSet1
        '
        'SalesUse_UniDataSet1
        '
        Me.SalesUse_UniDataSet1.DataSetName = "SalesUse_UniDataSet1"
        Me.SalesUse_UniDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'State_ID_RIATextBox
        '
        Me.State_ID_RIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "State_ID_RIA", True))
        Me.State_ID_RIATextBox.Location = New System.Drawing.Point(160, 40)
        Me.State_ID_RIATextBox.Name = "State_ID_RIATextBox"
        Me.State_ID_RIATextBox.Size = New System.Drawing.Size(104, 20)
        Me.State_ID_RIATextBox.TabIndex = 3
        '
        'State_NameTextBox
        '
        Me.State_NameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "State_Name", True))
        Me.State_NameTextBox.Location = New System.Drawing.Point(160, 66)
        Me.State_NameTextBox.Name = "State_NameTextBox"
        Me.State_NameTextBox.Size = New System.Drawing.Size(104, 20)
        Me.State_NameTextBox.TabIndex = 5
        '
        'Group4TextBox
        '
        Me.Group4TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "Group4", True))
        Me.Group4TextBox.Location = New System.Drawing.Point(160, 92)
        Me.Group4TextBox.Name = "Group4TextBox"
        Me.Group4TextBox.Size = New System.Drawing.Size(104, 20)
        Me.Group4TextBox.TabIndex = 7
        '
        'TransactionLimitAmountTextBox
        '
        Me.TransactionLimitAmountTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "TransactionLimitAmount", True))
        Me.TransactionLimitAmountTextBox.Location = New System.Drawing.Point(160, 118)
        Me.TransactionLimitAmountTextBox.Name = "TransactionLimitAmountTextBox"
        Me.TransactionLimitAmountTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TransactionLimitAmountTextBox.TabIndex = 9
        '
        'TaxMgrTextBox
        '
        Me.TaxMgrTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "TaxMgr", True))
        Me.TaxMgrTextBox.Location = New System.Drawing.Point(160, 144)
        Me.TaxMgrTextBox.Name = "TaxMgrTextBox"
        Me.TaxMgrTextBox.Size = New System.Drawing.Size(104, 20)
        Me.TaxMgrTextBox.TabIndex = 11
        '
        'OutStateAsUseCheckBox
        '
        Me.OutStateAsUseCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.StateBindingSource, "OutStateAsUse", True))
        Me.OutStateAsUseCheckBox.Location = New System.Drawing.Point(160, 170)
        Me.OutStateAsUseCheckBox.Name = "OutStateAsUseCheckBox"
        Me.OutStateAsUseCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.OutStateAsUseCheckBox.TabIndex = 13
        Me.OutStateAsUseCheckBox.Text = "CheckBox1"
        Me.OutStateAsUseCheckBox.UseVisualStyleBackColor = True
        '
        'OutStateLocationCheckBox
        '
        Me.OutStateLocationCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.StateBindingSource, "OutStateLocation", True))
        Me.OutStateLocationCheckBox.Location = New System.Drawing.Point(160, 200)
        Me.OutStateLocationCheckBox.Name = "OutStateLocationCheckBox"
        Me.OutStateLocationCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.OutStateLocationCheckBox.TabIndex = 15
        Me.OutStateLocationCheckBox.Text = "CheckBox1"
        Me.OutStateLocationCheckBox.UseVisualStyleBackColor = True
        '
        'DefaultZipTextBox
        '
        Me.DefaultZipTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.StateBindingSource, "DefaultZip", True))
        Me.DefaultZipTextBox.Location = New System.Drawing.Point(160, 230)
        Me.DefaultZipTextBox.Name = "DefaultZipTextBox"
        Me.DefaultZipTextBox.Size = New System.Drawing.Size(104, 20)
        Me.DefaultZipTextBox.TabIndex = 17
        '
        'TabPage2
        '
        Me.TabPage2.AutoScroll = True
        Me.TabPage2.Controls.Add(Me.DGV_FilingEntityState)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(771, 288)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'DGV_FilingEntityState
        '
        Me.DGV_FilingEntityState.AutoGenerateColumns = False
        Me.DGV_FilingEntityState.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_FilingEntityState.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewCheckBoxColumn1})
        Me.DGV_FilingEntityState.DataSource = Me.FilingEntityStateBindingSource
        Me.DGV_FilingEntityState.Location = New System.Drawing.Point(0, 0)
        Me.DGV_FilingEntityState.Name = "DGV_FilingEntityState"
        Me.DGV_FilingEntityState.Size = New System.Drawing.Size(744, 279)
        Me.DGV_FilingEntityState.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "FilingEntityKey"
        Me.DataGridViewTextBoxColumn1.HeaderText = "FilingEntityKey"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "State"
        Me.DataGridViewTextBoxColumn2.HeaderText = "State"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "PeriodID"
        Me.DataGridViewTextBoxColumn3.HeaderText = "PeriodID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "DefaultforStateFlag"
        Me.DataGridViewCheckBoxColumn1.HeaderText = "DefaultforStateFlag"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        '
        'FilingEntityStateBindingSource
        '
        Me.FilingEntityStateBindingSource.DataMember = "FilingEntityState"
        Me.FilingEntityStateBindingSource.DataSource = Me.SalesUse_UniDataSet1
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DGV_HandlingCodes)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(771, 288)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'StateTableAdapter
        '
        Me.StateTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.FilingEntityStateTableAdapter = Nothing
        Me.TableAdapterManager.FilingEntityTableAdapter = Nothing
        Me.TableAdapterManager.StateTableAdapter = Me.StateTableAdapter
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.SalesUse_UniDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'FilingEntityStateTableAdapter
        '
        Me.FilingEntityStateTableAdapter.ClearBeforeFill = True
        '
        'cboState
        '
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(19, 1)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(121, 21)
        Me.cboState.TabIndex = 18
        '
        'DGV_HandlingCodes
        '
        Me.DGV_HandlingCodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_HandlingCodes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_HandlingCodes.Location = New System.Drawing.Point(3, 3)
        Me.DGV_HandlingCodes.Name = "DGV_HandlingCodes"
        Me.DGV_HandlingCodes.Size = New System.Drawing.Size(765, 282)
        Me.DGV_HandlingCodes.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(709, 348)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 19
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMaintainStates
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(808, 377)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.cboState)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainStates"
        Me.Text = "FormMaintainStates"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.StateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SalesUse_UniDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DGV_FilingEntityState, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FilingEntityStateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DGV_HandlingCodes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents SalesUse_UniDataSet1 As SalesUse_UniDataSet1
    Friend WithEvents StateBindingSource As BindingSource
    Friend WithEvents StateTableAdapter As SalesUse_UniDataSet1TableAdapters.StateTableAdapter
    Friend WithEvents TableAdapterManager As SalesUse_UniDataSet1TableAdapters.TableAdapterManager
    Friend WithEvents StateTextBox As TextBox
    Friend WithEvents State_ID_RIATextBox As TextBox
    Friend WithEvents State_NameTextBox As TextBox
    Friend WithEvents Group4TextBox As TextBox
    Friend WithEvents TransactionLimitAmountTextBox As TextBox
    Friend WithEvents TaxMgrTextBox As TextBox
    Friend WithEvents OutStateAsUseCheckBox As CheckBox
    Friend WithEvents OutStateLocationCheckBox As CheckBox
    Friend WithEvents DefaultZipTextBox As TextBox
    Friend WithEvents FilingEntityStateBindingSource As BindingSource
    Friend WithEvents FilingEntityStateTableAdapter As SalesUse_UniDataSet1TableAdapters.FilingEntityStateTableAdapter
    Friend WithEvents DGV_FilingEntityState As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As DataGridViewCheckBoxColumn
    Friend WithEvents cboState As ComboBox
    Friend WithEvents DGV_HandlingCodes As DataGridView
    Friend WithEvents btnClose As Button
End Class

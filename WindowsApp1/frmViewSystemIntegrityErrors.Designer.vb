﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewSystemIntegrityErrors
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabFilingEntity_State = New System.Windows.Forms.TabPage()
        Me.DataSource_ViewSystemIntegirtyErrors = New WindowsApp1.DataSource_ViewSystemIntegirtyErrors()
        Me.TabFilingEntity_State_Group1 = New System.Windows.Forms.TabPage()
        Me.TabState_Binder = New System.Windows.Forms.TabPage()
        Me.TabGLDefault = New System.Windows.Forms.TabPage()
        Me.TableAdapterManager = New WindowsApp1.DataSource_ViewSystemIntegirtyErrorsTableAdapters.TableAdapterManager()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.dgvFilingEntityState = New System.Windows.Forms.DataGridView()
        Me.dgvFilingEntityStateGroup1 = New System.Windows.Forms.DataGridView()
        Me.dgvStateBinder = New System.Windows.Forms.DataGridView()
        Me.dgvGLDefault = New System.Windows.Forms.DataGridView()
        Me.TabControl1.SuspendLayout()
        Me.TabFilingEntity_State.SuspendLayout()
        CType(Me.DataSource_ViewSystemIntegirtyErrors, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabFilingEntity_State_Group1.SuspendLayout()
        Me.TabState_Binder.SuspendLayout()
        Me.TabGLDefault.SuspendLayout()
        CType(Me.dgvFilingEntityState, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFilingEntityStateGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStateBinder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGLDefault, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabFilingEntity_State)
        Me.TabControl1.Controls.Add(Me.TabFilingEntity_State_Group1)
        Me.TabControl1.Controls.Add(Me.TabState_Binder)
        Me.TabControl1.Controls.Add(Me.TabGLDefault)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(868, 401)
        Me.TabControl1.TabIndex = 0
        '
        'TabFilingEntity_State
        '
        Me.TabFilingEntity_State.Controls.Add(Me.dgvFilingEntityState)
        Me.TabFilingEntity_State.Location = New System.Drawing.Point(4, 22)
        Me.TabFilingEntity_State.Name = "TabFilingEntity_State"
        Me.TabFilingEntity_State.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFilingEntity_State.Size = New System.Drawing.Size(860, 375)
        Me.TabFilingEntity_State.TabIndex = 0
        Me.TabFilingEntity_State.Text = "Filing Entity/State"
        Me.TabFilingEntity_State.UseVisualStyleBackColor = True
        '
        'DataSource_ViewSystemIntegirtyErrors
        '
        Me.DataSource_ViewSystemIntegirtyErrors.DataSetName = "DataSource_ViewSystemIntegirtyErrors"
        Me.DataSource_ViewSystemIntegirtyErrors.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabFilingEntity_State_Group1
        '
        Me.TabFilingEntity_State_Group1.Controls.Add(Me.dgvFilingEntityStateGroup1)
        Me.TabFilingEntity_State_Group1.Location = New System.Drawing.Point(4, 22)
        Me.TabFilingEntity_State_Group1.Name = "TabFilingEntity_State_Group1"
        Me.TabFilingEntity_State_Group1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFilingEntity_State_Group1.Size = New System.Drawing.Size(860, 375)
        Me.TabFilingEntity_State_Group1.TabIndex = 1
        Me.TabFilingEntity_State_Group1.Text = "Filing Entity/State/Group1"
        Me.TabFilingEntity_State_Group1.UseVisualStyleBackColor = True
        '
        'TabState_Binder
        '
        Me.TabState_Binder.Controls.Add(Me.dgvStateBinder)
        Me.TabState_Binder.Location = New System.Drawing.Point(4, 22)
        Me.TabState_Binder.Name = "TabState_Binder"
        Me.TabState_Binder.Padding = New System.Windows.Forms.Padding(3)
        Me.TabState_Binder.Size = New System.Drawing.Size(860, 375)
        Me.TabState_Binder.TabIndex = 2
        Me.TabState_Binder.Text = "State/Binder"
        Me.TabState_Binder.UseVisualStyleBackColor = True
        '
        'TabGLDefault
        '
        Me.TabGLDefault.Controls.Add(Me.dgvGLDefault)
        Me.TabGLDefault.Location = New System.Drawing.Point(4, 22)
        Me.TabGLDefault.Name = "TabGLDefault"
        Me.TabGLDefault.Padding = New System.Windows.Forms.Padding(3)
        Me.TabGLDefault.Size = New System.Drawing.Size(860, 375)
        Me.TabGLDefault.TabIndex = 3
        Me.TabGLDefault.Text = "GL Default"
        Me.TabGLDefault.UseVisualStyleBackColor = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.UpdateOrder = WindowsApp1.DataSource_ViewSystemIntegirtyErrorsTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(707, 419)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvFilingEntityState
        '
        Me.dgvFilingEntityState.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFilingEntityState.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFilingEntityState.Location = New System.Drawing.Point(3, 3)
        Me.dgvFilingEntityState.Name = "dgvFilingEntityState"
        Me.dgvFilingEntityState.Size = New System.Drawing.Size(854, 369)
        Me.dgvFilingEntityState.TabIndex = 0
        '
        'dgvFilingEntityStateGroup1
        '
        Me.dgvFilingEntityStateGroup1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFilingEntityStateGroup1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFilingEntityStateGroup1.Location = New System.Drawing.Point(3, 3)
        Me.dgvFilingEntityStateGroup1.Name = "dgvFilingEntityStateGroup1"
        Me.dgvFilingEntityStateGroup1.Size = New System.Drawing.Size(854, 369)
        Me.dgvFilingEntityStateGroup1.TabIndex = 0
        '
        'dgvStateBinder
        '
        Me.dgvStateBinder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStateBinder.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvStateBinder.Location = New System.Drawing.Point(3, 3)
        Me.dgvStateBinder.Name = "dgvStateBinder"
        Me.dgvStateBinder.Size = New System.Drawing.Size(854, 369)
        Me.dgvStateBinder.TabIndex = 0
        '
        'dgvGLDefault
        '
        Me.dgvGLDefault.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGLDefault.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvGLDefault.Location = New System.Drawing.Point(3, 3)
        Me.dgvGLDefault.Name = "dgvGLDefault"
        Me.dgvGLDefault.Size = New System.Drawing.Size(854, 369)
        Me.dgvGLDefault.TabIndex = 0
        '
        'frmViewSystemIntegrityErrors
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(892, 485)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmViewSystemIntegrityErrors"
        Me.Text = "View System Integrity Errors"
        Me.TabControl1.ResumeLayout(False)
        Me.TabFilingEntity_State.ResumeLayout(False)
        CType(Me.DataSource_ViewSystemIntegirtyErrors, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabFilingEntity_State_Group1.ResumeLayout(False)
        Me.TabState_Binder.ResumeLayout(False)
        Me.TabGLDefault.ResumeLayout(False)
        CType(Me.dgvFilingEntityState, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFilingEntityStateGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStateBinder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGLDefault, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabFilingEntity_State As TabPage
    Friend WithEvents TabFilingEntity_State_Group1 As TabPage
    Friend WithEvents TabState_Binder As TabPage
    Friend WithEvents TabGLDefault As TabPage
    Friend WithEvents DataSource_ViewSystemIntegirtyErrors As DataSource_ViewSystemIntegirtyErrors
    Friend WithEvents TableAdapterManager As DataSource_ViewSystemIntegirtyErrorsTableAdapters.TableAdapterManager
    Friend WithEvents btnClose As Button
    Friend WithEvents dgvFilingEntityState As DataGridView
    Friend WithEvents dgvFilingEntityStateGroup1 As DataGridView
    Friend WithEvents dgvStateBinder As DataGridView
    Friend WithEvents dgvGLDefault As DataGridView
End Class

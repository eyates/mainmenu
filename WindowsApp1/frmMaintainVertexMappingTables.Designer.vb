﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMaintainVertexMappingTables
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabJurisdictionMapping = New System.Windows.Forms.TabPage()
        Me.TabTaxCategoryMapping = New System.Windows.Forms.TabPage()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.DGV_JurisdictionMapping = New System.Windows.Forms.DataGridView()
        Me.DGV_TaxCategoryMapping = New System.Windows.Forms.DataGridView()
        Me.TabControl1.SuspendLayout()
        Me.TabJurisdictionMapping.SuspendLayout()
        Me.TabTaxCategoryMapping.SuspendLayout()
        CType(Me.DGV_JurisdictionMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_TaxCategoryMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabJurisdictionMapping)
        Me.TabControl1.Controls.Add(Me.TabTaxCategoryMapping)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(745, 404)
        Me.TabControl1.TabIndex = 0
        '
        'TabJurisdictionMapping
        '
        Me.TabJurisdictionMapping.Controls.Add(Me.DGV_JurisdictionMapping)
        Me.TabJurisdictionMapping.Location = New System.Drawing.Point(4, 22)
        Me.TabJurisdictionMapping.Name = "TabJurisdictionMapping"
        Me.TabJurisdictionMapping.Padding = New System.Windows.Forms.Padding(3)
        Me.TabJurisdictionMapping.Size = New System.Drawing.Size(737, 378)
        Me.TabJurisdictionMapping.TabIndex = 0
        Me.TabJurisdictionMapping.Text = "Jurisdiction Mapping"
        Me.TabJurisdictionMapping.UseVisualStyleBackColor = True
        '
        'TabTaxCategoryMapping
        '
        Me.TabTaxCategoryMapping.Controls.Add(Me.DGV_TaxCategoryMapping)
        Me.TabTaxCategoryMapping.Location = New System.Drawing.Point(4, 22)
        Me.TabTaxCategoryMapping.Name = "TabTaxCategoryMapping"
        Me.TabTaxCategoryMapping.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTaxCategoryMapping.Size = New System.Drawing.Size(737, 378)
        Me.TabTaxCategoryMapping.TabIndex = 1
        Me.TabTaxCategoryMapping.Text = "TaxCategoryMapping"
        Me.TabTaxCategoryMapping.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(678, 422)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DGV_JurisdictionMapping
        '
        Me.DGV_JurisdictionMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_JurisdictionMapping.Location = New System.Drawing.Point(0, 0)
        Me.DGV_JurisdictionMapping.Name = "DGV_JurisdictionMapping"
        Me.DGV_JurisdictionMapping.Size = New System.Drawing.Size(737, 359)
        Me.DGV_JurisdictionMapping.TabIndex = 0
        '
        'DGV_TaxCategoryMapping
        '
        Me.DGV_TaxCategoryMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_TaxCategoryMapping.Location = New System.Drawing.Point(0, 0)
        Me.DGV_TaxCategoryMapping.Name = "DGV_TaxCategoryMapping"
        Me.DGV_TaxCategoryMapping.Size = New System.Drawing.Size(737, 352)
        Me.DGV_TaxCategoryMapping.TabIndex = 0
        '
        'frmMaintainVertexMappingTables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 493)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMaintainVertexMappingTables"
        Me.Text = "Maintain Vertex Mapping Tables"
        Me.TabControl1.ResumeLayout(False)
        Me.TabJurisdictionMapping.ResumeLayout(False)
        Me.TabTaxCategoryMapping.ResumeLayout(False)
        CType(Me.DGV_JurisdictionMapping, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_TaxCategoryMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabJurisdictionMapping As TabPage
    Friend WithEvents TabTaxCategoryMapping As TabPage
    Friend WithEvents btnClose As Button
    Friend WithEvents DGV_JurisdictionMapping As DataGridView
    Friend WithEvents DGV_TaxCategoryMapping As DataGridView
End Class

﻿Imports System.Data.SqlClient

Public Class FormMaintainJurisdiction
    Private Sub JurisdictionBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.JurisdictionBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.SalesUse_UniDataSet)

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SalesUse_UniDataSet.Period' table. You can move, or remove it, as needed.
        'Me.PeriodTableAdapter.Fill(Me.SalesUse_UniDataSet.Period)
        'TODO: This line of code loads data into the 'SalesUse_UniDataSet.Jurisdiction' table. You can move, or remove it, as needed.
        LoadPeriodCombo()
        Me.JurisdictionTableAdapter.Fill(Me.SalesUse_UniDataSet.Jurisdiction, cboPeriodID.Text)

    End Sub

    Private Sub PeriodComboBox_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub
    Private Sub LoadPeriodCombo()
        Dim SQL As String = "SELECT Period AS disp FROM Period with (NOLOCK) ORDER BY Period Desc "
        Using connection As SqlConnection = New SqlConnection("Data Source=EV-LAPTOP;Initial Catalog=SalesUse_Uni;Integrated Security=True")
            connection.Open()
            Using comm As SqlCommand = New SqlCommand(SQL, connection)
                Dim rs As SqlDataReader = comm.ExecuteReader
                Dim dt As DataTable = New DataTable
                dt.Load(rs)
                cboPeriodID.ValueMember = "Period"
                cboPeriodID.DisplayMember = "disp"
                cboPeriodID.DataSource = dt
            End Using 'comm
        End Using 'conn
    End Sub

    Private Sub cboPeriodID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodID.SelectedIndexChanged
        Me.JurisdictionTableAdapter.Fill(Me.SalesUse_UniDataSet.Jurisdiction, "201612")
    End Sub

    Private Sub TabJurisdictionDetail_Click(sender As Object, e As EventArgs) Handles TabJurisdictionDetail.Click

    End Sub
End Class
